#if !defined(WADIMPORT_H)
#define WADIMPORT_H

#include "ImportType.h"

namespace
{
    struct PatchPicture;
};

class WADImport : public ImportType
{
public:

    void makeImportType(MemoryArena *tempArena) override;    
    void unpackContainer(MemoryArena *tempArena, AssetCache::AssetBlock *containerAsset,
                         AssetCache *assetStore, MemoryArena *gameArena) override;
    void unpackLevelData(MemoryArena *tempArena, AssetCache *assetStore, WorldObjects *levelData) override;    
       

private:

    enum class LumpType
    {
        THINGS,
        VERTEXES,
        LINEDEFS,
        SIDEDEFS,
        SECTORS,
        NODES,
        SUBSECTORS,
        SEGMENTS,
        COUNT
    };
    
    struct SectorVertexDef
    {
        i32 vertCount;
        vfVec2 sectorVerts[WorldRef::maxWallsPerSector];
        i32 firstSegment;
        i32 segmentCount;
    };

    SectorVertexDef *sectorVertexList;
    SectorVertexDef *buildVertexList;

    struct GeometryDataBlock
    {
        MemoryArena *arena;
        AssetCache *assetStore;
        vclib::vcString *assetNames;
        WorldObjects *levelData;
        vfRect levelExtent;
    };

    struct UnpackDataBlock
    {
        MemoryArena *arena;
        vclib::vcString *lumpNames;
        u8 *baseAddress;
        i32 directoryOffset;
        i32 count;
    };

    i32 texturePatchCount = 0;
    PatchPicture *patchArray = nullptr;
    u32 *paletteLookup = nullptr;
    
    
private:

    i32 asIndex(LumpType type);
    vclib::vcString * createNameTable(MemoryArena *arena);
    
    void featureDependencyPass(UnpackDataBlock *loadData);
    void loadMapGeometry(UnpackDataBlock *loadData, MemoryArena *persistData,
                         vclib::vcString mapName, AssetCache *assetStore);
    void loadFlatTextures(UnpackDataBlock *loadData, MemoryArena *persistData, AssetCache *assetStore);
    void loadWallTextures(UnpackDataBlock *loadData, MemoryArena *persistData, AssetCache *assetStore);

    
    vfRect loadVertexInformation(GeometryDataBlock *callData);
    void setPlayerPosition(GeometryDataBlock *callData);
    void createSectorDataFromBSP(GeometryDataBlock *callData);
    void combineSectorVertexes(GeometryDataBlock *callData);
    void attachSectorFlatTextures(GeometryDataBlock *callData);
    void attachWallTexturesForSector(GeometryDataBlock *callData, i32 sectorIndex);

    i32 findSectorFromPositionUsingBSP(GeometryDataBlock *callData, vfVec2 worldPosition);
    i32 clipPolygonToLine(vfVec2 *vertexArray, i32 vertexCount,
                          vfVec2 clipStart, vfVec2 clipEnd, bool rightSide = true);
    void simplifyStoredPolygon(SectorVertexDef *vertexData);
    u8 * selectRawAsset(vclib::vcString assetName, AssetCache *assetStore,
                        AssetCache::RawBlockType *assetDetails = nullptr);

    void findArraysFromSegmentIndex(GeometryDataBlock *callData, i32 segmentIdx,
                                    u8 **segmentAddress = nullptr, u8 **linedefAddress = nullptr,
                                    u8 **sidedefAddress = nullptr, u8 **sectorAddress = nullptr);
};

#endif
