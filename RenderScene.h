#if !defined(RENDERSCENE_H)
#define RENDERSCENE_H

#include "PlatformTypes.h"
#include "VectorFunctions.h"
#include "AssetCache.h"
#include "WorldObjects.h"

class RenderScene
{
public:
    
    struct KeyState
    {
        bool isKeyDown;
        u32 keyTransitions;
    };
    
    struct ControllerState
    {
        bool controllerValid;
        bool digitalInput;
        r32 leftStickX;
        r32 leftStickY;
        r32 rightStickX;
        r32 rightStickY;
        
        union
        {
            KeyState keyList[10];
            struct
            {            
                KeyState moveFwd;
                KeyState moveBack;
                KeyState turnLeft;
                KeyState turnRight;
                KeyState moveLeft;
                KeyState moveRight;
                KeyState jump;
                KeyState crouch;
                KeyState increaseDepth;
                KeyState decreaseDepth;
            };
        };
    };

    struct FrameContext
    {
        vfRect screenBounds;
        vfVec2 screenDim;
        vfRect worldBounds;
        vfVec2 worldDim;
    };

    struct VerticalClip
    {
        r32 *floor;
        r32 *ceiling;
    };

    struct FlatPlane
    {
        bool skybox;
        bool visibility;
        i32 minX;
        i32 maxX;
        i32 extentX;
        r32 *startY;
        r32 *endY;
        r32 height;
        r32 uScale;
        r32 vScale;
        r32 shade;
        i32 texWidth;
        i32 texHeight;
        u32 *texelData;

        FlatPlane *next;
    };

    struct RenderItem
    {
        r32 screenClipMinX;
        r32 screenClipMaxX;
        i32 drawSector;
        i32 originatingSector;

        RenderItem *next;
    };

    struct VisibilityItem
    {
        vfVec2 worldA;
        vfVec2 worldB;
        r32 shade;

        VisibilityItem *next;
    };

    struct MappedTexture
    {
        bool mapValid;
        bool invert;
        vfVec2 texelA;
        vfVec2 texelB;
        r32 depthA;
        r32 depthB;
        r32 shade;
        i32 width;
        i32 height;
        u32 *texelData;
    };

    struct DeferredStrip
    {
        bool render;
        i32 column;
        r32 texelU;
        r32 floorPoint;
        r32 ceilingPoint;
        r32 lowerClip;
        r32 upperClip;
        r32 depth;
    };
    
    struct BackRenderTexture
    {
        MappedTexture textureData;
        i32 stripCount;
        DeferredStrip *renderStripArray;
        BackRenderTexture *next;
    };


public:

    void setCurrentLevelGeometry(WorldObjects *geometry);
    void initRenderBuffer(void *pixelData, i32 width, i32 height, i32 pitch);
    void clearRenderBuffer(vfVec3 colour);
    void updateSimulationAndRender(ControllerState *digitalState, ControllerState *analogState,
                                   r32 simulationTime, MemoryArena *frameArena);
    void renderOverlay(r64 frameTime, r64 frameInterval);
    
private:

    void drawPerspectiveView(vfRecti pixelFrame, MemoryArena *frameArena, VisibilityItem **visibilityList);
    void drawDeferredWalls(BackRenderTexture *renderList);
    void drawPlanes(FrameContext *ctx, FlatPlane *planeList, r32 playerEyelevel, MemoryArena *frameArena, VisibilityItem **visibilityList);
    void drawParallaxPlane(FrameContext *ctx, FlatPlane *plane);
    void drawMapView(vfRecti pixelFrame, VisibilityItem *visibilityList);
    void drawSectorWalls(FrameContext *ctx, i32 sector, vfVec3 portalColour, vfVec3 edgeColour);

    vfRect getWorldBoundsForLevel(vfVec2 extents);
    vfVec2 translateWorldToViewport(FrameContext *ctx, vfVec2 worldPos);
    vfVec3 translatePerspectiveToViewport(FrameContext *ctx, vfVec3 perspectPos);
    
    void drawFilledRect(vfRecti clipRect, vfVec2 topLeft, vfVec2 bottomRight, vfVec3 colour);
    bool edgeTest(float delta, float edgeDiff, float *t0, float *t1);
    void drawLineRectIntersection(vfRect rect, vfVec2 start, vfVec2 end, vfVec3 colour);
    void drawSimpleLine(vfVec2 start, vfVec2 end, vfVec3 colour);

    void untexturedStrip(i32 screenColumn, r32 start, r32 end, r32 clampLow, r32 clampHigh, vfVec3 colour, r32 depth);

    void texturedStrip(i32 screenColumn, r32 texelU, MappedTexture *surface, r32 start, r32 end, r32 clampLow, r32 clampHigh, r32 depth);
    void transparentStrip(i32 screenColumn, r32 texelU, MappedTexture *surface, r32 start, r32 end, r32 clampLow, r32 clampHigh, r32 depth);
    void deferredStrip(BackRenderTexture *stackFront, i32 startColumn, i32 screenColumn, r32 texelU, r32 start, r32 end, r32 clampLow, r32 clampHigh, r32 depth);
    
    FlatPlane *makeFlatPlane(WorldObjects::SectorDef::PlaneDef *plane, 
                             FlatPlane **allocList, MemoryArena *frameArena,
                             r32 startX, r32 endX, r32 clampMin, r32 clampMax);
    void addStripToFlatPlane(FlatPlane *plane, i32 screenColumn, r32 start, r32 end, r32 clampLow, r32 clampHigh);

    MappedTexture initTextureSurface(WorldObjects::SectorDef::WallDef *surface,
                                     WorldObjects::SectorDef::WallTexture *surfaceTexture,
                                     vfVec2 clipWallA, vfVec2 clipWallB,
                                     vfVec2 unclipWallA, r32 wallHeight);

    
private:

    // Provided world geometry
    WorldObjects *levelData = nullptr;
    
    // Graphics
    u32 *rgbaTarget = nullptr;
    static const i32 bytesPerPixel = 4;
    i32 displayWidth;
    i32 displayHeight;
    i32 displayPitch;

    vfRecti mapFrame;
    vfRecti perspectiveFrame;

    // Rendering settings
    i32 renderDepth = 256;
    bool renderDepthIncMask = false;
    bool renderDepthDecMask = false;
    
    // Precomputed focal length values taken from player FoV
    r32 hFocalLength = 1.0f;
    r32 vFocalLength = 1.0f;

    // Precomputed view distance scaling for colour
    r32 colourScaling = 1.0f;
};

#endif
