#if !defined(CUSTOMIMPORT_H)
#define CUSTOMIMPORT_H

class CustomImport : public ImportType
{
public:

    void makeImportType(MemoryArena *tempArena) override;    
    void unpackContainer(MemoryArena *tempArena, AssetCache::AssetBlock *containerAsset,
                         AssetCache *assetStore, MemoryArena *gameArena) override;
    void unpackLevelData(MemoryArena *tempArena, AssetCache *assetStore, WorldObjects *levelData) override;    


private:

    struct TokenLink
    {
        u8 tokenType;
        vclib::vcString tokenString;
        i32 tokenLength;
        TokenLink *nextToken;
    };

    TokenLink *tokenSentinel = nullptr;
    TokenLink *tokenListTail = nullptr;


private:
    
    void lexicalRead(u8 *dataBlock, i32 dataSize, MemoryArena *tempArena);
    void appendTokenList(u8 *tokenStart, u8 *tokenEnd, MemoryArena *tempArena);
    void parseGeometry(TokenLink *parseTokens, AssetCache *assetStore, WorldObjects *levelData);

    TokenLink * processVertexGroup(TokenLink *firstTokenInGroup, WorldObjects *levelData);
    TokenLink * processSectorGroup(TokenLink *firstTokenInGroup, AssetCache *assetStore, WorldObjects *levelData);
    TokenLink * processPlayerGroup(TokenLink *firstTokenInGroup, WorldObjects *levelData);        
};

#endif
