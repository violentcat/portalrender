
#include "StackArena.h"

u8 * StackAllocator::allocate(i32 allocationSize)
{
    i32 fullAllocSize = allocationSize + sizeof(StackHeader);
    platformAssert(fullAllocSize < arenaSize);

    if (fullAllocSize + usedSize > arenaSize)
    {
        platformAssert(allocationSize == 0);
        return nullptr;
    }

    u8 *blockStart = &(baseAddress[usedSize]);
    usedSize += fullAllocSize;    

    // Write header information for stack allocation
    StackHeader *writeHeader = reinterpret_cast<StackHeader *>(blockStart);
    writeHeader->size = allocationSize;

    // .. and offset returned block pointer to after the stack header
    return (blockStart + sizeof(StackHeader));
}

void StackAllocator::release(u8 *block)
{
    if (block == nullptr)
        return;

    StackHeader *readHeader = reinterpret_cast<StackHeader *>(block - sizeof(StackHeader));
    i32 releaseSize = readHeader->size + sizeof(StackHeader);

    usedSize -= releaseSize;
}

