
#include "WADImport.h"
#include "WorldObjects.h"

namespace
{
    // See https://doomwiki.org/wiki/WAD for more complete documentation
    // of these WAD elements    
#pragma pack(push,1)
    struct WADHeader
    {
        u8 identity[4];
        i32 lumpCount;
        i32 directoryOffset;
    };

    struct LumpDirectory
    {
        i32 offset;
        i32 size;
        u8 name[8];
    };

    struct PatchHeader
    {
        i16 width;
        i16 height;
        i16 leftOffset;
        i16 topOffset;       
    };

    struct PostHeader
    {
        u8 yOffset;
        u8 length;
        u8 padding;
    };

    struct PatchPicture
    {
        vclib::vcString name;
        i32 width;
        i32 height;
        i32 leftOffset;
        i32 topOffset;
        u32 *pixelData;
    };

    struct TextureHeader
    {
        u8 name[8];
        u32 masked;
        i16 width;
        i16 height;
        u32 columnDirectory;
        i16 patchCount;
    };

    struct TexturePatch
    {
        i16 originX;
        i16 originY;
        i16 patch;
        i16 stepDir;
        i16 colorMap;
    };

    struct ThingLump
    {
        i16 xCoord;
        i16 yCoord;
        i16 angle;
        u16 type;
        u16 flags;
    };
    
    struct LineDefLump
    {
        i16 startVertex;
        i16 endVertex;
        i16 flags;
        i16 type;
        i16 sectorTag;
        i16 frontDef;
        i16 backDef;
    };
    
    struct SideDefLump
    {
        i16 offsetX;
        i16 offsetY;
        u8 upperTexture[8];
        u8 lowerTexture[8];
        u8 midTexture[8];
        i16 sector;
    };
    
    struct SectorLump
    {
        i16 floorHeight;
        i16 ceilingHeight;
        u8 floorTexture[8];
        u8 ceilingTexture[8];
        i16 lightLevel;
        i16 type;
        i16 sectorTag;
    };

    struct NodeBoundingBox
    {
        i16 yTop;
        i16 yBottom;
        i16 xLeft;
        i16 xRight;
    };

    struct NodeLump
    {
        i16 xCoord;
        i16 yCoord;
        i16 xDelta;
        i16 yDelta;
        NodeBoundingBox rightBox;
        NodeBoundingBox leftBox;
        u16 rightNode;
        u16 leftNode;
    };

    struct SubsectorLump
    {
        i16 segmentCount;
        i16 firstSegment;
    };

    struct SegmentLump
    {
        i16 startVertex;
        i16 endVertex;
        i16 angle;
        i16 lineDef;
        i16 direction;
        i16 linedefOffset;
    };

    const r32 axisScale = 128.f;
    
#pragma pack(pop)    
};

void WADImport::makeImportType(MemoryArena *tempArena)
{
    sectorVertexList = tempArena->pushArray<SectorVertexDef>(WorldRef::maxSectorCount);
    buildVertexList = tempArena->pushStruct<SectorVertexDef>();
}

void WADImport::unpackContainer(MemoryArena *tempArena, AssetCache::AssetBlock *containerAsset,
                         AssetCache *assetStore, MemoryArena *gameArena)
{
    // This is a WAD importer so make sure the asset is a WAD
    if (containerAsset->type != AssetCache::AssetType::WAD)
        return;
    
    u8 *dataPos = containerAsset->impl.raw.data;
    i32 dataToConsume = containerAsset->impl.raw.length;
    if (dataToConsume < 12)
        return;

    typedef AssetCache::AssetIdentity Ast_Id;
    typedef AssetCache::AssetType Ast_Type;
    typedef AssetCache::AssetBlock Ast_Block;        
    
    u8 *baseAddress = dataPos;    
    WADHeader header = vclib::readElement<WADHeader>(dataPos, &dataToConsume);
    
    vclib::vcString fileType = vclib::stringFromBuffer(tempArena, header.identity, 4);
    if (vclib::compareRaw(fileType, "IWAD", 4) == false)
    {
        return;
    }

    u8 *directory = baseAddress + header.directoryOffset;

    vclib::vcString selectedMap = nullptr;  
    Ast_Block mapAsset = assetStore->findAsset(Ast_Id::MapName);
    
    if (mapAsset.type == Ast_Type::STR && mapAsset.impl.raw.length > 0)
    {
        selectedMap = vclib::stringFromBuffer(tempArena, mapAsset.impl.raw.data,
                                              mapAsset.impl.raw.length);        
    }
    else
    {
        selectedMap = vclib::stringFromRaw(tempArena, "E1M1");
    }

    UnpackDataBlock callData;
    callData.arena = tempArena;
    callData.baseAddress = baseAddress;
    callData.directoryOffset = header.directoryOffset;
    callData.count = header.lumpCount;

    // Initial data load for dependencies such as colour palette and
    // texture patch lists
    featureDependencyPass(&callData);

    // Perform the actual asset load steps
    loadMapGeometry(&callData, gameArena, selectedMap, assetStore);
    loadFlatTextures(&callData, gameArena, assetStore);
    loadWallTextures(&callData, gameArena, assetStore);        
}

void WADImport::unpackLevelData(MemoryArena *tempArena, AssetCache *assetStore, WorldObjects *levelData)
{
    typedef AssetCache::AssetIdentity Asset_Id;
    typedef AssetCache::AssetType Asset_Type;

    // This is a WAD importer so make sure there are Doom level assets
    AssetCache::AssetBlock doomType = assetStore->findAsset(Asset_Id::DoomMap);
    if (!(doomType.type == Asset_Type::RAW && doomType.impl.raw.length > 0))
        return;
    
    levelData->player = {};
    levelData->vertexCount = 0;
    levelData->sectorCount = 0;
    for (i32 loop = 0; loop < WorldRef::maxSectorCount; loop++)
    {
        levelData->sectorList[loop].wallCount = 0;
        sectorVertexList[loop].vertCount = 0;
    }

    GeometryDataBlock callData;
    callData.arena = tempArena;
    callData.assetStore = assetStore;
    callData.assetNames = createNameTable(tempArena);
    callData.levelData = levelData;
    callData.levelExtent = loadVertexInformation(&callData);
    
    setPlayerPosition(&callData);
    createSectorDataFromBSP(&callData);
    combineSectorVertexes(&callData);
    attachSectorFlatTextures(&callData);

    for (i32 sectorIndex = 0; sectorIndex < levelData->sectorCount; sectorIndex++)
    {
        WorldObjects::SectorDef *currentSector = &(levelData->sectorList[sectorIndex]);
        SectorVertexDef *currentVertexes = &(sectorVertexList[sectorIndex]);

        // Build sector walls from polygon vertex list
        for (i32 loop = 0; loop < currentVertexes->vertCount; loop++)
        {
            WorldObjects::SectorDef::WallDef *wallAccess = &currentSector->lineSegment[currentSector->wallCount];
                
            vfVec2 &lineStart = currentVertexes->sectorVerts[loop];
            vfVec2 &lineEnd = (loop == currentVertexes->vertCount-1) ? currentVertexes->sectorVerts[0] : currentVertexes->sectorVerts[loop+1];

            wallAccess->start = lineStart;
            wallAccess->end = lineEnd;
            wallAccess->wallLength = distance(lineEnd, lineStart);
            wallAccess->highTexture.map.data = nullptr;
            wallAccess->lowTexture.map.data = nullptr;
            wallAccess->wallTexture.map.data = nullptr;
            wallAccess->travelToSector = -1;
            wallAccess->blocking = false;
            wallAccess->traverse = true;
            wallAccess->shading = currentSector->floor.shading;
            
            currentSector->wallCount++;
        }

        attachWallTexturesForSector(&callData, sectorIndex);
    }

    // Re-parse all subsectors to complete associated side information for each
    // subsector wall
    for (i32 sectorIndex = 0; sectorIndex < levelData->sectorCount; sectorIndex++)
    {
        WorldObjects::SectorDef &currentSector = levelData->sectorList[sectorIndex];

        for (i32 wallIndex = 0; wallIndex < currentSector.wallCount; wallIndex++)
        {
            WorldObjects::SectorDef::WallDef &currentWall = currentSector.lineSegment[wallIndex];

            // Generate unit normal from sector wall
            vfVec2 unitNormal = unit(currentWall.end - currentWall.start) * normal();

            // .. and apply it at the midpoint of the wall to determine a
            // point on the outside of the wall for BSP traversal
            vfVec2 outsidePoint = midpoint(currentWall.start, currentWall.end) + (unitNormal * 0.01f);
            
            // Using the computed 'outside' point, traverse the BSP to find out
            // which sector the point lies in. This can then be set as the
            // travelToSector value for when player moves through this traversable
            // wall into an adjoining sector
            i32 sectorId = findSectorFromPositionUsingBSP(&callData, outsidePoint);
            if (currentWall.traverse == true)
            {
                currentWall.travelToSector = sectorId;
            }
        }
    }

    // Finally set the player initial sector using BSP traversal
    levelData->player.sector = findSectorFromPositionUsingBSP(&callData, levelData->player.position);
}

i32 WADImport::asIndex(LumpType type)
{
    switch(type)
    {
        case LumpType::THINGS:
            return 0;
        case LumpType::VERTEXES:
            return 1;
        case LumpType::LINEDEFS:
            return 2;
        case LumpType::SIDEDEFS:
            return 3;
        case LumpType::SECTORS:
            return 4;
        case LumpType::NODES:
            return 5;
        case LumpType::SUBSECTORS:
            return 6;
        case LumpType::SEGMENTS:
            return 7;
        case LumpType::COUNT:
            return 8;
    };

    return -1;
}

vclib::vcString * WADImport::createNameTable(MemoryArena *arena)
{
    vclib::vcString *nameTable = arena->pushArray<vclib::vcString>(asIndex(LumpType::COUNT));
    for (i32 loop = 0; loop < asIndex(LumpType::COUNT); loop++)
    {
        nameTable[loop] = nullptr;
    }

    nameTable[asIndex(LumpType::THINGS)] = vclib::stringFromRaw(arena, "things");
    nameTable[asIndex(LumpType::VERTEXES)] = vclib::stringFromRaw(arena, "vertexes");
    nameTable[asIndex(LumpType::LINEDEFS)] = vclib::stringFromRaw(arena, "linedefs");
    nameTable[asIndex(LumpType::SIDEDEFS)] = vclib::stringFromRaw(arena, "sidedefs");
    nameTable[asIndex(LumpType::SECTORS)] = vclib::stringFromRaw(arena, "sectors");
    nameTable[asIndex(LumpType::NODES)] = vclib::stringFromRaw(arena, "nodes");
    nameTable[asIndex(LumpType::SUBSECTORS)] = vclib::stringFromRaw(arena, "ssectors");
    nameTable[asIndex(LumpType::SEGMENTS)] = vclib::stringFromRaw(arena, "segs");

    return nameTable;
}
    
void WADImport::featureDependencyPass(UnpackDataBlock *loadData)
{
    // Initialise storage for contained palette and lump name arrays
    paletteLookup = loadData->arena->pushArray<u32>(256);
    loadData->lumpNames = loadData->arena->pushArray<vclib::vcString>(loadData->count);

    u8 *directory = loadData->baseAddress + loadData->directoryOffset;
    
    for (i32 lumpId = 0; lumpId < loadData->count; lumpId++)
    {
        LumpDirectory lump = vclib::readElement<LumpDirectory>(directory);

        loadData->lumpNames[lumpId] = vclib::stringFromBuffer(loadData->arena, lump.name, 8);
        vclib::lower(loadData->lumpNames[lumpId]);

        if (vclib::compareRaw(loadData->lumpNames[lumpId], "playpal", 7) == true)
        {
            // NOTE(KN): Special DOOM rendering colour modes are not supported
            // so just decode palette 0 (normal), although check the size of the
            // lump (14 x palettes) for integrity

            u8 *paletteAddr = loadData->baseAddress + lump.offset;
            if (lump.size == 3 * 256 * 14) // RGB * 256 colours * 14 palettes
            {
                for (i32 loop = 0; loop < 256; loop++)
                {
                    u32 resultColour = (*paletteAddr++) << 16;
                    resultColour |= (*paletteAddr++) << 8;
                    resultColour |= (*paletteAddr++);
                    paletteLookup[loop] = resultColour;
                }                
            }

            continue;
        }

        if (vclib::compareRaw(loadData->lumpNames[lumpId], "pnames", 6) == true)
        {
            u8 *readPatchBuffer = loadData->baseAddress + lump.offset;            
            texturePatchCount = vclib::readElement<i32>(readPatchBuffer);

            patchArray = loadData->arena->pushArray<PatchPicture>(texturePatchCount);
            for (i32 loop = 0; loop < texturePatchCount; loop++)
            {
                patchArray[loop].name = vclib::stringFromBuffer(loadData->arena, readPatchBuffer, 8);
                vclib::lower(patchArray[loop].name);
                readPatchBuffer += 8;
            }
            
            continue;
        }
    }
}

#define FIND_MAP_LUMP(LumpSize, LumpName, NameLength)                                                    \
    if (!LumpSize && vclib::compareRaw(loadData->lumpNames[lumpId], LumpName, NameLength) == true)       \
    {                                                                                                    \
        AssetCache::AssetBlock *block =                                                                  \
            assetStore->createAssetBlock(persistData,                                                    \
                                         AssetCache::AssetIdentity::DoomMap,                             \
                                         AssetCache::AssetType::RAW);                                    \
        block->name = vclib::stringCopy(persistData, loadData->lumpNames[lumpId]);                       \
        block->impl.raw.data = loadData->baseAddress + lump.offset;                                      \
        block->impl.raw.length = lump.size;                                                              \
        LumpSize = lump.size;                                                                            \
        continue;                                                                                        \
    }

void WADImport::loadMapGeometry(UnpackDataBlock *loadData, MemoryArena *persistData,
                                vclib::vcString mapName, AssetCache *assetStore)
{
    u8 *directory = loadData->baseAddress + loadData->directoryOffset;
    
    // Copy the string so we can manipulate it without affecting the original
    vclib::vcString selectedMap = vclib::stringCopy(loadData->arena, mapName);
    vclib::lower(selectedMap);

    // NOTE(KN): Once loadingMapData becomes true, load the first block of each
    // map data type we come across
    bool loadingMapData = false;
    i32 vertexSize = 0;
    i32 linedefSize = 0;
    i32 sidedefSize = 0;
    i32 sectorSize = 0;
    i32 thingSize = 0;
    i32 segmentsSize = 0;
    i32 subsectorSize = 0;
    i32 nodesSize = 0;
    
    for (i32 lumpId = 0; lumpId < loadData->count; lumpId++)
    {
        LumpDirectory lump = vclib::readElement<LumpDirectory>(directory);

        if (loadingMapData == false)
        {
            // Scan through the lump names until we spot the specified map
            // name
            if (vclib::compare(selectedMap, loadData->lumpNames[lumpId]) == true)
            {
                loadingMapData = true;
            }
            continue;            
        }        

        FIND_MAP_LUMP(thingSize,     "things",   6)
        FIND_MAP_LUMP(vertexSize,    "vertexes", 8)
        FIND_MAP_LUMP(linedefSize,   "linedefs", 8)
        FIND_MAP_LUMP(sidedefSize,   "sidedefs", 8)
        FIND_MAP_LUMP(sectorSize,    "sectors",  7)
        FIND_MAP_LUMP(segmentsSize,  "segs",     4)
        FIND_MAP_LUMP(subsectorSize, "ssectors", 8)
        FIND_MAP_LUMP(nodesSize,     "nodes",    5)
    }
}

void WADImport::loadFlatTextures(UnpackDataBlock *loadData, MemoryArena *persistData, AssetCache *assetStore)
{
    u8 *directory = loadData->baseAddress + loadData->directoryOffset;

    bool loadingFlatData = false;

    for (i32 lumpId = 0; lumpId < loadData->count; lumpId++)
    {
        LumpDirectory lump = vclib::readElement<LumpDirectory>(directory);

        if (loadingFlatData == false)
        {
            if (vclib::compareRaw(loadData->lumpNames[lumpId], "f_start", 7) == true)
            {
                loadingFlatData = true;
            }            
            continue;
        }

        if (vclib::compareRaw(loadData->lumpNames[lumpId], "f_end", 5) == true)
            break;

        AssetCache::AssetBlock *flatTexture =
            assetStore->createAssetBlock(persistData, AssetCache::AssetIdentity::Texture,
                                         AssetCache::AssetType::TEX);
        
        flatTexture->name = vclib::stringCopy(persistData, loadData->lumpNames[lumpId]);
        flatTexture->impl.tex.width = 64;
        flatTexture->impl.tex.height = 64;
        flatTexture->impl.tex.data = persistData->pushArray<u32>(4096);

        u32 *textureLoad = flatTexture->impl.tex.data;
        u8 *readFlatBuffer = loadData->baseAddress + lump.offset;
            
        for (i32 columnLoop = 0; columnLoop < 64; columnLoop++)
        {
            for (i32 rowLoop = 0; rowLoop < 64; rowLoop++)
            {
                i32 readIndex = columnLoop + (rowLoop * 64);
                *textureLoad++ = paletteLookup[readFlatBuffer[readIndex]];                    
            }
        }
    }    
}

void WADImport::loadWallTextures(UnpackDataBlock *loadData, MemoryArena *persistData, AssetCache *assetStore)
{
    // Search for all patch lumps specified in pnames index list and compose
    // the picture format elements ready for texture creation

    for (i32 patchId = 0; patchId < texturePatchCount; patchId++)
    {
        vclib::vcString& patchName = patchArray[patchId].name;
        u8 *directory = loadData->baseAddress + loadData->directoryOffset;
   
        for (i32 lumpId = 0; lumpId < loadData->count; lumpId++)
        {
            LumpDirectory lump = vclib::readElement<LumpDirectory>(directory);

            if (vclib::compare(patchName, loadData->lumpNames[lumpId]) == true)
            {
                u8 *readPatchBuffer = loadData->baseAddress + lump.offset;
                u8 *readPostAddr = readPatchBuffer;
                PatchHeader patchData = vclib::readElement<PatchHeader>(readPatchBuffer);

                patchArray[patchId].width = patchData.width;
                patchArray[patchId].height = patchData.height;
                patchArray[patchId].leftOffset = patchData.leftOffset;
                patchArray[patchId].topOffset = patchData.topOffset;

                i32 patchTexelCount = patchData.height * patchData.width;
                patchArray[patchId].pixelData = loadData->arena->pushArray<u32>(patchTexelCount);

                for (i32 fillData = 0; fillData < patchTexelCount; fillData++)
                {
                    // Pre-load texture patch with transparent texels
                    patchArray[patchId].pixelData[fillData] = 0xFF000000;
                }                        

                u32 *columnAddress = reinterpret_cast<u32 *>(patchArray[patchId].pixelData);
                
                for (i32 postIndex = 0; postIndex < patchData.width; postIndex++)
                {                    
                    i32 postOffset = vclib::readElement<i32>(readPatchBuffer);
                    u8 *readPostBuffer = loadData->baseAddress + lump.offset + postOffset;
                    
                    while (*readPostBuffer != 0xFF)
                    {
                        PostHeader postData = vclib::readElement<PostHeader>(readPostBuffer);

                        i32 yStart = postData.yOffset;
                        i32 pixelCount = postData.length;                        
                        u32 *offsetAddress = columnAddress + yStart;

                        for (i32 pixelLoop = 0; pixelLoop < pixelCount; pixelLoop++)
                        {
                            *offsetAddress++ = paletteLookup[*readPostBuffer++];
                        }

                        // Clear final padding byte positioned after pixel data
                        readPostBuffer++;
                    }

                    columnAddress += patchData.height;
                }
            }
        }
    }

    // Find texture definitions and build resulting texture asset based
    // on the defined patch collection
    
    u8 *directory = loadData->baseAddress + loadData->directoryOffset;
    for (i32 lumpId = 0; lumpId < loadData->count; lumpId++)
    {
        LumpDirectory lump = vclib::readElement<LumpDirectory>(directory);

        if (vclib::compareRaw(loadData->lumpNames[lumpId], "texture", 7) == true)
        {
            u8 *readMapBuffer = loadData->baseAddress + lump.offset;            
            i32 textureCount = vclib::readElement<i32>(readMapBuffer);

            for (i32 textureIndex = 0; textureIndex < textureCount; textureIndex++)
            {
                i32 textureOffset = vclib::readElement<i32>(readMapBuffer);
                u8 *readTextureBuffer = loadData->baseAddress + lump.offset + textureOffset;

                TextureHeader textureData = vclib::readElement<TextureHeader>(readTextureBuffer);
                i32 texWidth = textureData.width;
                i32 texHeight = textureData.height;

                AssetCache::AssetBlock *composeTex =
                    assetStore->createAssetBlock(persistData, AssetCache::AssetIdentity::Texture,
                                                 AssetCache::AssetType::TEX);
                
                composeTex->name = vclib::stringFromBuffer(persistData, textureData.name, 8);
                vclib::lower(composeTex->name);

                composeTex->impl.tex.width = texWidth;
                composeTex->impl.tex.height = texHeight;
                composeTex->impl.tex.data = persistData->pushArray<u32>(texWidth * texHeight);

                for (i32 fillData = 0; fillData < texWidth * texHeight; fillData++)
                {
                    // Pre-load composed texture with transparent texels
                    composeTex->impl.tex.data[fillData] = 0xFF000000;
                }

                // Add each patch into the final texture
                for (i32 patchIndex = 0; patchIndex < textureData.patchCount; patchIndex++)
                {
                    TexturePatch patchHeader = vclib::readElement<TexturePatch>(readTextureBuffer);
                    PatchPicture *patchData = &(patchArray[patchHeader.patch]);

                    for (i32 colIndex = 0; colIndex < patchData->width; colIndex++)
                    {
                        i32 targetCol = patchHeader.originX + colIndex;
                        if (targetCol >= 0 && targetCol < texWidth)
                        {
                            u32 *textureBuffer = composeTex->impl.tex.data + (targetCol * texHeight);
                            u32 *patchBuffer = patchData->pixelData + (colIndex * patchData->height);
                            textureBuffer += patchHeader.originY;
                            
                            for (i32 rowIndex = 0; rowIndex < patchData->height; rowIndex++)
                            {
                                i32 targetRow = patchHeader.originY + rowIndex;
                                if (targetRow >= 0 && targetRow < texHeight)
                                {
                                    if (!(*patchBuffer & 0xFF000000))
                                    {
                                        *textureBuffer = *patchBuffer;
                                    }
                                }

                                textureBuffer++;
                                patchBuffer++;
                            }                    
                        }                        
                    }
                }
            }
        }        
    }    
}

vfRect WADImport::loadVertexInformation(GeometryDataBlock *callData)
{
    vfVec2 minVertex = {};
    vfVec2 maxVertex = {};
    vfRect vertexExtent = {};
    vclib::vcString vertexAssetName = callData->assetNames[asIndex(LumpType::VERTEXES)];
    
    AssetCache::RawBlockType assetDetails;
    u8 *assetBlock = selectRawAsset(vertexAssetName, callData->assetStore, &assetDetails);
    if (assetBlock)
    {
        i32 remaining = assetDetails.length;

        bool initialisedExtent = false;
        WorldObjects *levelData = callData->levelData;
        
        // Scan through all vertexes to find the suitable min and max extents
        levelData->vertexCount = remaining / (sizeof(i16) * 2);
        for (i32 loop = 0; loop < levelData->vertexCount; loop++)
        {
            i32 xCoord = vclib::readElement<i16>(assetBlock);
            i32 yCoord = vclib::readElement<i16>(assetBlock);            

            r32 scaledX = vclib::scaledInt32ToReal(xCoord, axisScale);
            r32 scaledY = vclib::scaledInt32ToReal(yCoord, axisScale);
            levelData->vertexList[loop] = {scaledX, scaledY};

            if (initialisedExtent == false)
            {
                minVertex = maxVertex = levelData->vertexList[loop];
                initialisedExtent = true;
            }

            minVertex.x = vclib::minValue(minVertex.x, levelData->vertexList[loop].x);
            minVertex.y = vclib::minValue(minVertex.y, levelData->vertexList[loop].y);
            maxVertex.x = vclib::maxValue(maxVertex.x, levelData->vertexList[loop].x);
            maxVertex.y = vclib::maxValue(maxVertex.y, levelData->vertexList[loop].y);                
        }

        // .. and perform a final pass over the vertexes to offset them
        // based on the min bounds identified in the previous step
        for (i32 loop = 0; loop < levelData->vertexCount; loop++)
        {
            levelData->vertexList[loop] = levelData->vertexList[loop] - minVertex;
        }            

        vfVec2 extent = maxVertex - minVertex;
        vertexExtent = {minVertex, maxVertex};
        
        levelData->worldExtent = {extent.x,extent.y};
        levelData->viewDistance = 12.0f;    
        levelData->verticalFov = 167.f * vclib::to_radians;
        levelData->horizontalFov = 90.f * vclib::to_radians;
    }

    return vertexExtent;
}

void WADImport::setPlayerPosition(GeometryDataBlock *callData)
{
    vclib::vcString thingAssetName = callData->assetNames[asIndex(LumpType::THINGS)];

    AssetCache::RawBlockType assetDetails;
    u8 *assetBlock = selectRawAsset(thingAssetName, callData->assetStore, &assetDetails);
    if (assetBlock)
    {
        i32 remaining = assetDetails.length;
        WorldObjects *levelData = callData->levelData;        

        while(remaining > 0)
        {
            ThingLump thingData = vclib::readElement<ThingLump>(assetBlock, &remaining);
            if (thingData.type == 1)
            {
                // Type 1 is the player start location within the map so we use
                // this to initialise our playerdef structure
                levelData->player.position.x = vclib::scaledInt32ToReal(thingData.xCoord, axisScale);
                levelData->player.position.y = vclib::scaledInt32ToReal(thingData.yCoord, axisScale);
                levelData->player.position = levelData->player.position - callData->levelExtent.min;

                // Angle format specified in integer degrees anti-clockwise from 0 = East
                r32 initialAngle = static_cast<r32>(thingData.angle);
                levelData->player.orientation = initialAngle * vclib::two_piflt / 360.f;
            }
        }        
    }
}

void WADImport::createSectorDataFromBSP(GeometryDataBlock *callData)
{
    // Instructions:
    // 1. Read first segment and calculate normal
    // 2. Determine point just inside subsector by moving in direction of normal from mid-point of first segment
    // 3. Create AABB polygon around entire area of map
    // 4. Clip polygon by root partition of BSP
    // 5. Traverse BSP based on point in subsector, clipping polygon at each stage
    // 6. Finally clip resulting polygon by any explicit segments in subsector
    // 7. Add explicit segments to ensure they exist in final polygon
    // 8. Sort polygon vertexes to provide clockwise winding
    // 9. Simplify polygon to remove duplicate vertexes

    vclib::vcString subSectorAssetName = callData->assetNames[asIndex(LumpType::SUBSECTORS)];
    vclib::vcString segmentAssetName = callData->assetNames[asIndex(LumpType::SEGMENTS)];
    vclib::vcString nodeAssetName = callData->assetNames[asIndex(LumpType::NODES)];
    
    AssetCache::RawBlockType assetDetails;
    u8 *ssReadBuffer = selectRawAsset(subSectorAssetName, callData->assetStore, &assetDetails);
    if (ssReadBuffer)
    {
        i32 remaining = assetDetails.length;
        u8 *segmentBlock = selectRawAsset(segmentAssetName, callData->assetStore);

        AssetCache::RawBlockType nodeDetails;
        u8 *nodeBlock = selectRawAsset(nodeAssetName, callData->assetStore, &nodeDetails);
        i32 nodeCount = nodeDetails.length / sizeof(NodeLump);

        vfVec2 *subsectorBuild = &(buildVertexList->sectorVerts[0]);
        i32 & ssPointBuild = buildVertexList->vertCount;
        
        i32 readSubsectorId = 0;
        while(remaining > 0)
        {
            SubsectorLump subsectorInfo = vclib::readElement<SubsectorLump>(ssReadBuffer, &remaining);

            // Read the subsector data to get the first segment
            i16 segmentId = subsectorInfo.firstSegment;
            u8 *segmentLookup = &(segmentBlock[segmentId * sizeof(SegmentLump)]);
            SegmentLump segment = vclib::readElement<SegmentLump>(segmentLookup);

            // Get the end points of the first segment
            vfVec2 *worldVertexList = callData->levelData->vertexList;
            vfVec2 segmentStart = worldVertexList[segment.startVertex];
            vfVec2 segmentEnd = worldVertexList[segment.endVertex];

            // Generate unit normal
            vfVec2 unitNormal = unit(segmentEnd - segmentStart) * normal();

            // .. and apply it at the midpoint of the segment to determine a
            // point inside the subsector ready for BSP traversal
            vfVec2 insidePoint = midpoint(segmentStart, segmentEnd) - (unitNormal * 0.01f);

            // Set vertex base for processing this sector
            vfVec2 *subsectorPoints = &(sectorVertexList[readSubsectorId].sectorVerts[0]);
            i32 & ssPointCount = sectorVertexList[readSubsectorId].vertCount;
            
            // Create polygon from AABB that encloses map vertexes
            vfRect initialBox = makeAABB({0.f,0.f}, callData->levelData->worldExtent);
            subsectorPoints[0] = initialBox.min;
            subsectorPoints[1] = {initialBox.min.x, initialBox.max.y};
            subsectorPoints[2] = initialBox.max;
            subsectorPoints[3] = {initialBox.max.x, initialBox.min.y};
            ssPointCount = 4;

            // Final node in list is the root node, so start with that one
            u8 *nodeLookup = &(nodeBlock[(nodeCount - 1) * sizeof(NodeLump)]);

            bool traverseComplete = false;
            while(traverseComplete == false)
            {
                NodeLump partitionNode = vclib::readElement<NodeLump>(nodeLookup);

                r32 scaledX = vclib::scaledInt32ToReal(partitionNode.xCoord, axisScale);
                r32 scaledY = vclib::scaledInt32ToReal(partitionNode.yCoord, axisScale);
                vfVec2 scaledNodeVertex = {scaledX, scaledY};
                vfVec2 pLineStart = scaledNodeVertex - callData->levelExtent.min;
                
                r32 deltaX = vclib::scaledInt32ToReal(partitionNode.xDelta, axisScale);
                r32 deltaY = vclib::scaledInt32ToReal(partitionNode.yDelta, axisScale);
                vfVec2 nodeDelta = {deltaX, deltaY};
                vfVec2 pLineEnd = scaledNodeVertex + nodeDelta;
                pLineEnd = pLineEnd - callData->levelExtent.min;                               

                // Define direction of partition line as a clipping plane
                vfVec2 clippingPlane = pLineEnd - pLineStart;

                // Determine which side of the partition line the inside subsector
                // point is situated
                vfVec2 samplePointFromPlane = pLineStart - insidePoint;
                r32 samplePointInsidePlane = crossProduct(clippingPlane, samplePointFromPlane);
                bool selectRightSide = (samplePointInsidePlane >= 0.f) ? true : false;

                ssPointCount = clipPolygonToLine(subsectorPoints, ssPointCount,
                                                 pLineStart, pLineEnd, selectRightSide);
                
                // Travel to the next node in the BSP based on which side of the partition
                // line our 'insidePoint' is
                u16 nextBSPNode = (selectRightSide == true) ? partitionNode.rightNode : partitionNode.leftNode;
                bool isSubsector = nextBSPNode & 0x8000 ? true : false;
                i32 subsectorId = nextBSPNode & 0x7FFF;

                if (isSubsector)
                {
                    if (readSubsectorId != subsectorId)
                    {
                        // TODO(KN): For now we're just going to ignore these strange orphaned
                        // subsectors, but we can return to process these and add the seg(s) from
                        // the originating subsector into the found subsector
                        ssPointCount = 0;
                    }

                    //platformAssert(readSubsectorId != subsectorId);
                    traverseComplete = true;
                }
                else
                {
                    nodeLookup = &(nodeBlock[subsectorId * sizeof(NodeLump)]);
                }                
            }

            // At this stage subsector has been formed from implicit BSP partition
            // edges. The subsector can now be finalised using the explicitly defined
            // edges as clip planes
            i32 currentSegment = subsectorInfo.firstSegment;
            for (i32 loop = 0; loop < subsectorInfo.segmentCount; loop++, currentSegment++)
            {
                u8 *explicitSegment = &(segmentBlock[currentSegment * sizeof(SegmentLump)]);
                SegmentLump clipSegment = vclib::readElement<SegmentLump>(explicitSegment);
                
                // Define direction of segment line as a clipping plane
                vfVec2 clipLineStart = callData->levelData->vertexList[clipSegment.startVertex];
                vfVec2 clipLineEnd = callData->levelData->vertexList[clipSegment.endVertex];
                
                ssPointCount = clipPolygonToLine(subsectorPoints, ssPointCount, clipLineStart, clipLineEnd);                
            }

            // It is mandatory that final subsector convex polygon contains all of
            // the vertexes from the segment edges, so  explicitly add these to the
            // end of the point array. We can then sort and simplify (remove duplicates)
            // to create the final polygon            
            currentSegment = subsectorInfo.firstSegment;
            for (i32 loop = 0; loop < subsectorInfo.segmentCount; loop++, currentSegment++)
            {
                u8 *explicitSegment = &(segmentBlock[currentSegment * sizeof(SegmentLump)]);
                SegmentLump addSegment = vclib::readElement<SegmentLump>(explicitSegment);

                subsectorPoints[ssPointCount++] = callData->levelData->vertexList[addSegment.startVertex];
                subsectorPoints[ssPointCount++] = callData->levelData->vertexList[addSegment.endVertex];
            }

            // Sort polygon vertexes into clockwise winding order and remove duplicate
            // vertexes from resulting sorted set
            simplifyStoredPolygon(&(sectorVertexList[readSubsectorId]));

            // Store segment information with vertexes so the sector can reference the
            // original explicit segments for post-processing
            sectorVertexList[readSubsectorId].firstSegment = subsectorInfo.firstSegment;
            sectorVertexList[readSubsectorId].segmentCount = subsectorInfo.segmentCount;
            
            callData->levelData->sectorCount++;
            readSubsectorId++;            
        }
    }
}

void WADImport::combineSectorVertexes(GeometryDataBlock *callData)
{
    // Look for additional crossing points between sectors
    for (i32 sectorIndex = 0; sectorIndex < callData->levelData->sectorCount; sectorIndex++)
    {
        vfVec2 *currentVertexes = &(sectorVertexList[sectorIndex].sectorVerts[0]);
        i32 currentCount = sectorVertexList[sectorIndex].vertCount;
          
        for (i32 loop = 0; loop < currentCount; loop++)
        {                
            vfVec2 &lineStart = currentVertexes[loop];
            vfVec2 &lineEnd = (loop == currentCount-1) ? currentVertexes[0] : currentVertexes[loop+1];
            vfVec2 lineDirection = lineEnd - lineStart;

            for (i32 checkIndex = 0; checkIndex < callData->levelData->sectorCount; checkIndex++)
            {
                if (checkIndex == sectorIndex)
                    continue;

                vfVec2 *checkVertexes = &(sectorVertexList[checkIndex].sectorVerts[0]);
                i32 checkCount = sectorVertexList[checkIndex].vertCount;
                
                for (i32 vertexIndex = 0; vertexIndex < checkCount; vertexIndex++)
                {
                    vfVec2 &compare = checkVertexes[vertexIndex];

                    // For each vertex in any other sector we look to see if it is on
                    // our current sector line. If it is then we need to include this
                    // vertex into our original polygon so we can exactly align sector
                    // traverse information
                    vfVec2 vertexDirection = compare - lineStart;

                    r32 colinearTest = crossProduct(lineDirection, vertexDirection);
                    if (vclib::absoluteValue(colinearTest) < 0.01f)
                    {
                        // vertex is colinear with current line so check whether it
                        // exists within the x and y limits of the line
                        r32 vectorDot = dotProduct(lineDirection, vertexDirection);
                        r32 segmentLength = dotProduct(lineDirection, lineDirection);

                        if (vectorDot >= 0.f && vectorDot <= segmentLength)
                        {
                            i32 & updateCount = sectorVertexList[sectorIndex].vertCount;
                            currentVertexes[updateCount++] = compare;
                        }                                                
                    }                                        
                }
            }           
        }

        // If any new vertexes were added to our current sector polygon then we
        // need to re-sort and simplify
        simplifyStoredPolygon(&(sectorVertexList[sectorIndex]));
    }
}

void WADImport::attachSectorFlatTextures(GeometryDataBlock *callData)
{
    for (i32 sectorIndex = 0; sectorIndex < callData->levelData->sectorCount; sectorIndex++)
    {
        WorldObjects::SectorDef *currentSector = &(callData->levelData->sectorList[sectorIndex]);
        SectorVertexDef *currentVertexes = &(sectorVertexList[sectorIndex]);

        // Read the embedded first segment information for this vertex set so we
        // can preset information that is relevant for the whole sector
        if (currentVertexes->segmentCount)
        {
            u8 *sectorLookup;
            findArraysFromSegmentIndex(callData, currentVertexes->firstSegment,
                                       nullptr, nullptr, nullptr, &sectorLookup);            
            SectorLump sectorContainer = vclib::readElement<SectorLump>(sectorLookup);

            vclib::vcString floorTextureName =
                vclib::stringFromBuffer(callData->arena, sectorContainer.floorTexture, 8);
            vclib::lower(floorTextureName);
            AssetCache::AssetBlock floorTextureData =
                callData->assetStore->findAssetByName(AssetCache::AssetIdentity::Texture, floorTextureName);

            vclib::vcString ceilTextureName =
                vclib::stringFromBuffer(callData->arena, sectorContainer.ceilingTexture, 8);
            vclib::lower(ceilTextureName);

            // NOTE(KN): Special handling for when sky ceiling texture name is detected, in
            // which case the texture name is replaced with a 'special' texture name
            if (vclib::compareRaw(ceilTextureName, "f_sky1") == true)
            {
                ceilTextureName = vclib::stringFromRaw(callData->arena, "sky1");
            }            
            AssetCache::AssetBlock ceilTextureData =
                callData->assetStore->findAssetByName(AssetCache::AssetIdentity::Texture, ceilTextureName);                    
            
            r32 sectorLightLevel = vclib::scaledInt32ToReal(sectorContainer.lightLevel, 255.f);
            
            currentSector->floor.texture = floorTextureData.impl.tex;
            currentSector->floor.uScaling = 128.f;
            currentSector->floor.vScaling = 128.f;                    
            currentSector->floor.height = vclib::scaledInt32ToReal(sectorContainer.floorHeight, 8.f);
            currentSector->floor.shading = sectorLightLevel;
            currentSector->floor.skybox = false;
            currentSector->floor.applyVisibility = true;

            currentSector->ceiling.texture = ceilTextureData.impl.tex;
            currentSector->ceiling.height = vclib::scaledInt32ToReal(sectorContainer.ceilingHeight, 8.f);
            currentSector->ceiling.applyVisibility = false;

            // TODO(KN): Select correct sky texture based on level group
            if (vclib::compareRaw(ceilTextureName, "sky1") == true)
            {                
                currentSector->ceiling.uScaling = 4.f;
                currentSector->ceiling.vScaling = 1.5f;                    
                currentSector->ceiling.shading = 1.f;
                currentSector->ceiling.skybox = true;
            }
            else
            {
                currentSector->ceiling.uScaling = axisScale;
                currentSector->ceiling.vScaling = axisScale;                    
                currentSector->ceiling.shading = sectorLightLevel;
                currentSector->ceiling.skybox = false;
            }

            // NOTE(KN): As we don't actually support interaction with doors we detect
            // the door (same floor and ceiling height) and adjust the ceiling height to
            // show the door as 'mostly' opened
            if (currentSector->ceiling.height == currentSector->floor.height)
            {
                currentSector->ceiling.height = currentSector->floor.height + 8.f;
            }
        }
    }
}

void WADImport::attachWallTexturesForSector(GeometryDataBlock *callData, i32 sectorIndex)
{
    WorldObjects::SectorDef *currentSector = &(callData->levelData->sectorList[sectorIndex]);
    SectorVertexDef *currentVertexes = &(sectorVertexList[sectorIndex]);

    // Look for walls that have the blocking flag set and set our travel
    // sector value accordingly
    for (i32 loop = 0; loop < currentSector->wallCount; loop++)
    {
        WorldObjects::SectorDef::WallDef *wallAccess = &currentSector->lineSegment[loop];

        i32 matchSegment = currentVertexes->firstSegment;
        for (i32 loop = 0; loop < currentVertexes->segmentCount; loop++, matchSegment++)
        {
            u8 *segmentAddress;
            u8 *linedefAddress;
            u8 *sidedefAddress;
            findArraysFromSegmentIndex(callData, matchSegment,
                                       &segmentAddress, &linedefAddress, &sidedefAddress);
            
            SegmentLump segmentData = vclib::readElement<SegmentLump>(segmentAddress);
                    
            vfVec2 &lineStart = callData->levelData->vertexList[segmentData.startVertex];
            vfVec2 &lineEnd = callData->levelData->vertexList[segmentData.endVertex];

            if (distance(lineStart, wallAccess->start) < 0.05 &&
                distance(lineEnd, wallAccess->end) < 0.05)
            {
                // Matched a generated subsector edge to an explicit segment line
                // so we can now parse any additional information through the 
                // linedef->sidedef->sector linkage

                LineDefLump lineDef = vclib::readElement<LineDefLump>(linedefAddress);
                SideDefLump sideDef = vclib::readElement<SideDefLump>(sidedefAddress);
                    
                //
                vclib::vcString textureName = vclib::stringFromBuffer(callData->arena, sideDef.midTexture, 8);
                vclib::lower(textureName);
                AssetCache::AssetBlock textureData =
                    callData->assetStore->findAssetByName(AssetCache::AssetIdentity::Texture, textureName);

                vclib::vcString upperTextureName = vclib::stringFromBuffer(callData->arena, sideDef.upperTexture, 8);
                vclib::lower(upperTextureName);
                AssetCache::AssetBlock upperTextureData =
                    callData->assetStore->findAssetByName(AssetCache::AssetIdentity::Texture, upperTextureName);

                vclib::vcString lowerTextureName = vclib::stringFromBuffer(callData->arena, sideDef.lowerTexture, 8);
                vclib::lower(lowerTextureName);
                AssetCache::AssetBlock lowerTextureData =
                    callData->assetStore->findAssetByName(AssetCache::AssetIdentity::Texture, lowerTextureName);
                    
                wallAccess->highTexture.map = upperTextureData.impl.tex;
                wallAccess->highTexture.alignTopDown = false;
                wallAccess->lowTexture.map = lowerTextureData.impl.tex;
                wallAccess->lowTexture.alignTopDown = true;
                wallAccess->wallTexture.map = textureData.impl.tex;                    
                wallAccess->wallTexture.alignTopDown = true;
                wallAccess->xOffset = vclib::scaledInt32ToReal(sideDef.offsetX, 1.f);                    
                wallAccess->yOffset = vclib::scaledInt32ToReal(sideDef.offsetY, 1.f);
                wallAccess->uScaling = 128.f;
                wallAccess->vScaling = 8.f;

                // Block player
                if (lineDef.flags & 0x0001)
                {
                    wallAccess->blocking = true;
                }

                // Two-sided linedef
                if ((lineDef.flags & 0x0004) == 0)
                {
                    wallAccess->traverse = false;

                    // Texture unpegged
                    if (lineDef.flags & 0x0010)
                    {
                        wallAccess->wallTexture.alignTopDown = false;
                    }
                }
                else
                {
                    // Upper unpegged
                    if (lineDef.flags & 0x0008)
                    {
                        wallAccess->highTexture.alignTopDown = true;
                    }

                    // Lower unpegged
                    if (lineDef.flags & 0x0010)
                    {
                        wallAccess->lowTexture.alignTopDown = false;
                    }
                }
            }                                        
        }
    }
}

i32 WADImport::findSectorFromPositionUsingBSP(GeometryDataBlock *callData, vfVec2 worldPosition)
{
    vclib::vcString nodeAssetName = callData->assetNames[asIndex(LumpType::NODES)];
    
    AssetCache::RawBlockType nodeDetails;
    u8 *nodeBlock = selectRawAsset(nodeAssetName, callData->assetStore, &nodeDetails);
    i32 nodeCount = nodeDetails.length / sizeof(NodeLump);

    // Final node in list is the root node, so start with that one
    u8 *nodeLookup = &(nodeDetails.data)[(nodeCount - 1) * sizeof(NodeLump)];

    i32 sectorIdx = -1;    
    
    // Perform BSP traverse to determine sector for passed position
    bool traverseComplete = false;
    while(traverseComplete == false)
    {
        NodeLump partitionNode = vclib::readElement<NodeLump>(nodeLookup);

        r32 scaledX = vclib::scaledInt32ToReal(partitionNode.xCoord, axisScale);
        r32 scaledY = vclib::scaledInt32ToReal(partitionNode.yCoord, axisScale);
        vfVec2 pLineStart = {scaledX - callData->levelExtent.min.x, scaledY - callData->levelExtent.min.y};

        scaledX += vclib::scaledInt32ToReal(partitionNode.xDelta, axisScale);
        scaledY += vclib::scaledInt32ToReal(partitionNode.yDelta, axisScale);
        vfVec2 pLineEnd = {scaledX - callData->levelExtent.min.x, scaledY - callData->levelExtent.min.y};
        
        // Define direction of partition line as a clipping plane
        vfVec2 clippingPlane = pLineEnd - pLineStart;

        // Determine which side of the partition line the inside sector
        // point is situated
        vfVec2 samplePointFromPlane = pLineStart - worldPosition;
        r32 samplePointInsidePlane = crossProduct(clippingPlane, samplePointFromPlane);
        bool selectRightSide = (samplePointInsidePlane >= 0.f) ? true : false;
                                
        // Travel to the next node in the BSP based on which side of the partition
        // line our 'insidePoint' is
        u16 nextBSPNode = (selectRightSide == true) ? partitionNode.rightNode : partitionNode.leftNode;
        bool isSubsector = nextBSPNode & 0x8000 ? true : false;
        i32 subsectorId = nextBSPNode & 0x7FFF;

        if (isSubsector)
        {
            sectorIdx = subsectorId;
            traverseComplete = true;
        }
        else
        {
            nodeLookup = &(nodeDetails.data)[subsectorId * sizeof(NodeLump)];
        }                
    }

    return sectorIdx;
}

i32 WADImport::clipPolygonToLine(vfVec2 *vertexArray, i32 vertexCount, vfVec2 clipStart, vfVec2 clipEnd, bool rightSide)
{
    i32 buildCount = 0;
    vfVec2 *subsectorBuild = &(buildVertexList->sectorVerts[0]);

    vfVec2 clippingLine = clipEnd - clipStart;
    if (rightSide == false)
    {
        clippingLine = clipStart - clipEnd;
        clipStart = clipEnd;
    }                    
    
    // Clip each line of existing polygon into new polygon
    for (i32 loop = 0; loop < vertexCount; loop++)
    {
        vfVec2 &lineStart = vertexArray[loop];
        vfVec2 &lineEnd = (loop == vertexCount-1) ? vertexArray[0] : vertexArray[loop+1];
                    
        // Parametric form line test for intersection
        vfVec2 edgeDirection = lineEnd - lineStart;
        vfVec2 directionFromLine = clipStart - lineStart;

        r32 intersectNumer = crossProduct(directionFromLine, clippingLine);
        r32 intersectDenom = crossProduct(edgeDirection, clippingLine);
        r32 insideClipPlane = crossProduct(clippingLine, directionFromLine);

        bool intersecting = false;
                    
        if (intersectDenom)
        {
            r32 offset = intersectNumer / intersectDenom;
            if (offset >= 0.f && offset <= 1.f)
            {
                intersecting = true;
                if (insideClipPlane >= 0.f)
                {
                    subsectorBuild[buildCount++] = lineStart;
                    subsectorBuild[buildCount++] = lineStart + (edgeDirection * offset);
                }
                else
                {
                    subsectorBuild[buildCount++] = lineStart + (edgeDirection * offset);
                }
            }
        }

        if (intersecting == false && insideClipPlane >= 0.f)
        {
            subsectorBuild[buildCount++] = lineStart;
        }
    }

    // Copy resulting clipped polygon back into original passed vertex array
    i32 vertexResult = 0;
    for (i32 loop = 0; loop < buildCount; loop++)
    {
        vertexArray[vertexResult++] = subsectorBuild[loop];
    }

    return vertexResult;
}

void WADImport::simplifyStoredPolygon(SectorVertexDef *vertexData)
{
    i32 polygonVertexCount = vertexData->vertCount;
    
    // Compute centre of gravity for sector points
    vfVec2 pointAccum = {0.f,0.f};
    for (i32 loop = 0; loop < polygonVertexCount; loop++)
    {
        pointAccum = pointAccum + vertexData->sectorVerts[loop];        
    }
    vfVec2 polygonCentre = pointAccum * (1.f / static_cast<r32>(polygonVertexCount));

    // sort polygon vertexes into clockwise winding order with respect to the
    // calculated polygon centre of gravity
    for (;;)
    {
        // NOTE(KN): This is going to be a very naive bubble sort based
        // algorithm. But as it only happens on startup I don't think we
        // need to worry too much about the execution time when each subsector
        // is generally <16 points even allowing for vertex duplication out
        // of the clipping routine

        bool sortPassRequired = false;
                
        for (i32 loop = 0; loop < polygonVertexCount-1; loop++)
        {
            vfVec2 vertexA = vertexData->sectorVerts[loop] - polygonCentre;
            vfVec2 vertexB = vertexData->sectorVerts[loop+1] - polygonCentre;

            bool updateSortOrder = false;
                    
            if (vertexA.x < 0.f && vertexB.x >= 0.f)
            {
                updateSortOrder = true;
            }
            else if (vertexA.x == 0.f && vertexB.x == 0.f)
            {
                updateSortOrder = (vertexA.y >= vertexB.y) ? false : true;
            }
            else
            {
                r32 determinant = crossProduct(vertexA, vertexB);                    
                if (determinant > 0.f)
                {
                    updateSortOrder = true;
                }
            }
                        
            if (updateSortOrder == true)   
            {
                vfVec2 sortCopy = vertexData->sectorVerts[loop+1];
                vertexData->sectorVerts[loop+1] = vertexData->sectorVerts[loop];
                vertexData->sectorVerts[loop] = sortCopy;
                sortPassRequired = true;
            }                    
        }

        if (sortPassRequired == false)
            break;
    }            

    // TODO(KN): Should probably fall out here if the resulting polygon is
    // determined to have less than 3 vertexes
            
    // Simplify polygon (remove duplicate vertexes generated by clipping)
    buildVertexList->vertCount = 0;
    
    for (i32 loop = 0; loop < polygonVertexCount; loop++)
    {
        vfVec2 &lineStart = vertexData->sectorVerts[loop];
        vfVec2 &lineEnd = (loop == polygonVertexCount-1) ? vertexData->sectorVerts[0] : vertexData->sectorVerts[loop+1];

        if (distance(lineStart, lineEnd) > 0.005)
        {
            buildVertexList->sectorVerts[buildVertexList->vertCount++] = lineStart;
        }
    }

    vertexData->vertCount = 0;
    for (i32 loop = 0; loop < buildVertexList->vertCount; loop++)
    {
        vertexData->sectorVerts[vertexData->vertCount++] = buildVertexList->sectorVerts[loop];
    }
}

void WADImport::findArraysFromSegmentIndex(GeometryDataBlock *callData, i32 segmentIdx,
                                           u8 **segmentAddress, u8 **linedefAddress,
                                           u8 **sidedefAddress, u8 **sectorAddress)
{
    vclib::vcString segmentAssetName = callData->assetNames[asIndex(LumpType::SEGMENTS)];
    vclib::vcString lineDefAssetName = callData->assetNames[asIndex(LumpType::LINEDEFS)];
    vclib::vcString sideDefAssetName = callData->assetNames[asIndex(LumpType::SIDEDEFS)];
    vclib::vcString sectorAssetName = callData->assetNames[asIndex(LumpType::SECTORS)];

    u8 *segmentBlock = selectRawAsset(segmentAssetName, callData->assetStore);
    u8 *lineDefArray = selectRawAsset(lineDefAssetName, callData->assetStore);
    u8 *sideDefArray = selectRawAsset(sideDefAssetName, callData->assetStore);
    u8 *sectorArray = selectRawAsset(sectorAssetName, callData->assetStore);

    u8 *segmentLookup = &(segmentBlock)[segmentIdx * sizeof(SegmentLump)];    
    if (segmentAddress)
        *segmentAddress = segmentLookup;

    SegmentLump segmentData = vclib::readElement<SegmentLump>(segmentLookup);
    i16 linedefNumber = segmentData.lineDef;
    
    u8 *linedefLookup = &(lineDefArray)[linedefNumber * sizeof(LineDefLump)];
    if (linedefAddress)
        *linedefAddress = linedefLookup;

    LineDefLump lineDef = vclib::readElement<LineDefLump>(linedefLookup);
    i16 sidedefNumber = segmentData.direction == 0 ? lineDef.frontDef : lineDef.backDef;
    
    u8 *sidedefLookup = &(sideDefArray)[sidedefNumber * sizeof(SideDefLump)];
    if (sidedefAddress)
        *sidedefAddress = sidedefLookup;

    SideDefLump sideDef = vclib::readElement<SideDefLump>(sidedefLookup);
    i16 sectorNumber = sideDef.sector;
    
    u8 *sectorLookup = &(sectorArray)[sectorNumber * sizeof(SectorLump)];
    if (sectorAddress)
        *sectorAddress = sectorLookup;
}

u8 * WADImport::selectRawAsset(vclib::vcString assetName, AssetCache *assetStore, AssetCache::RawBlockType *assetDetails)
{
    AssetCache::RawBlockType rawBlock = {};
    
    AssetCache::AssetBlock blockResponse = assetStore->findAssetByName(AssetCache::AssetIdentity::DoomMap, assetName);    
    if (blockResponse.type == AssetCache::AssetType::RAW && blockResponse.impl.raw.length > 0)
    {
        rawBlock = blockResponse.impl.raw;
    }

    if (assetDetails)
    {
        *assetDetails = rawBlock;
    }
    
    return rawBlock.data;
}
