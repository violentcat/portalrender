#if !defined(WORLDREFERENCE_H)
#define WORLDREFERENCE_H

#include "Intrinsics.h"

namespace WorldRef
{
    // General geometry limits
    const i32 maxVertexCount    = 8192;
    const i32 maxSectorCount    = 1024;
    const i32 maxWallsPerSector = 128;

    // Player static parameters
    const r32 playerHeight = 5.f;
    const r32 stepLimit = 3.0f;

    // Player dynamic (motion) parameters
    const r32 accelRate = 15.0f; // m/s/s
    const r32 sideAccelRate = 12.0f; // m/s/s
    const r32 turnRate = 80.0f * vclib::to_radians; // radians/s
    const r32 jumpInitialVel = 16.0f; // m/s
    const r32 dragFactor = 7.0f;
    const r32 gravityAccel = -55.0f;
};

#endif
