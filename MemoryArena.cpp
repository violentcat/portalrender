
#include "MemoryArena.h"

template<typename allocator>
void ArenaAttachment<allocator>::attachStorage(void *memoryBlock, i32 blockLength)
{
    baseAddress = reinterpret_cast<u8 *>(memoryBlock);
    arenaSize = blockLength;
    usedSize = 0;
}

template<typename allocator>
void ArenaAttachment<allocator>::resetArena()
{
    usedSize = 0;
}

template<typename allocator>
u8 * ArenaAttachment<allocator>::pushBlock(i32 allocationSize)
{
    return static_cast<allocator&>(*this).allocate(allocationSize);
}

template<typename allocator>
void ArenaAttachment<allocator>::popBlock(u8 *block)
{
    static_cast<allocator&>(*this).release(block);
}

u8 * DefaultAllocator::allocate(i32 allocationSize)
{
    platformAssert(allocationSize < arenaSize);

    if (allocationSize + usedSize > arenaSize)
    {
        platformAssert(allocationSize == 0);
        return nullptr;
    }
        
    u8 *blockStart = &(baseAddress[usedSize]);
    usedSize += allocationSize;

    return blockStart;
}

