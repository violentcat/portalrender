
#include "GRPImport.h"
#include "WorldObjects.h"

namespace
{
#pragma pack(push,1)
    struct BlockIdent
    {
        u8 signature[12];
        i32 count;
    };

    struct ArtAsset
    {
        i32 versionNumber;
        i32 tileCount;
        i32 firstTileIndex;
        i32 lastTileIndex;
    };

    struct InitPlayer
    {
        i32 playerX;
        i32 playerY;
        i32 playerZ;
        u16 lookAngle;
        i16 initSector;
    };

    struct PlaneData
    {
        i16 texture;
        i16 slope;
        i8 shade;
        u8 palette;
        u8 panX;
        u8 panY;
    };

    struct GameData
    {
        i16 lotag;
        i16 hitag;
        i16 extra;
    };
    
    struct SectorData
    {
        i16 wallIndex;
        i16 wallCount;
        i32 ceilingHeight;
        i32 floorHeight;
        u16 ceilingFields;
        u16 floorFields;
        PlaneData ceiling;
        PlaneData floor;
        u8 visibility;
        u8 filler;
        GameData notUsed;
    };

    struct WallData
    {
        i32 xCoord;
        i32 yCoord;
        i16 nextWallIndex;
        i16 backWallIndex;
        i16 backSector;
        u16 wallFields;
        i16 texture;
        i16 maskTexture;
        i8 shade;
        u8 palette;
        u8 repeatX;
        u8 repeatY;
        u8 panX;
        u8 panY;
        GameData notUsed;
    };    
#pragma pack(pop)

    const r32 dukeAxisScale = 1024.f;
};

void GRPImport::makeImportType(MemoryArena *tempArena)
{
}

void GRPImport::unpackContainer(MemoryArena *tempArena, AssetCache::AssetBlock *containerAsset,
                                AssetCache *assetStore, MemoryArena *gameArena)
{
    // This is a GRP importer so make sure the asset is a GRP
    if (containerAsset->type != AssetCache::AssetType::GRP)
        return;
    
    u8 *dataPos = containerAsset->impl.raw.data;
    i32 dataToConsume = containerAsset->impl.raw.length;
    if (dataToConsume < 16)
        return;

    typedef AssetCache::AssetIdentity Ast_Id;
    typedef AssetCache::AssetType Ast_Type;
    typedef AssetCache::AssetBlock Ast_Block;        
    
    BlockIdent groupHeader = vclib::readElement<BlockIdent>(dataPos, &dataToConsume);
    
    vclib::vcString signature = vclib::stringFromBuffer(tempArena, groupHeader.signature, 12);
    if (vclib::compareRaw(signature, "KenSilverman", 12) == false)
    {
        return;
    }

    bool levelDataLoaded = false;
    
    // Test if the level data has already been loaded (from the filesystem) and if
    // so we won't try and load it from the group file
    Ast_Block checkLevel = assetStore->findAsset(Ast_Id::Duke3DMap);
    if (checkLevel.type == Ast_Type::RAW && checkLevel.impl.raw.length > 0)
    {
        levelDataLoaded = true;
    }

    i32 texturePalette = -1;
    i32 textureAssetStart = -1;
    i32 textureAssetEnd = -1;    
    i32 mapAssetLocation = -1;
    i32 mapAssetSize = 0;

    i32 currentFilePosition = 0;
    
    for (i32 loop = 0; loop < groupHeader.count; loop++)
    {
        if (dataToConsume < 16)
            break;

        BlockIdent fileHeader = vclib::readElement<BlockIdent>(dataPos, &dataToConsume);
        
        vclib::vcString filename = vclib::stringFromBuffer(tempArena, fileHeader.signature, 12);
        i32 fileSize = fileHeader.count;
        
        if (vclib::compareRaw(filename, "PALETTE.DAT") == true)
        {
            texturePalette = currentFilePosition;
        }
        else
        {
            vclib::vcString filetype = vclib::stringCopy(tempArena, filename);
            vclib::clipStringToRange(filetype, -4, -1);

            bool foundTextureEntry = vclib::compareRaw(filetype, ".ART");
            bool foundLevelEntry = vclib::compareRaw(filetype, ".MAP");

            if (foundTextureEntry)
            {
                if (textureAssetStart == -1)
                {
                    textureAssetStart = currentFilePosition;
                }
            }
            else
            {
                if (textureAssetStart != -1 && textureAssetEnd == -1)
                {
                    textureAssetEnd = currentFilePosition;
                }
            }

            if (levelDataLoaded == false && foundLevelEntry == true)
            {
                Ast_Block mapAsset = assetStore->findAsset(Ast_Id::MapName);
                if (mapAsset.type == Ast_Type::STR && mapAsset.impl.raw.length > 0)
                {
                    vclib::vcString selectedMap = vclib::stringFromBuffer(tempArena, mapAsset.impl.raw.data,
                                                                          mapAsset.impl.raw.length);

                    // Perform caseless comparison
                    vclib::lower(selectedMap);
                    vclib::lower(filename);

                    if(vclib::compare(selectedMap, filename) == true)
                    {
                        mapAssetLocation = currentFilePosition;
                        mapAssetSize = fileSize;
                        levelDataLoaded = true;
                    }
                }
            }            
        }

        currentFilePosition += fileSize;
    }

    // Finalise texture asset indexes in case the last texture asset was the last
    // entry in the group file
    if (textureAssetStart != -1 && textureAssetEnd == -1)
    {
        textureAssetEnd = currentFilePosition;
    }

    // If a map asset has been identified for load then add it to the internal
    // asset list with the appropriate asset Identity and type
    if (mapAssetLocation != -1)
    {
        Ast_Block *extractedMap = assetStore->createAssetBlock(gameArena,
                                                               Ast_Id::Duke3DMap,
                                                               Ast_Type::RAW);
        extractedMap->impl.raw.data = dataPos + mapAssetLocation;
        extractedMap->impl.raw.length = mapAssetSize;
    }

    if (texturePalette == -1 || textureAssetStart == -1 || textureAssetEnd == -1)
        return;

    // Load texture palette data into lookup table
    u32 *paletteLookup = tempArena->pushArray<u32>(256);    
    u8 *paletteAddr = dataPos + texturePalette;
    
    for (i32 loop = 0; loop < 256; loop++)
    {
        // NOTE(KN): The entries in the palette are 6-bit RGB so we need to
        // shift each component up by additional 2-bits to align with final
        // ARGB32 texture format
        u32 resultColour = (*paletteAddr++) << 18;
        resultColour |= (*paletteAddr++) << 10;
        resultColour |= (*paletteAddr++) << 2;
        paletteLookup[loop] = resultColour;
    }

    
    u8 *textureAddr = dataPos + textureAssetStart;

    while (textureAddr < (dataPos + textureAssetEnd))
    {
        ArtAsset assetData = vclib::readElement<ArtAsset>(textureAddr);
        if (assetData.versionNumber != 1)
            return;
        
        i32 firstArtIndex = assetData.firstTileIndex;
        i32 lastArtIndex = assetData.lastTileIndex;    
        i32 artCount = lastArtIndex - firstArtIndex + 1;
        Ast_Block *initialTexture = nullptr;

        for (i32 loop = 0; loop < artCount; loop++)
        {
            Ast_Block *processTexture = assetStore->createAssetBlock(gameArena,
                                                                     Ast_Id::IndexedTexture,
                                                                     Ast_Type::TEX);
            processTexture->index = firstArtIndex + loop;                      
            processTexture->impl.tex.width = vclib::readElement<i16>(textureAddr);

            if (processTexture->index == firstArtIndex)
            {
                // Store the first texture indexed from this texture block so we can
                // revisit them all to complete the contained information
                initialTexture = processTexture;
            }
        }
        
        Ast_Block *listIter = initialTexture;
        for (i32 loop = 0; loop < artCount; loop++)
        {
            listIter->impl.tex.height = vclib::readElement<i16>(textureAddr);
            listIter = listIter->next;
        }

        for (i32 loop = 0; loop < artCount; loop++)
        {
            i32 tileAttributes = vclib::readElement<i32>(textureAddr);
        }
        
        listIter = initialTexture;
        for (i32 loop = 0; loop < artCount; loop++)
        {
            listIter->impl.tex.data = gameArena->pushArray<u32>(listIter->impl.tex.width * listIter->impl.tex.height);

            for (i32 uLoop = 0; uLoop < listIter->impl.tex.height; uLoop++)
            {
                u32 *textureLine = &(listIter->impl.tex.data[uLoop*listIter->impl.tex.width]);
            
                for (i32 vLoop = 0; vLoop < listIter->impl.tex.width; vLoop++)
                {
                    u32 texelColour = paletteLookup[*textureAddr++];
                    textureLine[vLoop] = texelColour;
                }
            }
        
            listIter = listIter->next;
        }
    }    
}

void GRPImport::unpackLevelData(MemoryArena *tempArena, AssetCache *assetStore, WorldObjects *levelData)
{
    typedef AssetCache::AssetIdentity Asset_Id;
    typedef AssetCache::AssetType Asset_Type;

    // This is a GRP importer so make sure there are Duke3d level assets
    AssetCache::AssetBlock dukeType = assetStore->findAsset(Asset_Id::Duke3DMap);
    if (!(dukeType.type == Asset_Type::RAW && dukeType.impl.raw.length > 0))
        return;

    levelData->player = {};
    levelData->vertexCount = 0;
    levelData->sectorCount = 0;
    for (i32 loop = 0; loop < WorldRef::maxSectorCount; loop++)
    {
        levelData->sectorList[loop].wallCount = 0;
    }
    
    u8 *buffer = dukeType.impl.raw.data;
    i32 remaining = dukeType.impl.raw.length;

    u32 mapVersion = vclib::readElement<u32>(buffer);
    if (mapVersion != 7)
        return;

    InitPlayer playerData = vclib::readElement<InitPlayer>(buffer);
            
    // Scale to internal floating point representation
    // NOTE(KN): Y values must first be inverted to match our 'standard' internal format
    levelData->player.position.x = vclib::scaledInt32ToReal(playerData.playerX, dukeAxisScale);
    levelData->player.position.y = vclib::scaledInt32ToReal(0xFFFF, playerData.playerY, dukeAxisScale);

    // All angles are between 0..2047 inclusive, 0 angle is the same as our
    // internal format, however the rotation is in the opposite direction so
    // we need to invert it before scaling into radians
    r32 reorientAngle = 2048.f - static_cast<r32>(playerData.lookAngle);
    levelData->player.orientation = reorientAngle * vclib::two_piflt / 2048.f;

    levelData->player.sector = playerData.initSector;
    
    // Load the sector count
    levelData->sectorCount = vclib::readElement<i16>(buffer);

    // .. and temporarily skip over the sector definitions so we can
    // first read in the wall / vertex information
    u8 *sectorLoadBuffer = buffer;
    for (i32 loop = 0; loop < levelData->sectorCount; loop++)
    {
        SectorData skipSector = vclib::readElement<SectorData>(buffer);
    }

    levelData->vertexCount = vclib::readElement<i16>(buffer);
    u8 *wallLoadBuffer = buffer;

    // Initialise scaling centre point for processing vertexes
    r32 scaledVertexCentre = vclib::scaledInt32ToReal(0x7FFF, 0, dukeAxisScale);
    vfVec2 minVertex = {scaledVertexCentre,scaledVertexCentre};
    vfVec2 maxVertex = minVertex;

    // Scan through all vertexes to find the suitable min and max extents
    for (i32 loop = 0; loop < levelData->vertexCount; loop++)
    {
        WallData vertexRead = vclib::readElement<WallData>(buffer);

        // NOTE(KN): Y Vertex points needs to be inverted to get the
        // map co-ordinates into our 'standard' internal format
        r32 scaledX = vclib::scaledInt32ToReal(vertexRead.xCoord, dukeAxisScale);
        r32 scaledY = vclib::scaledInt32ToReal(0xFFFF, vertexRead.yCoord, dukeAxisScale);
        levelData->vertexList[loop] = {scaledX, scaledY};

        minVertex.x = vclib::minValue(minVertex.x, levelData->vertexList[loop].x);
        minVertex.y = vclib::minValue(minVertex.y, levelData->vertexList[loop].y);
        maxVertex.x = vclib::maxValue(maxVertex.x, levelData->vertexList[loop].x);
        maxVertex.y = vclib::maxValue(maxVertex.y, levelData->vertexList[loop].y);                
    }

    // .. and perform a final pass over the vertexes to offset them
    // based on the min bounds identified in the previous step
    for (i32 loop = 0; loop < levelData->vertexCount; loop++)
    {
        levelData->vertexList[loop] = levelData->vertexList[loop] - minVertex;
    }

    // Data load is initialised so continue on with loading the sectors
    // and associated wall data
    for (i32 loop = 0; loop < levelData->sectorCount; loop++)
    {
        SectorData readSector = vclib::readElement<SectorData>(sectorLoadBuffer);
        WorldObjects::SectorDef *addSector = &(levelData->sectorList[loop]);

        addSector->wallCount = readSector.wallCount;

        addSector->ceiling.height = vclib::scaledInt32ToReal(0x4000, readSector.ceilingHeight, 2048.f);
        addSector->ceiling.texture = assetStore->findTexture(readSector.ceiling.texture);
        addSector->ceiling.shading = vclib::scaledInt32ToReal(readSector.ceiling.shade, -32.f) + 1.f;
        addSector->ceiling.skybox = (readSector.ceilingFields & 0x0001) ? true : false;
        addSector->ceiling.applyVisibility = false;
        addSector->ceiling.uScaling = 64.0f;
        addSector->ceiling.vScaling = 64.0f;

        // Change scaling values for flat mapped sky texture
        if (addSector->ceiling.skybox == true)
        {
            addSector->ceiling.uScaling = 8.0f;
            addSector->ceiling.vScaling = 1.5f;
        }

        addSector->floor.height = vclib::scaledInt32ToReal(0x4000, readSector.floorHeight, 2048.f);
        addSector->floor.texture = assetStore->findTexture(readSector.floor.texture);
        addSector->floor.shading = vclib::scaledInt32ToReal(readSector.floor.shade, -32.f) + 1.f;
        addSector->floor.skybox = false;
        addSector->floor.applyVisibility = true;
        addSector->floor.uScaling = 64.0f;        
        addSector->floor.vScaling = 64.0f;        

        i16 wallIndex = readSector.wallIndex;
        
        for (i32 wallLoop = 0; wallLoop < addSector->wallCount; wallLoop++)
        {
            u8 *wallBufferOffset = wallLoadBuffer + (wallIndex * sizeof(WallData));
                
            WallData readWall = vclib::readElement<WallData>(wallBufferOffset);
            WorldObjects::SectorDef::WallDef *addWall = &(addSector->lineSegment[wallLoop]);

            if (wallLoop > 0)
            {
                WorldObjects::SectorDef::WallDef *previousWall = &(addSector->lineSegment[wallLoop-1]);
                previousWall->end = levelData->vertexList[wallIndex];
            }
            else                
            {
                // Ensure sequence of walls is closed by connecting the last vertex
                // back to first
                WorldObjects::SectorDef::WallDef *previousWall = &(addSector->lineSegment[addSector->wallCount-1]);
                previousWall->end = levelData->vertexList[wallIndex];
            }
            
            addWall->start = levelData->vertexList[wallIndex];
            addWall->travelToSector = readWall.backSector;
            addWall->lowTexture.map = assetStore->findTexture(readWall.texture);
            addWall->lowTexture.alignTopDown = true;
            addWall->highTexture.map = assetStore->findTexture(readWall.texture);
            addWall->highTexture.alignTopDown = true;
            addWall->shading = vclib::scaledInt32ToReal(readWall.shade, -32.f) + 1.f;
            addWall->uScaling = readWall.repeatX;
            addWall->vScaling = readWall.repeatY;
            addWall->blocking = false;
            addWall->traverse = true;

            if (addWall->travelToSector == -1)
            {
                addWall->wallTexture.map = addWall->highTexture.map;
                addWall->wallTexture.alignTopDown = true;
                addWall->blocking = true;
            }

            wallIndex = readWall.nextWallIndex;
        }

        // Post-process walls to compute wall length for scaling adjustment
        for (i32 wallLoop = 0; wallLoop < addSector->wallCount; wallLoop++)
        {
            WorldObjects::SectorDef::WallDef *currentWall = &(addSector->lineSegment[wallLoop]);
            currentWall->wallLength = distance(currentWall->end, currentWall->start);
            currentWall->uScaling = currentWall->uScaling / currentWall->wallLength * 8.f;
        }            
    }

    levelData->player.position = levelData->player.position - minVertex;
    vfVec2 extent = maxVertex - minVertex;
        
    levelData->worldExtent = {extent.x,extent.y};
    levelData->worldExtent = levelData->worldExtent * 2.0f;
    levelData->viewDistance = 16.0f;
    levelData->verticalFov = 162.f * vclib::to_radians;
    levelData->horizontalFov = 90.f * vclib::to_radians;
}

