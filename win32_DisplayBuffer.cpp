
#include "win32_DisplayBuffer.h"

win32DisplayBuffer::Dimensions win32DisplayBuffer::GetDeviceDimensions(HWND windowHandle)
{
    Dimensions result;
    
    RECT clientRect;
    GetClientRect(windowHandle, &clientRect);
    result.width = clientRect.right - clientRect.left;
    result.height = clientRect.bottom - clientRect.top;
    return result;
}

void win32DisplayBuffer::createDisplayBuffer(i32 width, i32 height)
{
    if (displayBuffer.pixelData)
    {
        HeapFree(GetProcessHeap(), 0, displayBuffer.pixelData);
    }

    displayBuffer.width = width;
    displayBuffer.height = height;
    displayBuffer.bytesPerPixel = 4;
    displayBuffer.pitch = width * displayBuffer.bytesPerPixel;

    displayBuffer.bitmapInfo.bmiHeader.biSize = sizeof(displayBuffer.bitmapInfo.bmiHeader);
    displayBuffer.bitmapInfo.bmiHeader.biWidth = width;
    displayBuffer.bitmapInfo.bmiHeader.biHeight = height;
    displayBuffer.bitmapInfo.bmiHeader.biPlanes = 1;
    displayBuffer.bitmapInfo.bmiHeader.biBitCount = 32;
    displayBuffer.bitmapInfo.bmiHeader.biCompression = BI_RGB;

    i32 bitmapSize = (width * height) * displayBuffer.bytesPerPixel;
    displayBuffer.pixelData = HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, bitmapSize);
}

void win32DisplayBuffer::updateDeviceFromBuffer(HDC deviceContext, i32 width, i32 height)
{
    StretchDIBits(deviceContext,
                  0, 0, displayBuffer.width, displayBuffer.height,
                  0, 0, displayBuffer.width, displayBuffer.height,
                  displayBuffer.pixelData,
                  &displayBuffer.bitmapInfo,
                  DIB_RGB_COLORS,
                  SRCCOPY);    
}
