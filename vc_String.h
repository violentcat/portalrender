#if !defined(VC_STRING_H)
#define VC_STRING_H

// TODO(KN):
// Container manipulations
// Find / Replace (case sensistive on and off)
// Internationalisation
// Conversion to and from fundamental types
// UTF-8 / UTF-16 support etc
// Thread safe

#include "MemoryArena.h"

namespace vclib
{
    static const i32 StringPathLimit = 1024;
    #define STRING_DEF(s) ((StringDef *)s - 1)

    struct StringDef
    {
        i32 length;
        i32 capacity;       
    };
    typedef char *vcString;


    i32 capacity(vcString str);
    i32 length(vcString str);
    void setLength(vcString str, i32 len);

    vcString stringCreate(MemoryArena *store, i32 length);
    vcString stringCopy(MemoryArena *store, vcString str);
    vcString stringFromRaw(MemoryArena *store, char *raw);
    vcString stringFromBuffer(MemoryArena *store, u8 *buffer, i32 length);
};

#endif
