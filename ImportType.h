#if !defined(IMPORTTYPE_H)
#define IMPORTTYPE_H

#include "AssetCache.h"

class AssetCache;
class WorldObjects;

class ImportType
{
public:

    virtual void makeImportType(MemoryArena *tempArena) = 0;
    
    virtual void unpackContainer(MemoryArena *tempArena, AssetCache::AssetBlock *containerAsset,
                                 AssetCache *assetStore, MemoryArena *gameArena) = 0;

    virtual void unpackLevelData(MemoryArena *tempArena, AssetCache *assetStore,
                                 WorldObjects *levelData) = 0;    
};

#endif
