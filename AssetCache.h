#if !defined(ASSETCACHE_H)
#define ASSETCACHE_H

class ImportType;

class AssetCache
{
public:

    enum class AssetIdentity : i32
    {
        Texture,
        PortalTexture,
        FloorTexture,
        CeilingTexture,
        LevelData,
        Duke3DGroup,
        Duke3DMap,
        DoomWAD,
        DoomMap,
        IndexedTexture,
        MapName
    };

    i32 asType(AssetIdentity type) {return static_cast<i32>(type);}

    enum AssetType
    {
        RAW,
        PPM,
        GRP,
        WAD,
        TEX,
        STR
    };

    struct RawBlockType
    {
        u8 *data;
        i32 length;
    };

    struct PPMBlockType
    {
        u8 *data;
        i32 length;
    };

    struct TexBlockType
    {
        // Texture is stored as a series of ARGB pixels
        u32 *data;
        i32 width;
        i32 height;
    };
    
    struct AssetBlock
    {
        AssetIdentity id;
        AssetType type;
        vclib::vcString name;
        i32 index;
        AssetBlock *next;

        union
        {
            RawBlockType raw;
            PPMBlockType ppm;
            TexBlockType tex;
        } impl;        
    };
    

public:

    void linkRawAssetData(MemoryArena *arena, u8 *data, i32 dataSize,
                          AssetIdentity id, AssetType type);
    void linkRawAssetString(MemoryArena *arena, vclib::vcString strData, AssetIdentity id);

    void convertRawDataToInternal(MemoryArena *arena, MemoryArena *unpack, ImportType *assetImporter);

    AssetBlock *createAssetBlock(MemoryArena *arena, AssetIdentity id, AssetType type);    
    AssetBlock findAsset(AssetIdentity id, i32 index = -1);
    AssetBlock findAssetByName(AssetIdentity id, vclib::vcString name);
    TexBlockType findTexture(i32 index);


private:

    enum {HDR_ELEMENT_CNT = 3};
    struct PPMHeaderBlock
    {
        i32 width;
        i32 height;
        i32 maxPixelValue;
    };

    struct AssetList
    {
        AssetBlock *head = nullptr;
        AssetBlock *tail = nullptr;        
    };

    AssetList rawAssetList;
    AssetList internalAssetList;
    

private:

    AssetBlock *addAssetToList(MemoryArena *arena, AssetList *list,
                               AssetIdentity id, AssetType type);
    AssetBlock *accessExistingAsset(AssetIdentity id, i32 index);

    void unpackPPMFormatAsset(MemoryArena *arena, AssetBlock *asset);

    bool isEndOfLine(char token);
    bool isWhitespace(char token);
    bool isNumeric(char token);    
    i32 readPPMHeader(MemoryArena *arena, PPMHeaderBlock *header, u8 *data, i32 length);
    i32 readElement(MemoryArena *arena, u8 *data, i32 length, vclib::vcString *element);

    void unpackBinaryPPM(u8 *data, i32 length, TexBlockType *texture);
};

#endif
