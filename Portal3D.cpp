
#include <windows.h>
#include <xinput.h>
#include "PlatformTypes.h"
#include "vc_String.cpp"
#include "vc_StringFunctions.cpp"
#include "MemoryArena.cpp"
#include "StackArena.cpp"
#include "win32_DisplayBuffer.cpp"
#include "WorldObjects.cpp"
#include "RenderScene.cpp"
#include "AssetCache.cpp"
#include "CustomImport.cpp"
#include "WADImport.cpp"
#include "GRPImport.cpp"

DYNAMIC_CALLDEF (DWORD, ERROR_DEVICE_NOT_CONNECTED, XInputGetState, DWORD, XINPUT_STATE *)
DYNAMIC_CALLDEF (DWORD, ERROR_DEVICE_NOT_CONNECTED, XInputSetState, DWORD, XINPUT_VIBRATION *)

struct LoadConfiguration
{
    enum AssetType
    {
        CUSTOM,
        DUKE3D,
        DOOM,
        UNDEFINED
    };

    AssetType type;
    vclib::vcString mapName;
};

char *searchPathLiterals[] = {"assets", "..\\assets", nullptr};
win32DisplayBuffer renderArea;

// Unpacker objects
CustomImport customImportType;
WADImport doomImportType;
GRPImport duke3dImportType;        


LRESULT CALLBACK WindowMessageHandler(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{    
    LRESULT handlerResult = 0;
    
    switch(message)
    {
        case WM_ACTIVATEAPP:
        {
        } break;
        
        case WM_DESTROY:            
        {
        } break;
        
        case WM_CLOSE:
        {
            PostQuitMessage(0);
        } break;
        
        case WM_SIZE:
        {
        } break;
        
        case WM_PAINT:
        {
            win32DisplayBuffer::Dimensions windowDim = renderArea.GetDeviceDimensions(windowHandle);
            
            PAINTSTRUCT paintInfo;
            HDC deviceContext = BeginPaint(windowHandle, &paintInfo);
            renderArea.updateDeviceFromBuffer(deviceContext, windowDim.width, windowDim.height);
            EndPaint(windowHandle, &paintInfo);
            
        } break;
        
        default:
        {
            handlerResult =  DefWindowProcA(windowHandle, message, wParam, lParam);
        } break;
    }
    
    return handlerResult;
}

// TODO(KN): Replace char * with own string implementation as parameter
i32 LoadAssetFromFileSystem(char *assetFilename, MemoryArena *persistStore, u8 **assetData)
{
    platformAssert(assetData);
    i32 finalAssetSize = 0;
    
    if (assetFilename)
    {
        i32 pathIndex = 0;
        bool readComplete = false;
        
        while (readComplete == false)
        {
            char *pathSelection = searchPathLiterals[pathIndex++];
            if (pathSelection == nullptr)
                break;
            
            // Create a new string to hold the current asset path and initialise
            // it with the current directory
            vclib::vcString pathToAsset = vclib::stringCreate(persistStore, vclib::StringPathLimit);
            DWORD length = GetCurrentDirectoryA(vclib::capacity(pathToAsset), pathToAsset);
            vclib::setLength(pathToAsset, length);

            // TODO(KN): This seriously needs to be replaced with something variadic or a
            // parameter pack in the modern world. Gah!
            vclib::concatenateRaw(pathToAsset, "\\");
            vclib::concatenateRaw(pathToAsset, pathSelection);
            vclib::concatenateRaw(pathToAsset, "\\");
            vclib::concatenateRaw(pathToAsset, assetFilename);

            HANDLE fileHandle = CreateFileA(pathToAsset,
                                            GENERIC_READ,
                                            FILE_SHARE_READ,
                                            0,
                                            OPEN_EXISTING,
                                            0,
                                            0);

            if (fileHandle != INVALID_HANDLE_VALUE)
            {
                LARGE_INTEGER fileSize;
                if (GetFileSizeEx(fileHandle, &fileSize))
                {
                    // NOTE(KN): Seeing as the memory arena system only support 32bit sized
                    // allocations there's no point trying to read file larger than would fit
                    // into that allocation size
                    if (fileSize.u.HighPart == 0)
                    {
                        i32 smallFileSize = fileSize.u.LowPart;
                        u8 *allocatedBlock = persistStore->pushBlock(smallFileSize);
                    
                        DWORD readCompleteSize;
                        ReadFile(fileHandle, allocatedBlock, smallFileSize, &readCompleteSize, 0);

                        if (readCompleteSize)
                        {
                            *assetData = allocatedBlock;
                            finalAssetSize = readCompleteSize;

                            readComplete = true;
                        }
                    }
                }
            }            
        }        
    }

    return finalAssetSize;
}

bool CheckAssetAndLink(AssetCache *systemAssets,
                       char *assetFilename, MemoryArena *persistStore,
                       AssetCache::AssetIdentity id, AssetCache::AssetType type)
{
    u8 *data;
    i32 dataSize;
    
    dataSize = LoadAssetFromFileSystem(assetFilename, persistStore, &data);
    if (dataSize > 0)
    {
        systemAssets->linkRawAssetData(persistStore, data, dataSize, id, type);
        return true;
    }

    return false;
}

ImportType * LoadAssetsIntoCache(AssetCache *systemAssets, MemoryArena *persistStore, LoadConfiguration &config)
{
    typedef AssetCache::AssetIdentity Ast_Id;
    typedef AssetCache::AssetType Ast_Type;

    ImportType *selectedImporter = nullptr;

    if (config.type == LoadConfiguration::DUKE3D)
    {
        CheckAssetAndLink(systemAssets, "duke3d.grp", persistStore, Ast_Id::Duke3DGroup, Ast_Type::GRP);

        if (!CheckAssetAndLink(systemAssets, config.mapName, persistStore,
                               Ast_Id::Duke3DMap, Ast_Type::RAW))
        {
            // No map of that name found in the filesystem so store it in the string
            // table so the asset system can try and find it in the group container
            systemAssets->linkRawAssetString(persistStore, config.mapName, Ast_Id::MapName);
        }

        selectedImporter = &duke3dImportType;        
    }
    else if (config.type == LoadConfiguration::DOOM)
    {
        CheckAssetAndLink(systemAssets, "doom1.wad", persistStore, Ast_Id::DoomWAD, Ast_Type::WAD);

        if (!CheckAssetAndLink(systemAssets, config.mapName, persistStore,
                               Ast_Id::DoomMap, Ast_Type::RAW))
        {
            // No map of that name found in the filesystem so store it in the string
            // table so the asset system can try and find it in the WAD container
            systemAssets->linkRawAssetString(persistStore, config.mapName, Ast_Id::MapName);
        }

        selectedImporter = &doomImportType;
    }
    else if (config.type == LoadConfiguration::CUSTOM)
    {
        CheckAssetAndLink(systemAssets, "wall2.ppm",  persistStore, Ast_Id::Texture, Ast_Type::PPM);
        CheckAssetAndLink(systemAssets, "wall3.ppm",  persistStore, Ast_Id::PortalTexture, Ast_Type::PPM);
        CheckAssetAndLink(systemAssets, "floor2.ppm", persistStore, Ast_Id::FloorTexture, Ast_Type::PPM);
        CheckAssetAndLink(systemAssets, "ceil2.ppm",  persistStore, Ast_Id::CeilingTexture, Ast_Type::PPM);

        if (!CheckAssetAndLink(systemAssets, config.mapName, persistStore,
                               Ast_Id::LevelData, Ast_Type::RAW))
        {
            // If the specified custom map could not be loaded then revert to
            // default name. There is no fallback for the custom map format
            // beyond this
            CheckAssetAndLink(systemAssets, "map-clear.txt", persistStore, Ast_Id::LevelData, Ast_Type::RAW);
        }

        selectedImporter = &customImportType;
    }

    return selectedImporter;
}

void ProcessDigitalControllerState(RenderScene::ControllerState *inputState, bool *runningState)
{
    for (i32 loop = 0; loop < countOf(inputState->keyList); loop++)
    {
        inputState->keyList[loop].keyTransitions = 0;
    }

    inputState->digitalInput = true;
    inputState->controllerValid = true;
    
    MSG Message;
    while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        switch(Message.message)
        {
            case WM_QUIT:
            {
                *runningState = false;
            } break;
            
            case WM_KEYDOWN:
            case WM_KEYUP:
            {
                WPARAM keyCode = Message.wParam;
                bool isKeyDown = (Message.lParam & (1 << 31)) == 0 ? true : false;
                bool wasKeyDown = (Message.lParam & (1 << 30)) == 0 ? false : true;
                
                if (isKeyDown != wasKeyDown)
                {
                    switch(keyCode)
                    {
                        case 0x26:
                            inputState->moveFwd.isKeyDown = isKeyDown;
                            inputState->moveFwd.keyTransitions++;
                            break; // Up
                        
                        case 0x28:
                            inputState->moveBack.isKeyDown = isKeyDown;
                            inputState->moveBack.keyTransitions++;
                            break; // Down
                        
                        case 0x25:
                            inputState->turnLeft.isKeyDown = isKeyDown;
                            inputState->turnLeft.keyTransitions++;
                            break; // Left
                        
                        case 0x27:
                            inputState->turnRight.isKeyDown = isKeyDown;
                            inputState->turnRight.keyTransitions++;
                            break; // Right

                        case 0x5A:
                            inputState->moveLeft.isKeyDown = isKeyDown;
                            inputState->moveLeft.keyTransitions++;
                            break; // z

                        case 0x58:
                            inputState->moveRight.isKeyDown = isKeyDown;
                            inputState->moveRight.keyTransitions++;
                            break; // x

                        case 0x20:
                            inputState->jump.isKeyDown = isKeyDown;
                            inputState->jump.keyTransitions++;
                            break; // space

                        case 0x43:
                            inputState->crouch.isKeyDown = isKeyDown;
                            inputState->crouch.keyTransitions++;
                            break; // c

                        case 0xBB:
                            inputState->increaseDepth.isKeyDown = isKeyDown;
                            inputState->increaseDepth.keyTransitions++;
                            break; // +

                        case 0xBD:
                            inputState->decreaseDepth.isKeyDown = isKeyDown;
                            inputState->decreaseDepth.keyTransitions++;
                            break; // -
                    };
                }
            }
            break;
            
            default:
            {
                TranslateMessage(&Message);
                DispatchMessageA(&Message);
            } break;
        }
    }
}

r32 ProcessAnalogStick(i32 stickValue, i32 deadZone)
{
    r32 normalisedResult = 0.0f;
        
    if (stickValue > deadZone)
    {
        normalisedResult = static_cast<r32>(stickValue - deadZone) / static_cast<r32>(32767 - deadZone);
    }
    else if (stickValue < -deadZone)
    {
        normalisedResult = static_cast<r32>(stickValue + deadZone) / static_cast<r32>(32768 - deadZone);
    }

    return normalisedResult;
}

void ProcessAnalogControllerState(RenderScene::ControllerState *inputState, i32 selectedController)
{
    inputState->digitalInput = false;

    XINPUT_STATE stateRead;
    if (XInputGetState_Dynamic(selectedController, &stateRead) == ERROR_SUCCESS)
    {
        inputState->controllerValid = true;

        XINPUT_GAMEPAD *padData = &(stateRead.Gamepad);
        inputState->leftStickX = ProcessAnalogStick(padData->sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
        inputState->leftStickY = ProcessAnalogStick(padData->sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
        inputState->rightStickX = ProcessAnalogStick(padData->sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
        inputState->rightStickY = ProcessAnalogStick(padData->sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);

        // Button states
        inputState->jump.isKeyDown = (padData->wButtons & XINPUT_GAMEPAD_A) ? true : false;
    }
    else
    {
        inputState->controllerValid = false;
    }
}

void InitControllerDevices(i32 *selectedController)
{
    HMODULE loadHandle = LoadLibraryA("XINPUT1_4.DLL");
    if (!loadHandle)
    {
        loadHandle = LoadLibraryA("XINPUT1_3.DLL");
    }
    if (!loadHandle)
    {
        loadHandle = LoadLibraryA("XINPUT9_1_0.DLL");        
    }

    if (loadHandle)
    {
        DYNAMIC_CALLLOAD(loadHandle, XInputGetState)
        DYNAMIC_CALLLOAD(loadHandle, XInputSetState)
    }

    // Determine whether we have an active controller
    for (i32 loop = 0; loop < XUSER_MAX_COUNT; loop++)
    {
        XINPUT_STATE stateRead;
        if (XInputGetState_Dynamic(loop, &stateRead) == ERROR_SUCCESS)
        {
            *selectedController = loop;
            break;
        }
    }
}

void ProgramLoop(HWND windowHandle, LoadConfiguration &config)
{
    bool programLoopRunning = true;
    
    if (timeBeginPeriod(1) != TIMERR_NOERROR)
    {
        // If can't raise the scheduler interrupt rate then no choice but to implement
        // simulation period delay as a busy loop
        // TODO(KN): Implement busy loop
        return;
    }

    // Initialise high resolution timing for frame interval
    LARGE_INTEGER performanceFreqResult;
    QueryPerformanceFrequency(&performanceFreqResult);
    r64 timerFrequency = static_cast<r64>(performanceFreqResult.QuadPart);

    // Initialise XInput for gamepads
    i32 selectedController = -1;
    InitControllerDevices(&selectedController);
    
    // Initialise controller states for keyboard and gamepad
    RenderScene::ControllerState keyState = {};
    RenderScene::ControllerState gamepadState = {};    

    i32 smallArenaSize = 16*1048576;
    i32 largeArenaSize = 256*1048576;
    
    // Allocate persistent storage Memory Arena
    MemoryArena persistentArena;
    persistentArena.attachStorage(HeapAlloc(GetProcessHeap(), 0, largeArenaSize), largeArenaSize);

    // Allocate unpacking (temporary) storage Memory Arena
    MemoryArena unpackArena;
    unpackArena.attachStorage(HeapAlloc(GetProcessHeap(), 0, smallArenaSize), smallArenaSize);

    // Allocate frame (temporary) storage Memory Arena
    MemoryArena frameArena;
    frameArena.attachStorage(HeapAlloc(GetProcessHeap(), 0, smallArenaSize), smallArenaSize);
    
    // Perform raw asset loads from file system as needed for source type
    AssetCache systemAssets;
    ImportType *assetImporter = LoadAssetsIntoCache(&systemAssets, &persistentArena, config);

    // Configure the selected asset importer for use by the raw asset conversion
    assetImporter->makeImportType(&unpackArena);
    systemAssets.convertRawDataToInternal(&persistentArena, &unpackArena, assetImporter);
    
    // Initialise geometry storage and unpack any assets relating to level structure
    WorldObjects geometryData(&persistentArena);
    assetImporter->unpackLevelData(&unpackArena, &systemAssets, &geometryData);
    
    RenderScene world;
    world.initRenderBuffer(renderArea.displayBuffer.pixelData,
                           renderArea.displayBuffer.width,
                           renderArea.displayBuffer.height,
                           renderArea.displayBuffer.pitch);        

    world.setCurrentLevelGeometry(&geometryData);

    r64 frameTimeElapsed = 0.0;
    r64 frameTimePeriod = 1000000.0 / 30.0;
    r32 frameSimInterval = static_cast<r32>(frameTimePeriod / 1000000.0);
    LARGE_INTEGER cycleInitTime;
    LARGE_INTEGER frameCompleteTime;
    QueryPerformanceCounter(&cycleInitTime);
    
    while (programLoopRunning)
    {
        ProcessDigitalControllerState(&keyState, &programLoopRunning);
        ProcessAnalogControllerState(&gamepadState, selectedController);
        
        HDC deviceContext = GetDC(windowHandle);
        win32DisplayBuffer::Dimensions windowDim = renderArea.GetDeviceDimensions(windowHandle);

        frameArena.resetArena();
        world.updateSimulationAndRender(&keyState, &gamepadState, frameSimInterval, &frameArena);
        world.renderOverlay(frameTimeElapsed, frameTimePeriod);
        
        renderArea.updateDeviceFromBuffer(deviceContext, windowDim.width, windowDim.height);        
        ReleaseDC(windowHandle, deviceContext);
        
        // Measure elapsed time in microseconds for total frame
        QueryPerformanceCounter(&frameCompleteTime);
        frameTimeElapsed = static_cast<r64>(frameCompleteTime.QuadPart - cycleInitTime.QuadPart) * 1000000.0 / timerFrequency;
        
        // .. then wait for the rest of the time to pass until the end of this cycle
        if (frameTimeElapsed < frameTimePeriod)
        {
            DWORD waitTime = static_cast<DWORD>((frameTimePeriod - frameTimeElapsed) / 1000.0);
            if (waitTime)
            {
                Sleep(waitTime);
            }
            
            // TODO(KN): Do we need to spin here to consume any partial milliseconds
            // left over after sleep?
        }
        
        // Begin next interval
        QueryPerformanceCounter(&cycleInitTime);        
    }
}

void SendToConsole(HANDLE standardOutput, const char *writeString)
{
    DWORD outputElements = 0;
    WriteConsoleA(standardOutput, writeString, strlen(writeString), &outputElements, NULL);
}
 
int MainInternal(HINSTANCE instance, int argc, char *argv[])
{
    i32 loadArenaSize = 8*1048576;
    MemoryArena loadArena;
    loadArena.attachStorage(HeapAlloc(GetProcessHeap(), 0, loadArenaSize), loadArenaSize);

    LoadConfiguration configData;
    configData.type = LoadConfiguration::UNDEFINED;
    configData.mapName = vclib::stringCreate(&loadArena, vclib::StringPathLimit);
    
    if (argc > 1)
    {
        vclib::vcString commandType = vclib::stringFromRaw(&loadArena, argv[1]);
        vclib::lower(commandType);

        if (vclib::compareRaw(commandType, "duke3d") == true)
        {
            configData.type = LoadConfiguration::DUKE3D;
        }
        else if (vclib::compareRaw(commandType, "doom") == true)
        {
            configData.type = LoadConfiguration::DOOM;
        }
        else if (vclib::compareRaw(commandType, "custom") == true)
        {
            configData.type = LoadConfiguration::CUSTOM;
        }

        if (argc > 2)
        {
            vclib::concatenateRaw(configData.mapName, argv[2]);
        }
    }

    if (configData.type == LoadConfiguration::UNDEFINED)
    {
        AttachConsole(ATTACH_PARENT_PROCESS);
        HANDLE standardOutput = GetStdHandle(STD_OUTPUT_HANDLE);
        if (standardOutput != NULL && standardOutput != INVALID_HANDLE_VALUE)
        {
            SendToConsole(standardOutput, "\n\n");
            SendToConsole(standardOutput, "Usage: Portal3D duke3d|doom|custom map_name\n");
            SendToConsole(standardOutput, "Options:\n");
            SendToConsole(standardOutput, "duke3d          Use DukeNukem3D assets.\n");
            SendToConsole(standardOutput, "doom            Use Original Doom assets.\n");
            SendToConsole(standardOutput, "custom          Use Custom (Bisqwit) assets.\n");
            SendToConsole(standardOutput, "map_name        Name of map file (or asset) to load.\n\n");
            return 0;
        }
        else
        {
            // No command line to report usage so set fallback default
            configData.type = LoadConfiguration::CUSTOM;
        }            
    }

    WNDCLASSEXA windowClass = {};
    
    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowMessageHandler;
    windowClass.hInstance = instance;
    windowClass.lpszClassName = "Portal3DEngine";
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    
    renderArea.createDisplayBuffer(1024, 600);
    
    if (RegisterClassExA(&windowClass))
    {
        // Define style to pass to both adjustwindowrect and createwindow
        DWORD windowStyle = WS_CAPTION | WS_MINIMIZEBOX | WS_SYSMENU | WS_THICKFRAME;

        // Turn out defined client area into a top level window size
        RECT adjustRect = {0,0,renderArea.displayBuffer.width,renderArea.displayBuffer.height};
        AdjustWindowRectEx(&adjustRect, windowStyle, false, 0);        
        
        HWND windowHandle = CreateWindowExA(0,
                                            windowClass.lpszClassName,
                                            "Portal3DEngine",
                                            windowStyle | WS_VISIBLE,
                                            CW_USEDEFAULT,
                                            CW_USEDEFAULT,
                                            adjustRect.right-adjustRect.left,
                                            adjustRect.bottom-adjustRect.top,
                                            0,
                                            0,
                                            instance,
                                            0);
        
        if (windowHandle)
        {
            ProgramLoop(windowHandle, configData);
        }
        else
        {
            // TODO(KN): Handle failure condition
        }
    }
    else
    {
        // TODO(KN): Handle failure condition
    }
    
    return 0;
}

int CALLBACK WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR commandLine, int showWindow)
{
    return MainInternal(instance, __argc, __argv);
}
