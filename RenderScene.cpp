
#include "RenderScene.h"

void RenderScene::setCurrentLevelGeometry(WorldObjects *geometry)
{
    levelData = geometry;
        
    // Calculate inverted view distance for perspective depth colour scaling
    colourScaling = 1.0f / levelData->viewDistance;

    // Calculate focal length scaling factor from FoV
    hFocalLength = 1.0f / vclib::tanScalar(geometry->horizontalFov / 2.0f);
    vFocalLength = 1.0f / vclib::tanScalar(geometry->verticalFov / 2.0f);
}

void RenderScene::initRenderBuffer(void *pixelData, i32 width, i32 height, i32 pitch)
{
    rgbaTarget = reinterpret_cast<u32 *>(pixelData);
    displayWidth = width;
    displayHeight = height;
    displayPitch = pitch;

    // Keep 25% of the right side of the render buffer for the top-down map
    i32 xSplit = (displayWidth * 75) / 100;
    mapFrame = {xSplit + 1, 0, displayWidth, displayHeight};
    perspectiveFrame = {0, 0, xSplit, displayHeight};
}

void RenderScene::clearRenderBuffer(vfVec3 colour)
{
    u32 colValue = vclib::roundNormalColourToARGB(colour.r, colour.g, colour.b);
    u32 *pixelClear = rgbaTarget;        

    // TODO(KN): Replace this with something based on intrinsics
    // to get some decent write performance (non-temporal store?)
    i32 totalElements = displayHeight * (displayPitch / bytesPerPixel);
    for (i32 loop = 0; loop < totalElements; loop++)
    {
        *pixelClear++ = colValue;
    }
}

void RenderScene::
updateSimulationAndRender(ControllerState *digitalState, ControllerState *analogState,
                          r32 simulationTime, MemoryArena *frameArena)
{    
    r32 fwdAcceleration = 0.0f;
    r32 strafeAcceleration = 0.0f;
    r32 lookAngle = 0.0f;
    r32 verticalVelocity = 0.0f;

    // NOTE(KN): Keyboard / digital button states are assumed to be independent
    // whereas controller sticks are not (can't be in both directions at the same
    // time) so they are processed separately
    
    if (digitalState->moveFwd.isKeyDown)
    {
        fwdAcceleration += WorldRef::accelRate;
    }
    if (digitalState->moveBack.isKeyDown)
    {
        fwdAcceleration -= WorldRef::accelRate;
    }
    if (digitalState->turnLeft.isKeyDown)
    {
        lookAngle += WorldRef::turnRate;
    }
    if (digitalState->turnRight.isKeyDown)
    {
        lookAngle -= WorldRef::turnRate;
    }
    if (digitalState->moveLeft.isKeyDown)
    {
        strafeAcceleration += WorldRef::sideAccelRate;
    }
    if (digitalState->moveRight.isKeyDown)
    {
        strafeAcceleration -= WorldRef::sideAccelRate;
    }
    if (digitalState->jump.isKeyDown)
    {
        verticalVelocity += WorldRef::jumpInitialVel;
    }

    if (analogState->leftStickX > 0.0f)
    {
        strafeAcceleration -= WorldRef::sideAccelRate;
    }
    else if (analogState->leftStickX < 0.0f)
    {
        strafeAcceleration += WorldRef::sideAccelRate;
    }
    if (analogState->leftStickY > 0.0f)
    {
        fwdAcceleration += WorldRef::accelRate;
    }
    else if (analogState->leftStickY < 0.0f)
    {
        fwdAcceleration -= WorldRef::accelRate;
    }
    if (analogState->rightStickX < 0.0f)
    {
        lookAngle += WorldRef::turnRate;
    }
    else if (analogState->rightStickX > 0.0f)
    {
        lookAngle -= WorldRef::turnRate;
    }
    if (analogState->jump.isKeyDown)
    {
        verticalVelocity += WorldRef::jumpInitialVel;
    }
    
    // Rendering control states
    if (digitalState->increaseDepth.isKeyDown)
    {
        if (renderDepthIncMask == false)
        {
            renderDepth++;
            renderDepthIncMask = true;
        }
    }
    else
    {
        renderDepthIncMask = false;
    }
    if (digitalState->decreaseDepth.isKeyDown)
    {
        if (renderDepthDecMask == false)
        {
            renderDepth--;
            renderDepthDecMask = true;
        }
    }
    else
    {
        renderDepthDecMask = false;
    }

    levelData->updateEnvironment(fwdAcceleration, strafeAcceleration, lookAngle, simulationTime,
                                 verticalVelocity);
            
    clearRenderBuffer({0.0f,0.0f,0.0f});

    VisibilityItem *visibilityList = nullptr;    
    drawPerspectiveView(perspectiveFrame, frameArena, &visibilityList);
    drawMapView(mapFrame, visibilityList);
}

void RenderScene::renderOverlay(r64 frameTime, r64 frameInterval)
{    
    vfRect cpuBar = makeRect(perspectiveFrame);
    cpuBar.min.y = 10;
    cpuBar.max.y = 30;

    vfVec2 barExtents = getDimensions(cpuBar);

    if (frameTime > frameInterval)
    {
        vfVec3 barColour = {0.8f,0.0f,0.0f};
        cpuBar.max.x = barExtents.x * static_cast<r32>(frameInterval / frameTime);
        drawFilledRect(perspectiveFrame, cpuBar.min, cpuBar.max, barColour);
    }
    else
    {
        vfVec3 barColour = {0.0f,0.8f,0.0f};
        cpuBar.max.x = barExtents.x * static_cast<r32>(frameTime / frameInterval);
        drawFilledRect(perspectiveFrame, cpuBar.min, cpuBar.max, barColour);
    }
}

void RenderScene::drawPerspectiveView(vfRecti pixelFrame, MemoryArena *frameArena, VisibilityItem **visibilityList)
{
    FrameContext ctx;
    ctx.screenBounds = makeRect(pixelFrame);
    ctx.screenDim = getDimensions(ctx.screenBounds);
    ctx.worldBounds = getWorldBoundsForLevel(levelData->worldExtent);
    ctx.worldDim = getDimensions(ctx.worldBounds);

    i32 localSector = levelData->player.sector;
    if(localSector >= levelData->sectorCount)
        return;

    // Set render eye height for all sectors
    r32 playerEyelevel = levelData->player.height + WorldRef::playerHeight;
    
    // TODO(KN): Eventually move over to using homogenous coordinates so we can combine
    // the translation and rotation into a single transform
    vfVec2 translationVector = -1.0f * levelData->player.position;
    vfMat2x2 rotationMatrix = rotation(-1.0f * levelData->player.orientation + vclib::half_piflt);

    // Setup viewing frustum based on 90 deg horizontal Fov
    // TODO(KN): Replace with code that builds view frustum from an actual FoV value
    vfVec2 fovLeft = {-1.0f, hFocalLength};
    vfVec2 fovRight = {1.0f, hFocalLength};
    vfVec2 intersect = {0.0f};
    vfVec2 intersectLeft = {0.0f};
    vfVec2 intersectRight = {0.0f};

    // Allocation per-vertical strip pixel clipping data
    i32 clipEntriesCount = vclib::roundToInt32(ctx.screenDim.x);
    
    VerticalClip clipArray;
    clipArray.floor = frameArena->pushArray<r32>(clipEntriesCount);
    vclib::memoryFill(clipArray.floor, clipEntriesCount, 0.0f);
    clipArray.ceiling = frameArena->pushArray<r32>(clipEntriesCount);
    vclib::memoryFill(clipArray.ceiling, clipEntriesCount, ctx.screenDim.y);

    FlatPlane *planeAllocHead = nullptr;
    FlatPlane ** planeAllocList = &planeAllocHead;    
    BackRenderTexture *stackedDrawList = nullptr;
    
    // Start the draw list from the current sector
    // Sectors visible through portal walls will be appended to the draw list
    // as the walls are processed
    RenderItem *firstSector = frameArena->pushStruct<RenderItem>();
    firstSector->screenClipMinX = ctx.screenBounds.min.x;
    firstSector->screenClipMaxX = ctx.screenBounds.max.x-1;
    firstSector->drawSector = localSector;
    firstSector->originatingSector = -1;
    firstSector->next = nullptr;
    
    RenderItem *listItem = firstSector;
    RenderItem *listTail = listItem;

    i32 maxRenderCount = renderDepth;
    
    while(listItem && maxRenderCount--)
    {        
        // Set horizontal clipping bounds for this sector (in pixels)
        r32 clipHMin = listItem->screenClipMinX;
        r32 clipHMax = listItem->screenClipMaxX;
        
        WorldObjects::SectorDef *drawSector = &(levelData->sectorList[listItem->drawSector]);
    
        for (i32 lineIndex = 0; lineIndex < drawSector->wallCount; lineIndex++)
        {
            WorldObjects::SectorDef::WallDef *sectorEdge = &(drawSector->lineSegment[lineIndex]);

            if (listItem->originatingSector != -1 &&
                listItem->originatingSector == sectorEdge->travelToSector)
                continue;
            
            vfVec2 transformA = (sectorEdge->start + translationVector) * rotationMatrix;
            vfVec2 transformB = (sectorEdge->end + translationVector) * rotationMatrix;

            // Sector edge is completely behind viewpoint so can be ignored
            // for further viewing frustum checks
            if (transformA.y < 0.0f && transformB.y < 0.0f)
                continue;
            
            vfVec2 unclippedTransformA = transformA;
            vfVec2 edgeDirection = transformB - transformA;

            // Perform 2d intersection tests against viewing frustum, which is in
            // effect our clipping space
            r32 edgeCross = crossProduct(transformA, edgeDirection);
            r32 leftDenom = crossProduct(fovLeft, edgeDirection);
            r32 leftNumer = crossProduct(transformA, fovLeft);
            r32 rightDenom = crossProduct(fovRight, edgeDirection);
            r32 rightNumer = crossProduct(transformA, fovRight);

            i32 intersectCount = 0;
        
            if (leftDenom)
            {
                r32 t1 = edgeCross / leftDenom;
                r32 t2 = leftNumer / leftDenom;
                if (t1 >= 0.0f && t2 >= 0.0f && t2 <= 1.0f)
                {
                    intersect = intersectLeft = t1 * fovLeft;
                    intersectCount++;
                }
            }

            if (rightDenom)
            {
                r32 t1 = edgeCross / rightDenom;
                r32 t2 = rightNumer / rightDenom;
                if (t1 >= 0.0f && t2 >= 0.0f && t2 <= 1.0f)
                {
                    intersect = intersectRight = t1 * fovRight;
                    intersectCount++;
                }
            }

            // After computing intersections against the 2 edges of the viewing
            // frustum, clip the wall into the view space accordingly
            if (intersectCount == 0)
            {
                if (leftNumer <= 0.0f || rightNumer >= 0.0f)
                {
                    continue;
                }
            }
            else if (intersectCount == 1)
            {
                if (leftNumer > 0.0f && rightNumer < 0.0f)
                {
                    transformB = intersect;
                }
                else
                {
                    transformA = intersect;
                }
            }
            else
            {
                transformA = intersectLeft;
                transformB = intersectRight;
            }
            
            r32 floorPoint = drawSector->floor.height - playerEyelevel;
            r32 ceilingPoint = drawSector->ceiling.height - playerEyelevel;
            
            // Perform perspective divide for wall endpoints using focal lengths
            // calculated from the player field of view
            vfVec3 perspectivePointA = { (hFocalLength * transformA.x) / transformA.y,
                                         (vFocalLength * floorPoint) / transformA.y,
                                         (vFocalLength * ceilingPoint) / transformA.y };

            vfVec3 perspectivePointB = { (hFocalLength * transformB.x) / transformB.y,
                                         (vFocalLength * floorPoint) / transformB.y,
                                         (vFocalLength * ceilingPoint) / transformB.y };

            vfVec3 viewPointA = translatePerspectiveToViewport(&ctx, perspectivePointA);
            vfVec3 viewPointB = translatePerspectiveToViewport(&ctx, perspectivePointB);

            // Sector walls are defined with clockwise winding so skip any walls that
            // have x coordinates winding the opposite way. Also any walls that are
            // completely outside of our horizonal screen bounds (pixels) for this sector            
            if (viewPointA.x > viewPointB.x || viewPointB.x < clipHMin || viewPointA.x > clipHMax)
                continue;

            r32 segmentLength = viewPointB.x - viewPointA.x;
            r32 alphaGradient = 1.0f / segmentLength;
            r32 alpha = 0.0f;

            i32 startX = vclib::roundToInt32(viewPointA.x);
            i32 endX = vclib::roundToInt32(viewPointB.x);
            i32 clipColumnMin = vclib::roundToInt32(clipHMin);
            i32 clipColumnMax = vclib::roundToInt32(clipHMax);            
            r32 *clipCeilingIndex = &(clipArray.ceiling[startX]);
            r32 *clipFloorIndex = &(clipArray.floor[startX]);

            FlatPlane *flatPlaneFloor = makeFlatPlane(&drawSector->floor, planeAllocList, frameArena,
                                                      viewPointA.x, viewPointB.x, clipHMin, clipHMax);
            FlatPlane *flatPlaneCeil = makeFlatPlane(&drawSector->ceiling, planeAllocList, frameArena,
                                                     viewPointA.x, viewPointB.x, clipHMin, clipHMax);
                        
            if (sectorEdge->travelToSector == -1)
            {
                MappedTexture surface =
                    initTextureSurface(sectorEdge, &(sectorEdge->wallTexture),
                                       transformA, transformB, unclippedTransformA,
                                       drawSector->ceiling.height - drawSector->floor.height);                                   
                
                for (i32 columnLoop = startX; columnLoop <= endX; columnLoop++)
                {
                    r32 *clipCeiling = clipCeilingIndex++;
                    r32 *clipFloor = clipFloorIndex++;
                    
                    if (columnLoop >= clipColumnMin && columnLoop <= clipColumnMax)
                    {
                        r32 clipTop = vclib::minValue(*clipCeiling, ctx.screenBounds.max.y);
                        r32 clipBottom = vclib::maxValue(*clipFloor, ctx.screenBounds.min.y);

                        r32 floorPt = vclib::lerp(viewPointA.y, alpha, viewPointB.y);
                        r32 ceilPt = vclib::lerp(viewPointA.z, alpha, viewPointB.z);
                        
                        // Draw vertical wall strip
                        if (surface.mapValid == true)
                        {
                            r32 depthPt = vclib::lerp(surface.depthA, alpha, surface.depthB);
                            r32 texelU = vclib::lerp(surface.texelA.u, alpha, surface.texelB.u) / depthPt;
                            
                            texturedStrip(columnLoop, texelU, &surface, floorPt, ceilPt, clipBottom, clipTop, depthPt);
                            //untexturedStrip(columnLoop, floorPt, ceilPt, clipBottom, clipTop, wallColour, depthShade);
                        }

                        // Store plane elements for floor and ceiling
                        addStripToFlatPlane(flatPlaneFloor, columnLoop, clipBottom, floorPt, clipBottom, clipTop);
                        addStripToFlatPlane(flatPlaneCeil, columnLoop, ceilPt, clipTop, clipBottom, clipTop);                        
                    }
                    
                    alpha += alphaGradient;
                }        
            }
            else
            {
                // Get floor and ceiling heights of sector on the other side of the portal
                WorldObjects::SectorDef *nextSector = &(levelData->sectorList[sectorEdge->travelToSector]);

                WorldObjects::SectorDef *selectFloor =
                    (nextSector->floor.height < drawSector->floor.height) ? drawSector : nextSector;
                WorldObjects::SectorDef *selectCeiling =
                    (nextSector->ceiling.height > drawSector->ceiling.height) ? drawSector : nextSector;

                r32 nextFloorPoint = selectFloor->floor.height - playerEyelevel;
                r32 nextCeilingPoint = selectCeiling->ceiling.height - playerEyelevel;
                
                MappedTexture lowerPanel =
                    initTextureSurface(sectorEdge, &(sectorEdge->lowTexture),
                                       transformA, transformB, unclippedTransformA,
                                       selectFloor->floor.height - drawSector->floor.height);

                MappedTexture wallPanel =
                    initTextureSurface(sectorEdge, &(sectorEdge->wallTexture),
                                       transformA, transformB, unclippedTransformA,
                                       selectCeiling->ceiling.height - selectFloor->floor.height);

                if (wallPanel.mapValid == true)
                {
                    BackRenderTexture *nextStack = frameArena->pushStruct<BackRenderTexture>();
                    nextStack->textureData = wallPanel;

                    // Add array of deferred strips to stacked texture
                    nextStack->stripCount = endX - startX + 1;
                    nextStack->renderStripArray = frameArena->pushArray<DeferredStrip>(nextStack->stripCount);                    
                    nextStack->next = stackedDrawList;
                    stackedDrawList = nextStack;

                    for (i32 strip = 0; strip < nextStack->stripCount; strip++)
                    {
                        nextStack->renderStripArray[strip].render = false;
                    }
                }

                MappedTexture upperPanel =
                    initTextureSurface(sectorEdge, &(sectorEdge->highTexture),
                                       transformA, transformB, unclippedTransformA,
                                       drawSector->ceiling.height - selectCeiling->ceiling.height);

                // If the current ceiling texture is a skybox and the next sector through the
                // portal also has a skybox texture then ignore the upper wall texture and just
                // draw through it to join up the sky. This is described in
                // https://doomwiki.org/wiki/Sky#Additional_information
                bool joinSkyTexture = false;
                if (drawSector->ceiling.skybox == true && nextSector->ceiling.skybox == true)
                {
                    joinSkyTexture = true;
                }
                
                // Perform perspective divide for floor and ceiling height of sector that can
                // can be seen through the current portal. We only compute new Y values as the
                // X perspective divide is still valid
                vfVec3 perspectiveNextA = { 0.0f,
                                            (vFocalLength * nextFloorPoint) / transformA.y,
                                            (vFocalLength * nextCeilingPoint) / transformA.y };

                vfVec3 perspectiveNextB = { 0.0f,
                                            (vFocalLength * nextFloorPoint) / transformB.y,
                                            (vFocalLength * nextCeilingPoint) / transformB.y };

                vfVec3 viewNextA = translatePerspectiveToViewport(&ctx, perspectiveNextA);
                vfVec3 viewNextB = translatePerspectiveToViewport(&ctx, perspectiveNextB);

                for (i32 columnLoop = startX; columnLoop <= endX; columnLoop++)
                {
                    r32 *clipCeiling = clipCeilingIndex++;
                    r32 *clipFloor = clipFloorIndex++;

                    if (columnLoop >= clipColumnMin && columnLoop <= clipColumnMax)
                    {
                        r32 clipTop = vclib::minValue(*clipCeiling, ctx.screenBounds.max.y);
                        r32 clipBottom = vclib::maxValue(*clipFloor, ctx.screenBounds.min.y);

                        r32 floorPt = vclib::lerp(viewPointA.y, alpha, viewPointB.y);
                        r32 ceilPt = vclib::lerp(viewPointA.z, alpha, viewPointB.z);
                        r32 nextFloorPt = vclib::lerp(viewNextA.y, alpha, viewNextB.y);
                        r32 nextCeilPt = vclib::lerp(viewNextA.z, alpha, viewNextB.z);
                        
                        // Draw vertical 'lower' wall strip
                        if (lowerPanel.mapValid == true)
                        {
                            r32 depthPtLower = vclib::lerp(lowerPanel.depthA, alpha, lowerPanel.depthB);
                            r32 texelU = vclib::lerp(lowerPanel.texelA.u, alpha, lowerPanel.texelB.u) / depthPtLower;

                            texturedStrip(columnLoop, texelU, &lowerPanel, floorPt, nextFloorPt, clipBottom, clipTop, depthPtLower);
                        }

                        // Stack vertical 'portal' wall strip. This only time a texture is
                        // rendered to the portal area is for transparent overlays
                        if (wallPanel.mapValid == true)
                        {
                            r32 depthPtWall = vclib::lerp(wallPanel.depthA, alpha, wallPanel.depthB);
                            r32 texelU = vclib::lerp(wallPanel.texelA.u, alpha, wallPanel.texelB.u) / depthPtWall;

                            deferredStrip(stackedDrawList, startX, columnLoop, texelU, nextFloorPt, nextCeilPt, clipBottom, clipTop, depthPtWall);                            
                        }

                        if (joinSkyTexture == false)
                        {
                            // Draw vertical 'upper' wall strip
                            if (upperPanel.mapValid == true)
                            {
                                r32 depthPtUpper = vclib::lerp(upperPanel.depthA, alpha, upperPanel.depthB);
                                r32 texelU = vclib::lerp(upperPanel.texelA.u, alpha, upperPanel.texelB.u) / depthPtUpper;

                                texturedStrip(columnLoop, texelU, &upperPanel, nextCeilPt, ceilPt, clipBottom, clipTop, depthPtUpper);
                            }

                            // Store normal plane element for ceiling
                            addStripToFlatPlane(flatPlaneCeil, columnLoop, ceilPt, clipTop, clipBottom, clipTop);                        
                        }
                        else
                        {
                            // Join plane elements between this ceiling and next ceiling
                            addStripToFlatPlane(flatPlaneCeil, columnLoop, nextCeilPt, clipTop, clipBottom, clipTop);                        
                        }
                        
                        // Store plane element for floor
                        addStripToFlatPlane(flatPlaneFloor, columnLoop, clipBottom, floorPt, clipBottom, clipTop);
                        
                        // Collapse pixel clipping column to within portal bounds
                        *clipCeiling = vclib::minValue(clipTop, nextCeilPt);
                        *clipFloor = vclib::maxValue(clipBottom, nextFloorPt);
                    }
                    
                    alpha += alphaGradient;
                }

                // Define portal through into next sector and add to render list
                RenderItem *portalSector = frameArena->pushStruct<RenderItem>();
                portalSector->screenClipMinX = vclib::maxValue(viewPointA.x, clipHMin);
                portalSector->screenClipMaxX = vclib::minValue(viewPointB.x, clipHMax);
                portalSector->drawSector = sectorEdge->travelToSector;
                portalSector->originatingSector = listItem->drawSector;
                portalSector->next = nullptr;

                listTail->next = portalSector;
                listTail = portalSector;
            }
        }

        listItem = listItem->next;
    }

    drawPlanes(&ctx, *planeAllocList, playerEyelevel, frameArena, visibilityList);
    drawDeferredWalls(stackedDrawList);
}

void RenderScene::drawDeferredWalls(BackRenderTexture *renderList)
{
    BackRenderTexture *iterateList = renderList;
    while (iterateList)
    {
        for (i32 loop = 0; loop < iterateList->stripCount; loop++)
        {
            DeferredStrip *indexStrip = &(iterateList->renderStripArray[loop]);
            if(indexStrip->render == true)
            {
                transparentStrip(indexStrip->column, indexStrip->texelU, &(iterateList->textureData),
                                 indexStrip->floorPoint, indexStrip->ceilingPoint,
                                 indexStrip->lowerClip, indexStrip->upperClip, indexStrip->depth);                              
            }
        }
        
        iterateList = iterateList->next;
    }
}

void RenderScene::drawPlanes(FrameContext *ctx, FlatPlane *planeList, r32 playerEyelevel,
                             MemoryArena *frameArena, VisibilityItem **visibilityList)
{
    vfVec2 translationVector = levelData->player.position;
    vfMat2x2 rotationMatrix = rotation(levelData->player.orientation - vclib::half_piflt);

    VisibilityItem **addItems = visibilityList;
    
    FlatPlane *plane = planeList;
    while(plane != nullptr)
    {
        if (plane->skybox == true)
        {
            // Special case mapping for 'sky' texture
            drawParallaxPlane(ctx, plane);
            plane = plane->next;
            continue;
        }

        r32 planeMinY = -1.0f;
        r32 planeMaxY = -1.0f;
        r32 *startYIter = &(plane->startY[0]);
        r32 *endYIter = &(plane->endY[0]);
        
        // First-pass to get y extents of flat plane
        for (i32 colIndex = 0; colIndex < plane->extentX; colIndex++)
        {
            if (planeMinY == -1.0f || planeMaxY == -1.0f)
            {
                planeMinY = *startYIter;
                planeMaxY = *endYIter;
            }
            else
            {
                if (*startYIter > -1.0f && *startYIter < planeMinY)
                {
                    planeMinY = *startYIter;
                }

                if (*endYIter > -1.0f && *endYIter > planeMaxY)
                {
                    planeMaxY = *endYIter;
                }
            }

            startYIter++;
            endYIter++;
        }

        i32 startRow = vclib::roundToInt32(planeMinY);
        i32 endRow = vclib::roundToInt32(planeMaxY);        
        
        if (startRow >= 0 && endRow >= 0)
        {          
            r32 planeViewHeight = plane->height - playerEyelevel;
            r32 alphaGradient = 1.0f / plane->extentX;

            // Translate pixel (viewport) positions back to a perspective values
            r32 perspectiveMinX = 2.0f * ((plane->minX - ctx->screenBounds.min.x) / ctx->screenDim.x) - 1.0f;
            r32 perspectiveMaxX = 2.0f * ((plane->maxX - ctx->screenBounds.min.x) / ctx->screenDim.x) - 1.0f;

            u32 uTexelMask = plane->texWidth - 1;
            u32 vTexelMask = plane->texHeight - 1;
            
            // Now switch to rendering in rows rather than columns
            // For floor and ceiling flat planes a row is at constant Z so distance
            // can be pre-computed for the entire row
        
            for (i32 loop = startRow; loop < endRow; loop++)
            {                
                // Unproject from pixel row back to z distance from viewer
                r32 perspectiveZ = 2.0f * ((loop - ctx->screenBounds.min.y) / ctx->screenDim.y) - 1.0f;
                if (perspectiveZ == 0.0f)
                    continue;
                                
                r32 distanceZ = (vFocalLength * planeViewHeight) / perspectiveZ;

                // Use unprojected Z to subsequently unproject the X positions at either
                // end of the row we want to draw
                vfVec2 unprojectMin;
                unprojectMin.x = (perspectiveMinX * distanceZ) / hFocalLength;
                unprojectMin.y = distanceZ;
                
                vfVec2 unprojectMax;
                unprojectMax.x = (perspectiveMaxX * distanceZ) / hFocalLength;
                unprojectMax.y = distanceZ;

                // Finally return unprojected points from view back to world coords
                vfVec2 worldA = (unprojectMin * rotationMatrix) + translationVector;
                vfVec2 worldB = (unprojectMax * rotationMatrix) + translationVector;
                
                r32 alpha = 0.0f;
                
                // Normalise distance into a 0.0->1.0 colour scaling with 0.0 representing the
                // furthest distance that can be seen
                r32 invertDepth = (levelData->viewDistance - distanceZ) * colourScaling;

                // Apply per-surface shading offset and clamp    
                r32 depthShade = vclib::clampValue(invertDepth * plane->shade, 0.0f, 1.0f);    
                        
                // Initialise target pixel buffer pointer
                u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget) + loop * displayPitch;
                u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt) + plane->minX;

                startYIter = &(plane->startY[0]);
                endYIter = &(plane->endY[0]);

                // Pre-roll loop until the first in-bounds column on this row
                i32 colIndex = 0;
                for (;colIndex < plane->extentX; colIndex++)
                {
                    if (loop >= *startYIter && loop <= *endYIter)
                    {
                        break;
                    }
                    pixelOffset++;
                    startYIter++;
                    endYIter++;
                    alpha += alphaGradient;
                }

                if (colIndex == plane->extentX)
                    continue;

                r32 recordAlphaStart = alpha;
                r32 recordAlphaEnd = alpha;
                vfVec2 interpPoint;
                                
                for (; colIndex < plane->extentX; colIndex++)
                {                    
                    if (loop >= *startYIter && loop <= *endYIter)
                    {
                        interpPoint.x = vclib::lerp(worldA.x, alpha, worldB.x);
                        interpPoint.y = vclib::lerp(worldA.y, alpha, worldB.y);
                        recordAlphaEnd = alpha;

                        i32 indexU = vclib::roundToInt32(interpPoint.x * plane->uScale) & uTexelMask;
                        i32 indexV = vclib::roundToInt32(interpPoint.y * plane->vScale) & vTexelMask;
                        
                        // Index into texel in our rotated texture
                        u32 packedTexel = plane->texelData[indexU * plane->texHeight + indexV];

                        // Apply depth shading to BGR colour elements (don't touch alpha)
                        u8 *componentTexel = reinterpret_cast<u8 *>(&packedTexel);
                        *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
                        *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
                        *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
                                                
                        *pixelOffset = packedTexel;                        
                    }

                    pixelOffset++;
                    startYIter++;
                    endYIter++;

                    alpha += alphaGradient;
                }

                if (plane->visibility == true && recordAlphaEnd > recordAlphaStart)
                {
                    // Store world coords to visiblity list for rendering on map view
                    // This is a quick way to render a visibility cone from the player
                    // position based on the area of floor we can see
                    VisibilityItem *sightLine = frameArena->pushStruct<VisibilityItem>();
                    sightLine->shade = plane->shade;
                    sightLine->next = nullptr;
                    *addItems = sightLine;
                    addItems = &(sightLine->next);
                
                    interpPoint.x = vclib::lerp(worldA.x, recordAlphaStart, worldB.x);
                    interpPoint.y = vclib::lerp(worldA.y, recordAlphaStart, worldB.y);
                    sightLine->worldA = interpPoint;

                    interpPoint.x = vclib::lerp(worldA.x, recordAlphaEnd, worldB.x);
                    interpPoint.y = vclib::lerp(worldA.y, recordAlphaEnd, worldB.y);                
                    sightLine->worldB = interpPoint;
                }
            }
        }   
                                    
        plane = plane->next;
    }    
}

void RenderScene::drawParallaxPlane(FrameContext *ctx, FlatPlane *plane)
{
    r32 *startYIter = &(plane->startY[0]);
    r32 *endYIter = &(plane->endY[0]);
    
    for (i32 colIndex = 0; colIndex < plane->extentX; colIndex++)
    {
        if (*startYIter == -1.0f || *endYIter == -1.0f || *startYIter > *endYIter)
        {
            startYIter++;
            endYIter++;
            continue;
        }

        i32 startRow = vclib::roundToInt32(*startYIter);
        i32 endRow = vclib::roundToInt32(*endYIter);

        r32 textureCyl = (r32)plane->texWidth * plane->uScale;

        // NOTE(KN): The sky (parallax) texture is mapped onto a cylinder around
        // the player position, and the texel lookup is calculated based on the
        // current view angle of the player and the amount of texture that fits in
        // the horizontal fov.
        // However, the actual texture mapping is just a linear scale between the
        // endpoints... so not cylindrical
        
        r32 centredOnTexel = ((vclib::two_piflt - levelData->player.orientation) / vclib::two_piflt) * textureCyl;
        r32 scaleExtent = (levelData->horizontalFov / vclib::two_piflt) * textureCyl;
        r32 textureOffset = (plane->minX + colIndex) * (scaleExtent / ctx->screenDim.x);

        r32 texelU = centredOnTexel - (scaleExtent / 2.0f) + textureOffset;
        i32 indexU = vclib::roundToInt32(texelU) % plane->texWidth;                

        r32 verticalScale = (r32)plane->texHeight * plane->vScale;
        r32 alphaGradient = 1.0f / ctx->screenDim.y;
        r32 alpha = 0.0f;    

        // Note(KN): Row and stepping are inverted here because the texture is
        // aligned to the top of the drawing area
        i32 clippedRowCount = vclib::roundToInt32(ctx->screenDim.y) - endRow;
        i32 initialRow = endRow - 1;
        i32 rowStepping = -displayPitch;    

        if (clippedRowCount > 0)
        {
            // Advance lerp to point that matches screen clip
            alpha += alphaGradient * static_cast<r32>(clippedRowCount);
        }
                
        // Initialise target pixel buffer pointer
        u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget) + initialRow * displayPitch;
        u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt) + plane->minX + colIndex;
        u8 *columnStart = reinterpret_cast<u8 *>(pixelOffset);
               
        // Set base address for this texture column in our rotated texture
        u32 *textureColumn = &(plane->texelData[indexU * plane->texHeight]);
    
        for (i32 loop = startRow; loop < endRow; loop++)
        {
            r32 texelV = vclib::lerp(0.0f, alpha, verticalScale);
            i32 indexV = vclib::roundToInt32(texelV) % plane->texHeight;

            pixelOffset = reinterpret_cast<u32 *>(columnStart);
            u32 packedTexel = textureColumn[indexV];        
            *pixelOffset = packedTexel;
                    
            columnStart += rowStepping;
            alpha += alphaGradient;
        }

        startYIter++;
        endYIter++;                
    }
}

void RenderScene::drawMapView(vfRecti pixelFrame, VisibilityItem *visibilityList)
{
//    setClipRegion(pixelFrame);
    vfRecti clientArea = rectExpandi(pixelFrame, -20, -80);
    
    FrameContext ctx;
    ctx.screenBounds = makeRect(clientArea);
    ctx.screenDim = getDimensions(ctx.screenBounds);

    // Define scaling so we still end up with correct x/y ratio
    // irrespective of the screen dimensions that we're drawing the map into
    r32 zoomLevel = 3.f;
    r32 majorExtent = vclib::maxValue(levelData->worldExtent.x, levelData->worldExtent.y) / zoomLevel;
    r32 displayRatio = ctx.screenDim.x / ctx.screenDim.y;

    // .. and use scaling to define appropriate world bounds for display
    vfVec2 scaledExtent = {majorExtent * displayRatio, majorExtent};
    ctx.worldBounds = getWorldBoundsForLevel(scaledExtent);
    ctx.worldDim = getDimensions(ctx.worldBounds);
    
    r32 cellStepping = 25.0f; // pixels

    vfVec2 gridLinep0;
    vfVec2 gridLinep1;
    vfVec2 gridLineWidth = {1.0f, 1.0f};

    r32 xGridPoint = ctx.screenBounds.min.x;
    i32 xGridCells = vclib::roundToInt32(ctx.screenDim.x / cellStepping);
    
    for (i32 loop = 0; loop <= xGridCells; loop++)
    {
        gridLinep0 = {xGridPoint, ctx.screenBounds.min.y};
        gridLinep1 = {xGridPoint, ctx.screenBounds.max.y};
        drawFilledRect(clientArea, gridLinep0, gridLinep1 + gridLineWidth, {0.0f,0.1f,0.0f});
        xGridPoint += cellStepping;
    }

    r32 yGridPoint = ctx.screenBounds.min.y;
    i32 yGridCells = vclib::roundToInt32(ctx.screenDim.y / cellStepping);

    for (i32 loop = 0; loop <= yGridCells; loop++)
    {    
        gridLinep0 = {ctx.screenBounds.min.x, yGridPoint};
        gridLinep1 = {ctx.screenBounds.max.x, yGridPoint};
        drawFilledRect(clientArea, gridLinep0, gridLinep1 + gridLineWidth, {0.0f,0.1f,0.0f});
        yGridPoint += cellStepping;
    }

    // Compute a pixel based correction so map view is centred on player position
    vfVec2 viewOrigin = translateWorldToViewport(&ctx, levelData->player.position);
    vfVec2 viewCentre = getCentre(ctx.screenBounds);
    vfVec2 viewOffset = viewCentre - viewOrigin;
    viewOrigin = viewOrigin + viewOffset;
    
    // Render lines of visibility on top of grid, but under actual map data
    VisibilityItem *renderVisLines = visibilityList;
    while(renderVisLines)
    {
        vfVec2 visLineA = translateWorldToViewport(&ctx, renderVisLines->worldA) + viewOffset;
        vfVec2 visLineB = translateWorldToViewport(&ctx, renderVisLines->worldB) + viewOffset;
        drawLineRectIntersection(ctx.screenBounds, visLineA, visLineB, {0.0f,0.0f,0.2f * renderVisLines->shade});

        renderVisLines = renderVisLines->next;
    }
    
    vfRect vertexRect = {0.0f, 0.0f, 5.0f, 5.0f};
    vfVec2 vertexRectCentre = getCentre(vertexRect);
    vfVec2 vertexRectDim = getDimensions(vertexRect);

#if 1
    for (i32 loop = 0; loop < levelData->vertexCount; loop++)
    {
        vfVec2 vertexOrigin = translateWorldToViewport(&ctx, levelData->vertexList[loop]) + viewOffset;
        vfVec2 vertexPosRectMin = round(vertexOrigin) - vertexRectCentre;
        vfVec2 vertexPosRectMax = vertexPosRectMin + vertexRectDim;

        drawFilledRect(clientArea, vertexPosRectMin, vertexPosRectMax, {0.0f, 0.5f, 0.0f});
    }
#else
    vfVec2 *sectorVertexes = levelData->sectorVertexList[levelData->player.sector].sectorVerts;
    i32 sectorVertexCount = levelData->sectorVertexList[levelData->player.sector].vertCount;

    for (i32 loop = 0; loop < sectorVertexCount; loop++)
    {
        vfVec2 vertexOrigin = translateWorldToViewport(&ctx, sectorVertexes[loop]) + viewOffset;
        vfVec2 vertexPosRectMin = round(vertexOrigin) - vertexRectCentre;
        vfVec2 vertexPosRectMax = vertexPosRectMin + vertexRectDim;

        drawFilledRect(clientArea, vertexPosRectMin, vertexPosRectMax, {0.0f, 0.5f, 0.0f});
    }
#endif

    vfVec3 portalColour =  {0.3f, 0.0f, 0.0f};
    vfVec3 edgeColour = {0.3f, 0.3f, 0.3f};
    
    for (i32 loop = 0; loop < levelData->sectorCount; loop++)
    {
        if (loop != levelData->player.sector)
        {
            drawSectorWalls(&ctx, loop, portalColour, edgeColour);        
        }
    }

    drawSectorWalls(&ctx, levelData->player.sector, portalColour, edgeColour);    

    // Reuse vertex rect definition for our player position indicator
    vfVec2 viewPosRectMin = round(viewOrigin) - vertexRectCentre;
    vfVec2 viewPosRectMax = viewPosRectMin + vertexRectDim;

    drawFilledRect(clientArea, viewPosRectMin, viewPosRectMax, {0.0f,0.0f,1.0f});

    vfVec2 endPoint = levelData->player.position + vectorAtRange(levelData->player.orientation, 1.0f);
    vfVec2 viewEndPoint = translateWorldToViewport(&ctx, endPoint) + viewOffset;
    drawLineRectIntersection(ctx.screenBounds, viewOrigin, viewEndPoint, {0.0f,0.0f,1.0f});
}

void RenderScene::drawSectorWalls(FrameContext *ctx, i32 sector, vfVec3 portalColour, vfVec3 edgeColour)
{
    // Compute a pixel based correction so map view is centred on player position
    vfVec2 viewOrigin = translateWorldToViewport(ctx, levelData->player.position);
    vfVec2 viewCentre = getCentre(ctx->screenBounds);
    vfVec2 viewOffset = viewCentre - viewOrigin;

    WorldObjects::SectorDef *drawSector = &(levelData->sectorList[sector]);
        
    for (i32 lineIndex = 0; lineIndex < drawSector->wallCount; lineIndex++)
    {
        WorldObjects::SectorDef::WallDef *sectorEdge = &(drawSector->lineSegment[lineIndex]);
            
        vfVec2 viewPt1 = translateWorldToViewport(ctx, sectorEdge->start) + viewOffset;
        vfVec2 viewPt2 = translateWorldToViewport(ctx, sectorEdge->end) + viewOffset;            
        
        vfVec3 lineColour = (sectorEdge->travelToSector == -1) ? edgeColour : portalColour;
        if (sector == levelData->player.sector)
        {
            lineColour = 2.5f * lineColour;
        }
        
        drawLineRectIntersection(ctx->screenBounds, viewPt1, viewPt2, lineColour);
    }
}

vfRect RenderScene::getWorldBoundsForLevel(vfVec2 extents)
{
    vfRect result;
    result.min.x = 0.0f;
    result.min.y = 0.0f;
    result.max.x = extents.x;
    result.max.y = extents.y;
    return result;
}

vfVec3 RenderScene::translatePerspectiveToViewport(FrameContext *ctx, vfVec3 perspectPos)
{
    vfVec3 result;
    result.x = (0.5f * perspectPos.x + 0.5f) * ctx->screenDim.x + ctx->screenBounds.min.x;

    // y and z values relate to floor and ceiling positions respectively for the given x
    result.y = (0.5f * perspectPos.y + 0.5f) * ctx->screenDim.y + ctx->screenBounds.min.y;    
    result.z = (0.5f * perspectPos.z + 0.5f) * ctx->screenDim.y + ctx->screenBounds.min.y;
    return result;
}

vfVec2 RenderScene::translateWorldToViewport(FrameContext *ctx, vfVec2 worldPos)
{
    // TODO(KN): This scaling calculation from metres to pixels can be replaced with
    // a single pre-computed scale factor for the level
    
    vfVec2 result;
    result.x = (worldPos.x * ctx->screenDim.x) / ctx->worldDim.x + ctx->screenBounds.min.x;
    result.y = (worldPos.y * ctx->screenDim.y) / ctx->worldDim.y + ctx->screenBounds.min.y;
    return result;
}

void RenderScene::drawFilledRect(vfRecti clipRect, vfVec2 topLeft, vfVec2 bottomRight, vfVec3 colour)
{
    vfRecti boundedRect;
    boundedRect.min.x = vclib::roundToInt32(topLeft.x);
    boundedRect.min.y = vclib::roundToInt32(topLeft.y);
    boundedRect.max.x = vclib::roundToInt32(bottomRight.x);
    boundedRect.max.y = vclib::roundToInt32(bottomRight.y);

    // Clip the resulting bounds against the current clipping rect
    boundedRect = rectIntersectioni(boundedRect, clipRect);

    u32 colValue = vclib::roundNormalColourToARGB(colour.r, colour.g, colour.b);

    // Be mindful of pointer arithmetic here
    u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget + boundedRect.min.x) + (boundedRect.min.y * displayPitch);
    
    for (i32 yIndex = boundedRect.min.y; yIndex < boundedRect.max.y; yIndex++)
    {
        u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt);        
        for (i32 xIndex = boundedRect.min.x; xIndex < boundedRect.max.x; xIndex++)
        {
            *pixelOffset++ = colValue;
        }

        pixelPt += displayPitch;
    }
}

bool RenderScene::edgeTest(r32 delta, r32 edgeDiff, r32 *t0, r32 *t1)
{
    if (delta == 0.0f)
    {
        return (edgeDiff >= 0.0f);
    }

    r32 ratio = edgeDiff / delta;
    bool clipInside = false;
    
    if (delta < 0.0f)
    {
        if (ratio <= *t1)
        {
            clipInside = true;
            if (ratio > *t0)
            {
                *t0 = ratio;
            }
        }                            
    }
    else
    {
        if (ratio >= *t0)
        {
            clipInside = true;
            if (ratio < *t1)
            {
                *t1 = ratio;
            }
        }
    }

    return clipInside;
}

void RenderScene::drawLineRectIntersection(vfRect rect, vfVec2 start, vfVec2 end, vfVec3 colour)
{
    r32 t0 = 0.0f;
    r32 t1 = 1.0f;
    vfVec2 delta = end - start;
    
    if (edgeTest(-delta.x, start.x - rect.min.x, &t0, &t1) && // left
        edgeTest(delta.x,  rect.max.x - start.x, &t0, &t1) && // right
        edgeTest(-delta.y, start.y - rect.min.y, &t0, &t1) && // bottom
        edgeTest(delta.y,  rect.max.y - start.y, &t0, &t1))   // top
    {
        vfVec2 clipStart = {start.x + t0 * delta.x, start.y + t0 * delta.y};
        vfVec2 clipEnd   = {start.x + t1 * delta.x, start.y + t1 * delta.y};
        
        drawSimpleLine(clipStart, clipEnd, colour);
    }
}

void RenderScene::drawSimpleLine(vfVec2 start, vfVec2 end, vfVec3 colour)
{
    u32 colValue = vclib::roundNormalColourToARGB(colour.r, colour.g, colour.b);

    i32 startX, endX, startY, endY;
    
    if (vclib::absoluteValue(end.y - start.y) < vclib::absoluteValue(end.x - start.x))
    {
        if (start.x > end.x)
        {
            startX = vclib::roundToInt32(end.x);
            endX = vclib::roundToInt32(start.x);
            startY = vclib::roundToInt32(end.y);
            endY = vclib::roundToInt32(start.y);
        }
        else
        {
            startX = vclib::roundToInt32(start.x);
            endX = vclib::roundToInt32(end.x);
            startY = vclib::roundToInt32(start.y);
            endY = vclib::roundToInt32(end.y);
        }

        i32 deltaX = endX - startX;
        i32 deltaY = endY - startY;
        i32 eps = (deltaY << 1) - deltaX;
        i32 drawY = startY;

        i32 yStep = 1;
        if (deltaY < 0)
        {
            yStep = -1;
            deltaY = -deltaY;
        }
    
        u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget);

        for (i32 drawX = startX; drawX <= endX; drawX++)
        {
            u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt + drawY * displayPitch) + drawX;
            *pixelOffset = colValue;

            if (eps > 0)
            {
                drawY += yStep;
                eps -= (deltaX << 1);
            }

            eps += (deltaY << 1);
        }        
    }
    else
    {
        if (start.y > end.y)
        {
            startX = vclib::roundToInt32(end.x);
            endX = vclib::roundToInt32(start.x);
            startY = vclib::roundToInt32(end.y);
            endY = vclib::roundToInt32(start.y);
        }
        else
        {
            startX = vclib::roundToInt32(start.x);
            endX = vclib::roundToInt32(end.x);
            startY = vclib::roundToInt32(start.y);
            endY = vclib::roundToInt32(end.y);
        }

        i32 deltaX = endX - startX;
        i32 deltaY = endY - startY;
        i32 eps = (deltaX << 1) - deltaY;
        i32 drawX = startX;

        i32 xStep = 1;
        if (deltaX < 0)
        {
            xStep = -1;
            deltaX = -deltaX;
        }
    
        u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget);

        for (i32 drawY = startY; drawY <= endY; drawY++)
        {
            u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt + drawY * displayPitch) + drawX;
            *pixelOffset = colValue;

            if (eps > 0)
            {
                drawX += xStep;
                eps -= (deltaY << 1);
            }

            eps += (deltaX << 1);
        }        
    }
}

void RenderScene::untexturedStrip(i32 screenColumn, r32 start, r32 end,
                                  r32 clampLow, r32 clampHigh, vfVec3 colour, r32 depth)
{
    if (start > end)
        return;

    vfVec3 depthAdjusted = depth * colour;
    u32 colourValue = vclib::roundNormalColourToARGB(depthAdjusted.r, depthAdjusted.g, depthAdjusted.b);

    i32 startRow = vclib::roundToInt32(vclib::clampValue(start, clampLow, clampHigh));
    i32 endRow = vclib::roundToInt32(vclib::clampValue(end, clampLow, clampHigh));
        
    u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget);
    u8 *columnStart = pixelPt + startRow * displayPitch;

    for (i32 loop = startRow; loop < endRow; loop++)
    {
        u32 *pixelOffset = reinterpret_cast<u32 *>(columnStart) + screenColumn;
        *pixelOffset = colourValue;
        
        columnStart += displayPitch;
    }
}

void RenderScene::texturedStrip(i32 screenColumn, r32 texelU, MappedTexture *surface, r32 start, r32 end,
                                r32 clampLow, r32 clampHigh, r32 depth)
{
    if (start > end || surface->texelData == nullptr)
        return;

    i32 indexU = vclib::roundToInt32(texelU) % surface->width;    
    r32 alphaGradient = 1.0f / (end - start);
    r32 alpha = 0.0f;    
    
    i32 startRow = vclib::roundToInt32(vclib::clampValue(start, clampLow, clampHigh));
    i32 endRow = vclib::roundToInt32(vclib::clampValue(end, clampLow, clampHigh));

    i32 clippedRowCount;
    i32 initialRow;
    i32 rowStepping;

    // Set parameters for top down or bottom up texture alignment
    if (surface->invert == true)
    {
        clippedRowCount = vclib::roundToInt32(end) - endRow;
        initialRow = endRow - 1;
        rowStepping = -displayPitch;
    }
    else
    {
        clippedRowCount = startRow - vclib::roundToInt32(start);
        initialRow = startRow;
        rowStepping = displayPitch;
    }
    
    if (clippedRowCount > 0)
    {
        // Advance lerp to point that matches screen clip
        alpha += alphaGradient * static_cast<r32>(clippedRowCount);
    }

    // NOTE(KN): This rescales the lerped perspective Z value to be a
    // value between 0 and 1 with 1 being closest to the viewpoint and
    // 0 being at the z range equating to max view distance for this
    // sector. Derived from (max - z) / max [with z substituded by 1/z]
    r32 invertDepth = 1.0f - (colourScaling / depth);

    // Apply per-surface shading offset and clamp    
    r32 depthShade = vclib::clampValue(invertDepth * surface->shade, 0.0f, 1.0f);
               
    u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget);
    u8 *columnStart = pixelPt + initialRow * displayPitch;

    // Set base address for this texture column in our rotated texture
    u32 *textureColumn = &(surface->texelData[indexU * surface->height]);
    u32 vTexelMask = surface->height - 1;

    // Determine whether we can use the fast masking version (for power of 2
    // texture heights only), or whether modulus is necessary
    if ((surface->height & vTexelMask) == 0)
    {
        for (i32 loop = startRow; loop < endRow; loop++)
        {
            r32 texelV = vclib::lerp(surface->texelA.v, alpha, surface->texelB.v);
            i32 indexV = vclib::roundToInt32(texelV) & vTexelMask;
        
            u32 *pixelOffset = reinterpret_cast<u32 *>(columnStart) + screenColumn;
            u32 packedTexel = textureColumn[indexV];
        
            // Apply depth shading to BGR colour elements (don't touch alpha)
            u8 *componentTexel = reinterpret_cast<u8 *>(&packedTexel);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
                   
            *pixelOffset = packedTexel;
        
            columnStart += rowStepping;
            alpha += alphaGradient;
        }    
    }
    else
    {
        for (i32 loop = startRow; loop < endRow; loop++)
        {
            r32 texelV = vclib::lerp(surface->texelA.v, alpha, surface->texelB.v);
            i32 indexV = vclib::roundToInt32(texelV) % surface->height;
        
            u32 *pixelOffset = reinterpret_cast<u32 *>(columnStart) + screenColumn;
            u32 packedTexel = textureColumn[indexV];
        
            // Apply depth shading to BGR colour elements (don't touch alpha)
            u8 *componentTexel = reinterpret_cast<u8 *>(&packedTexel);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
                   
            *pixelOffset = packedTexel;
        
            columnStart += rowStepping;
            alpha += alphaGradient;
        }    
    }
}

void RenderScene::transparentStrip(i32 screenColumn, r32 texelU, MappedTexture *surface, r32 start, r32 end,
                                   r32 clampLow, r32 clampHigh, r32 depth)
{
    if (start > end || surface->texelData == nullptr)
        return;

    i32 indexU = vclib::roundToInt32(texelU) % surface->width;    
    r32 alphaGradient = 1.0f / (end - start);
    r32 alpha = 0.0f;    
    
    i32 startRow = vclib::roundToInt32(vclib::clampValue(start, clampLow, clampHigh));
    i32 endRow = vclib::roundToInt32(vclib::clampValue(end, clampLow, clampHigh));

    i32 clippedRowCount;
    i32 initialRow;
    i32 rowStepping;

    // Set parameters for top down or bottom up texture alignment
    if (surface->invert == true)
    {
        clippedRowCount = vclib::roundToInt32(end) - endRow;
        initialRow = endRow - 1;
        rowStepping = -displayPitch;
    }
    else
    {
        clippedRowCount = startRow - vclib::roundToInt32(start);
        initialRow = startRow;
        rowStepping = displayPitch;
    }
    
    if (clippedRowCount > 0)
    {
        // Advance lerp to point that matches screen clip
        alpha += alphaGradient * static_cast<r32>(clippedRowCount);
    }

    // NOTE(KN): This rescales the lerped perspective Z value to be a
    // value between 0 and 1 with 1 being closest to the viewpoint and
    // 0 being at the z range equating to max view distance for this
    // sector. Derived from (max - z) / max [with z substituded by 1/z]
    r32 invertDepth = 1.0f - (colourScaling / depth);

    // Apply per-surface shading offset and clamp    
    r32 depthShade = vclib::clampValue(invertDepth * surface->shade, 0.0f, 1.0f);
               
    u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget);
    u8 *columnStart = pixelPt + initialRow * displayPitch;

    // Set base address for this texture column in our rotated texture
    u32 *textureColumn = &(surface->texelData[indexU * surface->height]);
    u32 vTexelMask = surface->height - 1;

    // Determine whether we can use the fast masking version (for power of 2
    // texture heights only), or whether modulus is necessary
    for (i32 loop = startRow; loop < endRow; loop++)
    {
        r32 texelV = vclib::lerp(surface->texelA.v, alpha, surface->texelB.v);
        i32 indexV = vclib::roundToInt32(texelV) % surface->height;
        
        u32 *pixelOffset = reinterpret_cast<u32 *>(columnStart) + screenColumn;
        u32 packedTexel = textureColumn[indexV];

        if ((packedTexel & 0xFF000000) == 0)
        {
            // Apply depth shading to BGR colour elements (don't touch alpha)
            u8 *componentTexel = reinterpret_cast<u8 *>(&packedTexel);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
            *componentTexel = (u8)((r32)(*componentTexel++) * depthShade);
                   
            *pixelOffset = packedTexel;
        }
                
        columnStart += rowStepping;
        alpha += alphaGradient;
    }    
}

void RenderScene::deferredStrip(BackRenderTexture *stackFront, i32 startColumn, i32 screenColumn, r32 texelU,
                                r32 start, r32 end, r32 clampLow, r32 clampHigh, r32 depth)
{
    if (start > end)
        return;

    DeferredStrip *stripData = &(stackFront->renderStripArray[screenColumn-startColumn]);
    stripData->render = true;
    stripData->column = screenColumn;
    stripData->texelU = texelU;
    stripData->floorPoint = start;
    stripData->ceilingPoint = end;
    stripData->lowerClip = clampLow;
    stripData->upperClip = clampHigh;
    stripData->depth = depth;
}

RenderScene::FlatPlane *
RenderScene::makeFlatPlane(WorldObjects::SectorDef::PlaneDef *plane,
                           FlatPlane **allocList, MemoryArena *frameArena,
                           r32 startX, r32 endX, r32 clampMin, r32 clampMax)
{
    r32 planeMin = vclib::clampValue(startX, clampMin, clampMax);
    r32 planeMax = vclib::clampValue(endX, clampMin, clampMax);
    
    // Allocate flat plane structure with appropriately sized
    // column limit arrays
    FlatPlane *planeStorage = frameArena->pushStruct<FlatPlane>();

    planeStorage->minX = vclib::roundToInt32(planeMin);
    planeStorage->maxX = vclib::roundToInt32(planeMax);
    i32 planeColumnCount = (planeStorage->maxX - planeStorage->minX) + 1;

    planeStorage->extentX = planeColumnCount;
    planeStorage->startY = frameArena->pushArray<r32>(planeColumnCount);
    vclib::memoryInit(planeStorage->startY, sizeof(r32)*planeColumnCount);
    planeStorage->endY = frameArena->pushArray<r32>(planeColumnCount);
    vclib::memoryInit(planeStorage->endY, sizeof(r32)*planeColumnCount);

    planeStorage->height = plane->height;
    planeStorage->texWidth = plane->texture.width;
    planeStorage->texHeight = plane->texture.height;
    planeStorage->texelData = plane->texture.data;
    planeStorage->uScale = plane->uScaling;
    planeStorage->vScale = plane->vScaling;
    planeStorage->shade = plane->shading;
    planeStorage->skybox = plane->skybox;
    planeStorage->visibility = plane->applyVisibility;
    
    // Push storage for this plane onto the allocation list
    planeStorage->next = *allocList;
    *allocList = planeStorage;    
        
    return planeStorage;
}

void RenderScene::addStripToFlatPlane(FlatPlane *plane, i32 screenColumn,
                                      r32 start, r32 end, r32 clampLow, r32 clampHigh)
{
    i32 relativeIndex = screenColumn - plane->minX;
    platformAssert(relativeIndex < plane->extentX);

    if (start > end)
    {
        plane->startY[relativeIndex] = -1.0f;
        plane->endY[relativeIndex] = -1.0f;
    }
    else
    {
        plane->startY[relativeIndex] = vclib::clampValue(start, clampLow, clampHigh);
        plane->endY[relativeIndex] = vclib::clampValue(end, clampLow, clampHigh);
    }
}

RenderScene::MappedTexture
RenderScene::initTextureSurface(WorldObjects::SectorDef::WallDef *surface,
                                WorldObjects::SectorDef::WallTexture *surfaceTexture,
                                vfVec2 clipWallA, vfVec2 clipWallB,
                                vfVec2 unclipWallA, r32 wallHeight)
{
    MappedTexture mapData;

    if (wallHeight == 0 || surfaceTexture->map.data == nullptr)
    {
        mapData.mapValid = false;
        return mapData;
    }
        
    r32 texelScalingU = surface->uScaling;
    r32 texelScalingV = surface->vScaling;
    
    r32 texelU0 = surface->xOffset + distance(clipWallA, unclipWallA) * texelScalingU;
    r32 texelU1 = surface->xOffset + distance(clipWallB, unclipWallA) * texelScalingU;

    mapData.texelA.u = texelU0 / clipWallA.y;
    mapData.texelA.v = surface->yOffset;
    mapData.texelB.u = texelU1 / clipWallB.y;
    mapData.texelB.v = surface->yOffset + wallHeight * texelScalingV;
    mapData.depthA = 1.0f / clipWallA.y;
    mapData.depthB = 1.0f / clipWallB.y;

    // Invert texture v to support bottom up alignment of texture to surface
    if (surfaceTexture->alignTopDown == false)
    {
        mapData.texelA.v = mapData.texelA.v + surfaceTexture->map.height;
        mapData.texelB.v = surfaceTexture->map.height - mapData.texelB.v;        
    }

    mapData.shade = surface->shading;
    mapData.mapValid = true;
    mapData.invert = surfaceTexture->alignTopDown;

    mapData.width = surfaceTexture->map.width;
    mapData.height = surfaceTexture->map.height;
    mapData.texelData = surfaceTexture->map.data;
    
    return mapData;   
}
