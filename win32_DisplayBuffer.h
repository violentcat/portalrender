#if !defined(WIN32_DISPLAYBUFFER_H)
#define WIN32_DISPLAYBUFFER_H

class win32DisplayBuffer
{
public:

    struct Dimensions
    {
        i32 width;
        i32 height;
    };

    struct BufferInfo
    {
        i32 width;
        i32 height;
        i32 bytesPerPixel;
        i32 pitch;

        BITMAPINFO bitmapInfo;
        void *pixelData;
    };

    BufferInfo displayBuffer;

    
public:

    Dimensions GetDeviceDimensions(HWND windowHandle);   
    void createDisplayBuffer(i32 width, i32 height);
    
    void updateDeviceFromBuffer(HDC deviceContext, i32 width, i32 height);
};

#endif
