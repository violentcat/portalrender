#if !defined(WORLDOBJECTS_H)
#define WORLDOBJECTS_H

#include "PlatformTypes.h"
#include "VectorFunctions.h"
#include "WorldReference.h"
#include "AssetCache.h"

class WorldObjects
{
public:

    vfVec2 worldExtent = {};
    r32 viewDistance;
    r32 verticalFov;
    r32 horizontalFov;
    
    struct SectorDef
    {
        struct PlaneDef
        {
            r32 height;
            AssetCache::TexBlockType texture;
            r32 shading;
            r32 uScaling;
            r32 vScaling;
            bool skybox;
            bool applyVisibility;
        };

        PlaneDef ceiling;
        PlaneDef floor;        

        struct WallTexture
        {
            AssetCache::TexBlockType map;
            bool alignTopDown;
        };
        
        struct WallDef
        {
            vfVec2 start;
            vfVec2 end;
            r32 wallLength;
            i32 travelToSector;
            WallTexture lowTexture;
            WallTexture highTexture;
            WallTexture wallTexture;
            r32 shading;
            r32 xOffset;
            r32 yOffset;
            r32 uScaling;
            r32 vScaling;
            bool blocking;
            bool traverse;
        };

        i32 wallCount;
        WallDef lineSegment[WorldRef::maxWallsPerSector];
    };
    
    i32 vertexCount;
    i32 sectorCount;
    vfVec2 *vertexList;
    SectorDef *sectorList;

    struct PlayerDef
    {
        vfVec2 position;
        vfVec2 velocity;
        r32 height;
        r32 upVelocity;
        r32 orientation;
        i32 sector;
    };

    PlayerDef player;


public:

    WorldObjects(MemoryArena *persistStore);
    
    void updateEnvironment(r32 fwdAcceleration, r32 sideAcceleration, r32 lookDelta, r32 timeStep,
                           r32 verticalVelocity);
    vfVec2 checkPlayerWallInteraction(vfVec2 newPosition);

    void unpackLevelGeometry(MemoryArena *persistStore, ImportType *assetImporter);
        
        
private:

    void unpackDuke3DLevelFormat(u8 *dataBlock, i32 dataSize, MemoryArena *persistStore);
};

#endif
