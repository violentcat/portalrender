
#include "AssetCache.h"
#include "ImportType.h"

void AssetCache::linkRawAssetData(MemoryArena *arena, u8 *data, i32 dataSize,
                                  AssetIdentity id, AssetType type)
{
    AssetBlock *newAsset = addAssetToList(arena, &rawAssetList, id, type);
    newAsset->impl.raw.data = data;
    newAsset->impl.raw.length = dataSize;
}

void AssetCache::linkRawAssetString(MemoryArena *arena, vclib::vcString strData, AssetIdentity id)
{
    linkRawAssetData(arena, reinterpret_cast<u8 *>(strData), vclib::length(strData),
                     id, STR);
}

void AssetCache::convertRawDataToInternal(MemoryArena *arena, MemoryArena *unpack, ImportType *assetImporter)
{
    AssetBlock *asset;
    
    // First pass of conversion is for individually specified assets that
    // are assumed to have no dependency restrictions
    asset = rawAssetList.head;
    while (asset)
    {
        switch(asset->type)
        {
            case RAW:
            case STR:
            {
                // NOTE(KN): This assumes that the raw asset data is already
                // stored in a persistent arena so we can just copy the pointer
                // to the data rather than the whole data block itself
                     
                AssetBlock *converted = addAssetToList(arena, &internalAssetList, asset->id, asset->type);
                converted->impl.raw.data = asset->impl.raw.data;
                converted->impl.raw.length = asset->impl.raw.length;                
            }
            break;

            case PPM:
            {
                unpackPPMFormatAsset(arena, asset);                
            }
            break;

            default:
                break;
        };

        asset = asset->next;
    }

    // Second pass of conversion is for container (group) assets. This allows
    // for dependencies to individual assets external to the container. Any
    // dependencies internal to the container must be handled by the container
    // unpacker itself
    
    asset = rawAssetList.head;
    while (asset)
    {
        assetImporter->unpackContainer(unpack, asset, this, arena);
        asset = asset->next;
    }
}

AssetCache::AssetBlock * AssetCache::createAssetBlock(MemoryArena *arena, AssetIdentity id, AssetType type)
{
    // Just redirect this to the internal asset list
    return addAssetToList(arena, &internalAssetList, id, type);    
}

AssetCache::AssetBlock AssetCache::findAsset(AssetIdentity id, i32 index)
{
    AssetBlock nulAsset = {};

    AssetBlock *asset = internalAssetList.head;
    while(asset)
    {
        if (asset->id == id && (index == -1 || index == asset->index))
            return *asset;
        
        asset = asset->next;
    }
    
    return nulAsset;
}

AssetCache::AssetBlock AssetCache::findAssetByName(AssetIdentity id, vclib::vcString name)
{
    AssetBlock nulAsset = {};

    AssetBlock *asset = internalAssetList.head;
    while(asset)
    {
        if (asset->id == id && vclib::compare(name, asset->name) == true)
            return *asset;
        
        asset = asset->next;
    }
    
    return nulAsset;
}

AssetCache::TexBlockType AssetCache::findTexture(i32 index)
{
    TexBlockType nulAsset = {};

    AssetBlock *asset = internalAssetList.head;
    while(asset)
    {
        if (asset->id == AssetIdentity::IndexedTexture && index == asset->index)
            return (*asset).impl.tex;
        
        asset = asset->next;
    }
    
    return nulAsset;
}

AssetCache::AssetBlock *
AssetCache::addAssetToList(MemoryArena *arena, AssetList *list, AssetIdentity id, AssetType type)
{
    AssetBlock *assetStore = arena->pushStruct<AssetBlock>();
    assetStore->id = id;
    assetStore->type = type;
    assetStore->name = nullptr;
    assetStore->next = nullptr;

    // Push storage for this asset onto the allocation list
    if (list->tail == nullptr)
    {
        list->head = list->tail = assetStore;
    }
    else
    {
        list->tail->next = assetStore;
        list->tail = assetStore;
    }
    
    return assetStore;
}

AssetCache::AssetBlock *AssetCache::accessExistingAsset(AssetIdentity id, i32 index)
{
    AssetBlock *nulAsset = nullptr;

    AssetBlock *asset = internalAssetList.head;
    while(asset)
    {
        if (asset->id == id && (index == -1 || index == asset->index))
            return asset;
        
        asset = asset->next;
    }
    
    return nulAsset;
}

void AssetCache::unpackPPMFormatAsset(MemoryArena *arena, AssetBlock *asset)
{
    u8 *dataPos = asset->impl.ppm.data;
    i32 dataToConsume = asset->impl.ppm.length;

    if (dataToConsume > 2 && *dataPos++ == 'P')
    {
        char ppmType = *dataPos++;
        dataToConsume -= 2;

        PPMHeaderBlock header;
        i32 headerLength = readPPMHeader(arena, &header, dataPos, dataToConsume);
        i32 pixelDataLength = dataToConsume - headerLength;
       
        if (headerLength > 0 && pixelDataLength > 0)
        {
            AssetBlock *converted = addAssetToList(arena, &internalAssetList, AssetIdentity::IndexedTexture, TEX);
            converted->index = asType(asset->id);
            
            // Create texture storage block
            converted->impl.tex.data = arena->pushArray<u32>(header.width * header.height);
            converted->impl.tex.width = header.width;
            converted->impl.tex.height = header.height;

            switch(ppmType)
            {
                case '6':
                {
                    // NOTE(KN): Binary PPM format has a single byte of whitespace
                    // before the first actual pixel data starts. We don't care what
                    // it is
                    dataPos += (headerLength + 1);
                    unpackBinaryPPM(dataPos, pixelDataLength-1, &(converted->impl.tex));
                }
                break;

                default:
                    break;                
            };
        }
    }
}

bool AssetCache::isEndOfLine(char token)
{
    bool result = (token == '\n' || token == 'r');
    return result;
}

bool AssetCache::isWhitespace(char token)
{
    bool result = (token == ' '  || token == '\t' ||
                   token == '\v' || token == '\f' ||
                   isEndOfLine(token));
    return result;
}

bool AssetCache::isNumeric(char token)
{
    bool result = !(token < '0' || token > '9');
    return result;
}

i32 AssetCache::readPPMHeader(MemoryArena *arena, PPMHeaderBlock *header, u8 *data, i32 length)
{
    u8 *buffer = data;
    i32 remaining = length;

    vclib::vcString elementArray[HDR_ELEMENT_CNT] = {};
    for (i32 loop = 0; loop < HDR_ELEMENT_CNT; loop++)
    {
        i32 elementLength = readElement(arena, buffer, remaining, &elementArray[loop]);
        if (elementLength > 0 && elementArray[loop])
        {
            remaining -= elementLength;
            buffer += elementLength;
        }
        else
        {
            return 0;
        }
    }

    // NOTE(KN): Wouldn't it be nice if a language supported enumerating
    // through the members of a struct so we could do this as part of a loop
    header->width = vclib::convertStringToInt32(elementArray[0]);
    header->height = vclib::convertStringToInt32(elementArray[1]);
    header->maxPixelValue = vclib::convertStringToInt32(elementArray[2]);

    // NOTE(KN): Only supporting 8bit colour values from source image
    if (header->maxPixelValue > 255)
    {
        return 0;
    }
    
    i32 finalHeaderLength = length - remaining;
    return finalHeaderLength;        
}

i32 AssetCache::readElement(MemoryArena *arena, u8 *data, i32 length, vclib::vcString *element)
{
    vclib::vcString elementString = nullptr;
    u8 *readValue = data;
    i32 bytesConsumed = 0;
    
    u8 *gatherStart = nullptr;
    bool gatherString = false;
    
    while (bytesConsumed < length)
    {
        if (gatherString == true && isNumeric(*readValue) == false)
        {
            gatherString = false;
            i32 gatherLength = readValue - gatherStart;
            elementString = vclib::stringFromBuffer(arena, gatherStart, gatherLength);
            break;
        }
        
        if (isWhitespace(*readValue))
        {
            // NOP
        }
        else if (isNumeric(*readValue))
        {
            if (gatherString == false)
            {
                gatherString = true;
                gatherStart = readValue;
            }
        }
        else
        {
            while (bytesConsumed < length)
            {
                // Any token other than whitespace or numeric values is
                // assumed to be the start of a line we're not interested
                // in (comment etc) so flush to EOL
                if (isEndOfLine(*readValue))
                {
                    break;
                }

                readValue++;
                bytesConsumed++;                
            }
        }

        readValue++;
        bytesConsumed++;        
    }

    *element = elementString;
    return elementString ? bytesConsumed : 0;
}

void AssetCache::unpackBinaryPPM(u8 *data, i32 length, TexBlockType *texture)
{
    // Read source data as 8-bit colour values with RGB ordering and convert
    // into ARGB packed 32-bit values rotated by 90 deg. Wall textures are
    // rendered in columns so re-orienting the texture to match makes the
    // memory access much more cache friendly

    u8 *readIndex = data;
    u32 *writeIndex = texture->data;

    for (i32 offseth = 0; offseth < texture->height; offseth++)
    {
        u32 *writeRotated = &(writeIndex[texture->height - offseth]);

        for (i32 offsetw = 0; offsetw < texture->width; offsetw++)
        {
            u32 pixelValue = (*readIndex++) << 16;
            pixelValue |= (*readIndex++) << 8;
            pixelValue |= (*readIndex++);

            *writeRotated = pixelValue;
            writeRotated += texture->height;
        }
    }
}



