#if !defined(GRPIMPORT_H)
#define GRPIMPORT_H

#include "ImportType.h"

class GRPImport : public ImportType
{
public:

    void makeImportType(MemoryArena *tempArena) override;    
    void unpackContainer(MemoryArena *tempArena, AssetCache::AssetBlock *containerAsset,
                         AssetCache *assetStore, MemoryArena *gameArena) override;
    void unpackLevelData(MemoryArena *tempArena, AssetCache *assetStore, WorldObjects *levelData) override;    
       

private:

    
private:

};

#endif
