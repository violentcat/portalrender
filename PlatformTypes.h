#if !defined(PLATFORMTYPES_H)
#define PLATFORMTYPES_H

#include <stdint.h>

#ifdef PLATFORM_DEBUG
#define platformAssert(Expression) if (!(Expression)) {__debugbreak();}
#else
#define platformAssert(Expression)
#endif

#define countOf(array) sizeof(array) / sizeof(array[0])

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float r32;
typedef double r64;


#define DYNAMIC_CALLDEF(returnType, returnStub, function, ...)               \
typedef returnType WINAPI function##_func (__VA_ARGS__);                     \
static returnType WINAPI stub_##function (__VA_ARGS__) {return returnStub;}  \
static function##_func * function##_Dynamic = stub_##function;               \

#define DYNAMIC_CALLLOAD(handle, function)                                                   \
function##_Dynamic = reinterpret_cast<function##_func *>(GetProcAddress(handle, #function)); \
if (!function##_Dynamic) { function##_Dynamic = stub_##function; }                           \

#endif
