
#include "CustomImport.h"
#include "WorldObjects.h"

namespace
{
    enum class LEXERTOKEN : u8
    {
        TEXT,
        NUMBER
    };

    u8 asType(LEXERTOKEN type) {return static_cast<u8>(type);}

    const r32 customAxisScale = 3.f;    
};

void CustomImport::makeImportType(MemoryArena *tempArena)
{
    // No custom allocations necessary for processing this type
}

void CustomImport::unpackContainer(MemoryArena *tempArena, AssetCache::AssetBlock *containerAsset,
                                   AssetCache *assetStore, MemoryArena *gameArena)
{
    // No container to unpack, it's all direct asset loads
}

void CustomImport::unpackLevelData(MemoryArena *tempArena, AssetCache *assetStore, WorldObjects *levelData)
{
    typedef AssetCache::AssetIdentity Asset_Id;
    typedef AssetCache::AssetType Asset_Type;

    // This is a custom importer so make sure there are appropriate level assets
    AssetCache::AssetBlock levelType = assetStore->findAsset(Asset_Id::LevelData);
    if (!(levelType.type == Asset_Type::RAW && levelType.impl.raw.length > 0))
        return;
    
    levelData->player = {};
    levelData->vertexCount = 0;
    levelData->sectorCount = 0;
    for (i32 loop = 0; loop < WorldRef::maxSectorCount; loop++)
    {
        levelData->sectorList[loop].wallCount = 0;
    }

    levelData->worldExtent = {20.0f,30.0f};
    levelData->viewDistance = 10.0f;
    levelData->verticalFov = 166.f * vclib::to_radians;
    levelData->horizontalFov = 90.f * vclib::to_radians;
    
    lexicalRead(levelType.impl.raw.data, levelType.impl.raw.length, tempArena);
    parseGeometry(tokenSentinel, assetStore, levelData);
}

void CustomImport::lexicalRead(u8 *dataBlock, i32 dataSize, MemoryArena *persistStore)
{
    // Lexer operation
    // 1. Remove leading whitespace
    // 2. Record start position of symbol
    // 3. Seek to end of symbol
    // 4. Record end position of symbol
    // 5. Store symbol type (text / numeric), length and symbol
    // 6. Repeat
    
    u8 *tokenStart = nullptr;
    u8 *tokenIterator = dataBlock;
    u8 *endOfBlock = &dataBlock[dataSize];

    bool processingToken = false;
    
    while(tokenIterator != endOfBlock)
    {
        if (processingToken == false)
        {
            if (*tokenIterator > 0x20)
            {
                processingToken = true;
                tokenStart = tokenIterator;
            }
        }
        else
        {
            if (*tokenIterator <= 0x20)
            {
                processingToken = false;
                appendTokenList(tokenStart, tokenIterator, persistStore);
            }
        }

        tokenIterator++;
    }

    // Close off an in progress token at the end of the block
    if (processingToken == true)
    {
        appendTokenList(tokenStart, tokenIterator, persistStore);
    }
}

void CustomImport::appendTokenList(u8 *tokenStart, u8 *tokenEnd, MemoryArena *persistStore)
{
    u8 tokenType = (*tokenStart > 0x39) ? asType(LEXERTOKEN::TEXT) : asType(LEXERTOKEN::NUMBER);
    i32 tokenSize = tokenEnd - tokenStart;

    platformAssert(tokenSize <= 255);

    TokenLink *createToken = persistStore->pushStruct<TokenLink>();
    createToken->tokenType = tokenType;
    createToken->tokenLength = tokenSize;
    createToken->tokenString = vclib::stringFromBuffer(persistStore, tokenStart, tokenSize);
    createToken->nextToken = nullptr;
    
    if (tokenSentinel)
    {
        tokenListTail->nextToken = createToken;
        tokenListTail = createToken;
    }
    else
    {
        tokenSentinel = createToken;
        tokenListTail = tokenSentinel;
    }
}

void CustomImport::parseGeometry(TokenLink *parseTokens, AssetCache *assetStore, WorldObjects *levelData)
{
    TokenLink *currentToken = parseTokens;

    while (currentToken)
    {
        TokenLink *nextToken = currentToken->nextToken;
        
        if (currentToken->tokenType == asType(LEXERTOKEN::TEXT))
        {
            if (nextToken)
            {
                if (vclib::compareRaw(currentToken->tokenString, "vertex"))
                {
                    nextToken = processVertexGroup(nextToken, levelData);
                }
                else if (vclib::compareRaw(currentToken->tokenString, "sector"))
                {
                    nextToken = processSectorGroup(nextToken, assetStore, levelData);
                }
                else if (vclib::compareRaw(currentToken->tokenString, "player"))
                {
                    nextToken = processPlayerGroup(nextToken, levelData);
                }
            }            
        }
        else
        {
            // TODO(KN): Better error reporting
            return;
        }

        currentToken = nextToken;
    }    
}

CustomImport::TokenLink * CustomImport::processVertexGroup(TokenLink *firstTokenInGroup, WorldObjects *levelData)
{
    TokenLink *currentToken = firstTokenInGroup;
    bool vertexReadActive = false;
    vfVec2 vertexRead = {};

    while (currentToken && currentToken->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        if (vertexReadActive == false)
        {
            vertexRead.x = vclib::convertStringToReal32(currentToken->tokenString) / customAxisScale;
            vertexReadActive = true;
        }
        else
        {
            if (levelData->vertexCount < WorldRef::maxVertexCount)
            {
                vertexRead.y = vclib::convertStringToReal32(currentToken->tokenString) / customAxisScale;
                levelData->vertexList[levelData->vertexCount++] = vertexRead;
            }
        }
        
        currentToken = currentToken->nextToken;
    }
    
    return currentToken;
}

CustomImport::TokenLink * CustomImport::processSectorGroup(TokenLink *firstTokenInGroup,
                                                           AssetCache *assetStore,
                                                           WorldObjects *levelData)
{
    TokenLink *currentToken = firstTokenInGroup;

    r32 floorHeight = 0.0f;
    r32 ceilingHeight = 0.0f;
    
    if (currentToken && currentToken->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        floorHeight = vclib::convertStringToReal32(currentToken->tokenString);
        currentToken = currentToken->nextToken;
    }

    if (currentToken && currentToken->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        ceilingHeight = vclib::convertStringToReal32(currentToken->tokenString);
        currentToken = currentToken->nextToken;
    }

    i32 sectorEdgeCount = 0;
    TokenLink *sectorEndSearch = currentToken;
    while (sectorEndSearch && sectorEndSearch->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        sectorEdgeCount++;
        sectorEndSearch = sectorEndSearch->nextToken;
    }

    typedef AssetCache::AssetIdentity Asset_Id;
    AssetCache::TexBlockType lowTextureData = assetStore->findTexture(assetStore->asType(Asset_Id::PortalTexture));
    AssetCache::TexBlockType highTextureData = assetStore->findTexture(assetStore->asType(Asset_Id::Texture));
    
    WorldObjects::SectorDef *loadSec = &(levelData->sectorList)[levelData->sectorCount];
    
    if (levelData->sectorCount < WorldRef::maxSectorCount)
    {        
        sectorEdgeCount >>= 1;

        for (i32 loop = 0; loop < sectorEdgeCount; loop++)
        {
            i32 vertexIndex = vclib::convertStringToInt32(currentToken->tokenString);
            WorldObjects::SectorDef::WallDef *currentWall = &(loadSec->lineSegment[loop]);

            currentWall->end = levelData->vertexList[vertexIndex];
            currentToken = currentToken->nextToken;

            if (loop)
            {
                currentWall->start = loadSec->lineSegment[loop-1].end;
            }

            currentWall->lowTexture.map = lowTextureData;
            currentWall->lowTexture.alignTopDown = false;
            currentWall->highTexture.map = highTextureData;
            currentWall->highTexture.alignTopDown = false;
            currentWall->wallTexture.map.data = nullptr;
            currentWall->wallTexture.alignTopDown = false;
            currentWall->shading = 1.f;
            currentWall->xOffset = 0.f;
            currentWall->yOffset = 0.f;            
            currentWall->uScaling = static_cast<r32>(lowTextureData.width) / 1.5f;
            currentWall->vScaling = static_cast<r32>(lowTextureData.height) / 12.f;
            currentWall->blocking = false;
            currentWall->traverse = true;            
        }

        loadSec->lineSegment[0].start = loadSec->lineSegment[sectorEdgeCount-1].end;

        for (i32 loop = 0; loop < sectorEdgeCount; loop++)
        {
            WorldObjects::SectorDef::WallDef *currentWall = &(loadSec->lineSegment[loop]);
            currentWall->wallLength = distance(currentWall->end, currentWall->start);

            currentWall->travelToSector = vclib::convertStringToInt32(currentToken->tokenString);
            if (currentWall->travelToSector == -1)
            {
                currentWall->wallTexture.map = currentWall->highTexture.map;
                currentWall->blocking = true;
            }
            currentToken = currentToken->nextToken;
        }    

        loadSec->floor.texture = assetStore->findTexture(assetStore->asType(Asset_Id::FloorTexture));
        loadSec->ceiling.texture = assetStore->findTexture(assetStore->asType(Asset_Id::CeilingTexture));
        
        loadSec->wallCount = sectorEdgeCount;
        loadSec->floor.height = floorHeight;
        loadSec->ceiling.height = ceilingHeight;
        loadSec->floor.shading = 1.f;
        loadSec->floor.skybox = false;
        loadSec->floor.applyVisibility = true;
        loadSec->floor.uScaling = static_cast<r32>(loadSec->floor.texture.width) / 4.0f;
        loadSec->floor.vScaling = static_cast<r32>(loadSec->floor.texture.height) / 4.0f;
        loadSec->ceiling.shading = 1.f;
        loadSec->ceiling.skybox = false;
        loadSec->ceiling.applyVisibility = false;
        loadSec->ceiling.uScaling = static_cast<r32>(loadSec->ceiling.texture.width) / 4.0f;
        loadSec->ceiling.vScaling = static_cast<r32>(loadSec->ceiling.texture.height) / 4.0f;
        levelData->sectorCount++;
    }
    
    return sectorEndSearch;
}

CustomImport::TokenLink * CustomImport::processPlayerGroup(TokenLink *firstTokenInGroup, WorldObjects *levelData)
{
    TokenLink *currentToken = firstTokenInGroup;
    
    if (currentToken && currentToken->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        levelData->player.position.y = vclib::convertStringToReal32(currentToken->tokenString) / customAxisScale;
        currentToken = currentToken->nextToken;
    }

    if (currentToken && currentToken->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        levelData->player.position.x = vclib::convertStringToReal32(currentToken->tokenString) / customAxisScale;
        currentToken = currentToken->nextToken;
    }

    if (currentToken && currentToken->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        levelData->player.orientation = vclib::convertStringToReal32(currentToken->tokenString);
        currentToken = currentToken->nextToken;
    }

    if (currentToken && currentToken->tokenType == asType(LEXERTOKEN::NUMBER))
    {
        levelData->player.sector = vclib::convertStringToInt32(currentToken->tokenString);
        currentToken = currentToken->nextToken;
    }
    
    return currentToken;
}


