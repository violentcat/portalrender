#if !defined(VC_STRINGFUNCTIONS_H)
#define VC_STRINGFUNCTIONS_H

#include "vc_String.h"

// TODO(KN):
// Container manipulations
// Find / Replace (case sensistive on and off)
// Internationalisation
// Conversion to and from fundamental types
// UTF-8 / UTF-16 support etc
// Thread safe

namespace vclib
{
    bool compare(vcString first, vcString second);
    bool compareRaw(vcString first, char *raw, i32 length = 0);

    void concatenate(vcString dst, vcString string);
    void concatenateRaw(vcString dst, char *raw, i32 length = 0);

    void clipStringToRange(vcString string, i32 start, i32 end);
    void lower(vcString string);
    
    r32 convertStringToReal32(vcString string);
    i32 convertStringToInt32(vcString string);
};

#endif
