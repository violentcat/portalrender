# Portal Render

This project was started after watching Joel Yliluoma (Bisqwit) present a video entitled 'Creating a Doom-style 3D engine in C'. (https://www.youtube.com/watch?v=HQYsFshbkYw)
The initial aim was to educate myself on the rendering techniques used in that era of game engines and reproduce the steps taken in the video. Without, of course, looking at the original source code. 

For the most part this worked, with a couple of sneak peaks to work out some of the details of Joel's custom map format.

So firstly, of course, thanks to Joel for putting together such an excellent video.

This particular type of renderer is called a Portal Renderer, a technique used by Build engine games such as Duke Nukem 3D.
Although the results are similar on screen this is quite different to the BSP tree system used by Doom.

There were many optimisations used in the engines of the time just to get the games running on the limited hardware.
My implementation is more focussed on just getting the right results. So most parts are calculated in floating point, it uses perspective projection and even does a 2D version of frustum culling to reduce unwanted draw calculations. 

However, one important place where the rendering does use the original techniques is in the texture mapping.

For this, the engines made assumptions about the nature of walls. i.e. The top and bottom vertex of a wall are the same distance from the player, and therefore texture mapping can be performed using a much more efficient linear interpolation rather than having to calculate perspective of each pixel individually. A similar thing is true for ceilings and floors, whereby a horizontal strip of pixels can be assumed to be the same distance from the player, therefore limiting the number of perspective calculations that are necessary.

The downside of these optimisations is that, for the assumption to be valid, the engine couldn't allow the player to look up and down.

Here's the result:

![Portal Render](images/custom.png)

A few of the features supported by this implementation.

- Wall culling against view frustum
- Texture mapped walls and floors using original linear mapping techniques
- Continuous texture shading based on distance from camera
- Support for keyboard and gamepad

## Build Engine

After the initial development, I decided it would be interesting to add support for the container file format used by the Build engine.
This would open up access to some additional maps for testing, and even potentially show actual game levels. Very little was changed in the renderer to support this, the challenge was in collecting together enough online information about the file format. Although, in the end, not all engine features were implemented (e.g. no support for sloping floors), it is enough to display simpler test maps and a few in game levels.

![DUKE3D Render](images/dukee1l3.png)

## Doom Engine

Throughout development, an eventual goal of this project was to be able to render and navigate through a Doom level. Not to display any of the sprites, or be able to actually play the game. But just to show the geometry of the level with correct texturing. The challenge here is that Doom doesn't use portal rendering, so it was necessary to construct the portal data structures from the Doom WAD level data. I had this as a single line in the TODO for the project but, naturally, it ended being one of the longest running pieces of work. I wasn't able to find anything elsewhere on the internet covering exactly this process so, in case anybody else finds it interesting, I have written it up in more detail in the following blog post. (http://violentcat.co.uk/cplusplus/rendering/2020/06/17/portal-render.html)

![Doom Render](images/doome1m1.png)

## Usage

There are 2 arguments that should be passed to the executable:

**Argument 1:** Input type (custom / duke3d / doom)

**Argument 2:** Level file to load

There are 3 different input types supported by the application.

#### 1. Custom / Bisqwit

**Portal3D.exe custom map-clear.txt**

map-clear.txt is the only available level file in this format.
Although the level file and associated texture assets are provided in this repository for convenience, I make no claim that they are mine or that I originated them.
They are all available from their original source here (https://bisqwit.iki.fi/jutut/kuvat/programming_examples/portalrendering.html)

#### 2. Duke Nukem 3D / Build Engine

**Portal3D.exe duke3d portren1.map**

There are some example .map level files provided in this repository, but as with the custom format I did not create them. 
See (https://bisqwit.iki.fi/jutut/kuvat/programming_examples/portalrendering/portren_duke3dmaps.zip)

Note that, in order to use duke3d format levels, the duke3d.grp file from the original game must be available. I have not included it here.

If you would like to load one of the original Duke3D games levels instead of the example .map files then just replace the portren1.map with a game level name.
These are of the format E#L#.map, where the first # should be replaced with the World number and the second # should be replaced with the level number.

#### 3. Doom Engine

**Portal3D.exe doom E1M1**

Note that, in order to use doom format levels, a WAD file from the original game (shareware or commercial) must be available. I have not included one here.

The game level name is of the format E#M#, where the first # should be replaced with the World number and the second # should be replaced with the level number.

## Controls

Both keyboard and gamepad controls are supported.

| Key         | Gamepad       | Command      |
| ---         | ---           | ---          |
| Arrow Up    | Left Stick    | Forward      | 
| Arrow Down  |               | Back         |
| z           |               | Strafe Left  |
| x           |               | Strafe Right |
| Arrow Left  | Right Stick   | Turn Left    |
| Arrow Right |               | Turn Right   |
| Space       | A Button      | Jump         |
| c           | X Button      | Crouch       |

## Compatibility

Although the Build engine loader works well for the simple example levels, it doesn't fare quite so well with real Duke3D levels.
There are quite a few special features provided by the Build engine, such as sloped floors. Very few of these are currently implemented by this project (they are not required for rendering Doom levels which was the main focus), and therefore there are a lot of elements that are displayed incorrectly or not displayed at all.

## Building

Simply run build.bat from within a Visual Studio command prompt.

## Future Enhancements

The following is a list of potential future enhancements.

- Performance optimisations
- Overlays to show frame time & profiling information
- Support dynamic loading of levels
- Light mapping

