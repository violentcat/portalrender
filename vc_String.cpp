
#include "vc_String.h"

namespace vclib
{

i32 capacity(vcString str)
{
    return STRING_DEF(str)->capacity;
}

i32 length(vcString str)
{
    return STRING_DEF(str)->length;
}

void setLength(vcString str, i32 len)
{
    STRING_DEF(str)->length = len;
    str[len] = 0;
}

vcString stringCreate(MemoryArena *store, i32 length)
{
    platformAssert(length > 0 && length <= StringPathLimit);
    
    vcString returnValue = nullptr;    

    i32 stringEntrySize = sizeof(StringDef) + length + 1;
    vcString allocString = reinterpret_cast<vcString>(store->pushBlock(stringEntrySize));

    if (allocString)
    {
        returnValue = allocString + sizeof(StringDef);                
        STRING_DEF(returnValue)->capacity = length;
        setLength(returnValue, 0);
    }

    return returnValue;
}

vcString stringCopy(MemoryArena *store, vcString str)
{
    vcString returnValue = stringCreate(store, length(str));
    setLength(returnValue, length(str));

    vcString loadString = returnValue;

    for (i32 loop = 0; loop < length(str); loop++)
    {
        *loadString++ = *str++;
    }

    return returnValue;
}

vcString stringFromRaw(MemoryArena *store, char *raw)
{
    u8 *srcBuffer = reinterpret_cast<u8 *>(raw);
    size_t srcLen = strlen(raw);

    return stringFromBuffer(store, srcBuffer, srcLen);
}

vcString stringFromBuffer(MemoryArena *store, u8 *buffer, i32 length)
{
    vcString returnValue = stringCreate(store, length);
    if (returnValue)
    {
        vcString srcBuffer = reinterpret_cast<vcString>(buffer);
        vcString loadString = returnValue;
        i32 checkedLength = length;
        
        for (i32 loop = 0; loop < length; loop++)
        {
            if (*srcBuffer == 0)
            {
                checkedLength = loop;
                break;
            }                
                
            *loadString++ = *srcBuffer++;
        }

        setLength(returnValue, checkedLength);
    }

    return returnValue;
}

}

