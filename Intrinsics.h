#if !defined(INTRINSICS_H)
#define INTRINSICS_H

#include "math.h"

namespace vclib
{
    static const float piflt = 3.141593f;
    static const float two_piflt = piflt * 2.0f;
    static const float half_piflt = piflt / 2.0f;
    static const float to_radians = piflt / 180.0f;
    
    inline r32 sinScalar(r32 angle)
    {
        r32 result = sinf(angle);
        return result;
    }

    inline r32 cosScalar(r32 angle)
    {
        r32 result = cosf(angle);
        return result;
    }

    inline r32 tanScalar(r32 angle)
    {
        r32 result = tanf(angle);
        return result;
    }

    inline r32 clampAngle(r32 angle)
    {
        r32 result = fmodf(angle + piflt, two_piflt);
        if (result < 0.0f)
        {
            result += two_piflt;
        }
        return result - piflt;
    }

    inline r32 squareRoot(r32 value)
    {
        r32 result = sqrtf(value);
        return result;
    }
    
    inline r32 absoluteValue(r32 value)
    {
        r32 result = fabsf(value);
        return result;
    }

    inline r32 roundValue(r32 value)
    {
        r32 result = roundf(value);
        return result;
    }

    inline r32 clampValue(r32 value, r32 min, r32 max)
    {
        r32 result = value;
        if (result < min)
        {
            result = min;
        }
        if (result > max)
        {
            result = max;
        }
        return result;
    }

    inline r32 lerp(r32 start, r32 alpha, r32 end)
    {
        r32 result = (1.0f - alpha) * start + alpha * end;
        return result;
    }
    
    inline i32 roundToInt32(r32 value)
    {
        i32 result = static_cast<i32>(roundf(value));
        return result;
    }
    
    inline u32 roundNormalColourToARGB(r32 r, r32 g, r32 b)
    {
        u32 rVal = roundToInt32(r * 255.0f);
        u32 gVal = roundToInt32(g * 255.0f);
        u32 bVal = roundToInt32(b * 255.0f);
        u32 aVal = 255;
        u32 resultColour = (aVal << 24) | (rVal << 16) | (gVal << 8) | (bVal << 0);
        return resultColour;
    }

    inline r32 scaledInt32ToReal(i32 invertLimit, i32 value, r32 scale)
    {
        r32 result = static_cast<r32>(invertLimit - value) / scale;
        return result;
    }

    inline r32 scaledInt32ToReal(i32 value, r32 scale)
    {
        r32 result = static_cast<r32>(value) / scale;
        return result;
    }    

    inline void memoryInit(void *area, i32 count)
    {
        memset(area, 0, count);
    }
    
    inline void memoryFill(r32 *arrayStart, i32 count, r32 value)
    {
        r32 *itrCurrent = arrayStart;
        r32 *itrEnd = arrayStart + count;
        while(itrCurrent != itrEnd)
        {
            *itrCurrent++ = value;
        }
    }
    
    template<typename T>
    T maxValue(T first, T second)
    {
        return first > second ? first : second;
    }

    template<typename T>
    T minValue(T first, T second)
    {
        return first < second ? first : second;
    }

    template<typename T>
    T readElement(u8 *&buffer, i32 *bufferSize = nullptr)
    {
        T returnType;
        returnType = *reinterpret_cast<T*>(buffer);
        buffer += sizeof(T);
        if (bufferSize)
        {
            *bufferSize -= sizeof(T);
        }
        return returnType;
    }    
}

#endif
