#if !defined(VECTORFUNCTIONS_H)
#define VECTORFUNCTIONS_H

#include "Intrinsics.h"

struct vfVec2i
{
    i32 x;
    i32 y;
};

struct vfRecti
{
    vfVec2i min;
    vfVec2i max;
};

inline vfRecti rectExpandi(vfRecti source, i32 x, i32 y)
{
    vfRecti result;
    result.min.x = source.min.x - x;
    result.min.y = source.min.y - y;
    result.max.x = source.max.x + x;
    result.max.y = source.max.y + y;
    return result;
}

inline vfRecti rectIntersectioni(vfRecti a, vfRecti b)
{
    vfRecti result;
    result.min.x = a.min.x > b.min.x ? a.min.x : b.min.x;
    result.min.y = a.min.y > b.min.y ? a.min.y : b.min.y;
    result.max.x = a.max.x < b.max.x ? a.max.x : b.max.x;
    result.max.y = a.max.y < b.max.y ? a.max.y : b.max.y;
    return result;
}

union vfVec3
{
    struct
    {
        r32 x;
        r32 y;
        r32 z;        
    };

    struct
    {
        r32 u;
        r32 v;
        r32 w;        
    };
    
    struct
    {
        r32 r;
        r32 g;
        r32 b;
    };
};

inline vfVec3 operator*(r32 scalar, vfVec3 a)
{
    vfVec3 result;

    result.x = a.x * scalar;
    result.y = a.y * scalar;
    result.z = a.z * scalar;
    return result;
}

union vfVec2
{
    struct
    {
        r32 x;
        r32 y;
    };

    struct
    {
        r32 u;
        r32 v;
    };
};

inline vfVec2 operator-(vfVec2 a, vfVec2 b)
{
    vfVec2 result;

    result.x = a.x - b.x;
    result.y = a.y - b.y;
    return result;
}

inline vfVec2 operator+(vfVec2 a, vfVec2 b)
{
    vfVec2 result;

    result.x = a.x + b.x;
    result.y = a.y + b.y;
    return result;
}

inline vfVec2 operator*(r32 scalar, vfVec2 a)
{
    vfVec2 result;

    result.x = a.x * scalar;
    result.y = a.y * scalar;
    return result;
}

inline vfVec2 operator*(vfVec2 a, r32 scalar)
{
    vfVec2 result;

    result.x = a.x * scalar;
    result.y = a.y * scalar;
    return result;
}

inline vfVec2 round(vfVec2 a)
{
    vfVec2 result;

    result.x = vclib::roundValue(a.x);
    result.y = vclib::roundValue(a.y);
    return result;
}

inline r32 magnitude(vfVec2 a)
{
    return vclib::squareRoot(a.x*a.x + a.y*a.y);
}

inline r32 distance(vfVec2 a, vfVec2 b)
{
    return magnitude(a - b);
}

inline vfVec2 unit(vfVec2 a)
{
    vfVec2 result;

    r32 vectorLength = magnitude(a);
    result.x = a.x / vectorLength;
    result.y = a.y / vectorLength;
    return result;
}

inline vfVec2 midpoint(vfVec2 a, vfVec2 b)
{
    vfVec2 result;

    result = (a + b) * 0.5f;
    return result;
}

inline r32 dotProduct(vfVec2 a, vfVec2 b)
{
    return (a.x*b.x + a.y*b.y);
}

inline r32 crossProduct(vfVec2 a, vfVec2 b)
{
    return (a.x*b.y - a.y*b.x);
}

inline vfVec2 vectorAtRange(r32 angle, r32 range)
{
    vfVec2 result;

    result.x = range * vclib::cosScalar(angle);
    result.y = range * vclib::sinScalar(angle);
    return result;
}

struct vfRect
{
    vfVec2 min;
    vfVec2 max;
};

inline vfVec2 getDimensions(vfRect r)
{
    vfVec2 result = r.max - r.min;
    return result;
};

inline vfVec2 getCentre(vfRect r)
{
    vfVec2 result = 0.5f * (r.min + r.max);
    return result;
};

inline vfRect makeRect(vfRecti r)
{
    vfRect result = {static_cast<r32>(r.min.x),
                     static_cast<r32>(r.min.y),
                     static_cast<r32>(r.max.x),
                     static_cast<r32>(r.max.y)};
    return result;
};

inline vfRect makeAABB(vfVec2 a, vfVec2 b)
{
    vfRect result;
    if (a.x <= b.x)
    {
        result.min.x = a.x;
        result.max.x = b.x;
    }
    else
    {
        result.min.x = b.x;
        result.max.x = a.x;
    }

    if (a.y <= b.y)
    {
        result.min.y = a.y;
        result.max.y = b.y;
    }
    else
    {
        result.min.y = b.y;
        result.max.y = a.y;
    }
    return result;
}

inline bool rectIntersects(vfRect a, vfRect b)
{
    bool result = true;
    if ((b.min.x >= a.max.x) ||
        (b.max.x <= a.min.x) ||
        (b.min.y >= a.max.y) ||
        (b.max.y <= a.min.y))
    {
        result = false;
    }

    return result;
};

struct vfMat2x2
{
    r32 elem[2][2];
};

inline vfMat2x2 rotation(r32 angle)
{
    r32 sinValue = vclib::sinScalar(angle);
    r32 cosValue = vclib::cosScalar(angle);

    vfMat2x2 result =
    {
        {{cosValue, -sinValue},
         {sinValue, cosValue}},
    };

    return result;
};

inline vfMat2x2 normal()
{
    // Note(KN): This is effectively a rotation by 90deg, however
    // given that cos(90) = 0 and sin(90) = 1, we simply build a
    // matrix containing constant values to represent it
    
    vfMat2x2 result =
    {
        {{0.0f, -1.0f},
         {1.0f, 0.0f}},
    };

    return result;
};

inline vfVec2 operator*(vfVec2 a, vfMat2x2 mat)
{
    vfVec2 result;

    result.x = a.x * mat.elem[0][0] + a.y * mat.elem[0][1];
    result.y = a.x * mat.elem[1][0] + a.y * mat.elem[1][1];
    return result;
}

#endif
