#if !defined(STACKARENA_H)
#define STACKARENA_H

#include "MemoryArena.h"

class StackAllocator : public ArenaAttachment<StackAllocator>
{
public:

    u8 * allocate(i32 allocationSize);
    void release(u8 *block);

private:

#pragma pack(push,1)
    struct StackHeader
    {
        i32 size;
    };
#pragma pack(pop)

};

typedef StackAllocator StackArena;

#endif
