
#include "WorldObjects.h"
#include "AssetCache.h"
#include "vc_String.h"
#include "vc_StringFunctions.h"

WorldObjects::WorldObjects(MemoryArena *persistStore)
{
    vertexList = persistStore->pushArray<vfVec2>(WorldRef::maxVertexCount);
    sectorList = persistStore->pushArray<SectorDef>(WorldRef::maxSectorCount);
}

void WorldObjects::updateEnvironment(r32 fwdAcceleration, r32 sideAcceleration, r32 lookDelta, r32 timeStep,
                                     r32 verticalVelocity)
{
    // 1. Create unit vector & rotate it to represent current orientation
    // 2. Calculate position update from acceleration and stored velocity over timestep
    // 3. Update current velocity based on acceleration
    // 4. Check against level geometry for interaction
    // 5. Apply position update
    // 6. Perform any vertical motion (jump / step up and down etc)

    player.orientation = vclib::clampAngle(player.orientation + (lookDelta * timeStep));
    
    vfVec2 unitVector = {1.0f, 0.0f};
    vfVec2 directionVector = unitVector * rotation(player.orientation);
    vfVec2 perpendicularVector = directionVector * normal();

    vfVec2 dragEffect = WorldRef::dragFactor * player.velocity;
    vfVec2 directedAccel =
        (fwdAcceleration * directionVector) +
        (sideAcceleration * perpendicularVector) -
        dragEffect;

    vfVec2 newPosition =
        (0.5f * directedAccel * (timeStep * timeStep)) +
        (player.velocity * timeStep) +
        player.position;

    // Ensure velocity returns to 0 when the speed gets below a threshold
    // TODO(KN): What should this threshold be and is there a way we can derive it
    player.velocity = (directedAccel * timeStep) + player.velocity;
    if (magnitude(player.velocity) < 0.005f)
    {
        player.velocity = {0.0f,0.0f};
    }
       
    player.position = checkPlayerWallInteraction(newPosition);

    r32 heightAboveFloor = player.height - sectorList[player.sector].floor.height;
    r32 frameVerticalAccel = 0.0f;    
    
    if (heightAboveFloor != 0.0f)
    {
        if (heightAboveFloor < 0.005f)
        {
            // Finish an in progress jump if we're close enough to the floor
            // NOTE(KN): This also covers the situation when going up steps when
            // the height above floor goes negative
            player.height = sectorList[player.sector].floor.height;
            player.upVelocity = 0.0f;
        }
        else
        {
            frameVerticalAccel = WorldRef::gravityAccel;
        }
    }
    else
    {
        // Set initial vertical velocity for a jump. Unlike other motion we
        // just treat this as an instantaneous event with no acceleration
        // build up
        player.upVelocity = verticalVelocity;
    }
   
    player.height =
        (0.5f * frameVerticalAccel * (timeStep * timeStep)) +
        (player.upVelocity * timeStep) +
        player.height;
    player.upVelocity = (frameVerticalAccel * timeStep) + player.upVelocity;    
}

vfVec2 WorldObjects::checkPlayerWallInteraction(vfVec2 newPosition)
{
    vfVec2 playerStart = player.position;
    vfVec2 playerEnd = newPosition;
    i32 originatingSector = player.sector;

    bool travelComplete = false;
    i32 attemptsToComplete = 4;
    while(travelComplete == false && attemptsToComplete--)
    {    
        vfRect motionBounds = makeAABB(playerStart, playerEnd);

        SectorDef *currentSector = &(sectorList[player.sector]);

        bool sectorIntersects = false;
        for (i32 lineIndex = 0; lineIndex < currentSector->wallCount; lineIndex++)
        {
            SectorDef::WallDef *sectorEdge = &(currentSector->lineSegment[lineIndex]);

            // NOTE(KN): Broad phase intersection test with AABBs before we do the
            // more expensive line intersection 

            // TODO(KN): Precalculate AABBs for walls when geometry is first loaded
        
            vfRect wallBounds = makeAABB(sectorEdge->start, sectorEdge->end);
            if (rectIntersects(motionBounds, wallBounds) == true)
            {
                // Parametric form line test for intersection
            
                vfVec2 playerDelta = playerEnd - playerStart;
                vfVec2 wallDirection = sectorEdge->end - sectorEdge->start;

                vfVec2 directionBetweenLines = sectorEdge->start - playerStart;
                r32 commonDenominator = crossProduct(playerDelta, wallDirection);

                if (commonDenominator != 0.0f)
                {
                    r32 t1Intersect = crossProduct(directionBetweenLines, wallDirection) / commonDenominator;
                    r32 t2Intersect = crossProduct(directionBetweenLines, playerDelta) / commonDenominator;            

                    if (t1Intersect >= 0.0f && t1Intersect <= 1.0f &&
                        t2Intersect >= 0.0f && t2Intersect <= 1.0f)
                    {
                        bool impassableWall = false;
                        
                        if (sectorEdge->blocking == true ||
                            (sectorList[sectorEdge->travelToSector].floor.height - player.height > WorldRef::stepLimit) ||
                            (player.height + WorldRef::playerHeight > sectorList[sectorEdge->travelToSector].ceiling.height))
                        {
                            impassableWall = true;
                        }
                        
                        if (impassableWall == false)
                        {
                            // If we transition through a sector 'wall' just update the current
                            // sector information but retain the original player delta to check
                            // collisions in the next sector
                            if (sectorEdge->travelToSector != originatingSector)
                            {
                                originatingSector = player.sector;
                                player.sector = sectorEdge->travelToSector;
                                sectorIntersects = true;
                            }
                        }
                        else
                        {
                            vfVec2 unitNormalEdge = unit(wallDirection * normal());

                            // Move current position to calculated intersection and back off by a
                            // small amount of the wall normal vector to ensure we're not sat *on*
                            // the wall itself at the end of this move (which will break collision detection)
                            playerStart = playerStart + (t1Intersect * playerDelta) - (0.001f * unitNormalEdge);

                            // Remove component of player motion and player velocity going into the
                            // wall that we've just intersected with
                            playerEnd = playerStart + (playerDelta - unitNormalEdge * dotProduct(playerDelta, unitNormalEdge));
                            player.velocity = player.velocity - unitNormalEdge * dotProduct(player.velocity, unitNormalEdge);
                            sectorIntersects = true;
                        }

                        if (sectorIntersects == true)
                            break;
                    }
                }
            }
        }

        travelComplete = !sectorIntersects;
    }

    return playerEnd;
}

