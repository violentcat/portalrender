#if !defined(MEMORYARENA_H)
#define MEMORYARENA_H

template<typename allocator>
class ArenaAttachment
{
public:
    
    void attachStorage(void *memoryBlock, i32 blockLength);
    void resetArena();
    u8 * pushBlock(i32 allocationSize);
    void popBlock(u8 *block);

    template<typename T>
    T* pushStruct() { return reinterpret_cast<T*>(pushBlock(sizeof(T))); }

    template<typename T>
    T* pushArray(i32 count) { return reinterpret_cast<T*>(pushBlock(sizeof(T)*count)); }

protected:

    u8 *baseAddress = nullptr;
    i32 arenaSize = 0;
    i32 usedSize = 0;    
};

class DefaultAllocator : public ArenaAttachment<DefaultAllocator>
{
public:

    u8 * allocate(i32 allocationSize);
};

typedef DefaultAllocator MemoryArena;

#endif
