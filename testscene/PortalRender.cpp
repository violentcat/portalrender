
#include <windows.h>
#include <xinput.h>
#include "PlatformTypes.h"
#include "win32_DisplayBuffer.cpp"
#include "SceneWalk.cpp"

DYNAMIC_CALLDEF (DWORD, ERROR_DEVICE_NOT_CONNECTED, XInputGetState, DWORD, XINPUT_STATE *)
DYNAMIC_CALLDEF (DWORD, ERROR_DEVICE_NOT_CONNECTED, XInputSetState, DWORD, XINPUT_VIBRATION *)

win32DisplayBuffer renderArea;

LRESULT CALLBACK WindowMessageHandler(HWND windowHandle, UINT message, WPARAM wParam, LPARAM lParam)
{    
    LRESULT handlerResult = 0;
    
    switch(message)
    {
        case WM_ACTIVATEAPP:
        {
        } break;
        
        case WM_DESTROY:            
        {
        } break;
        
        case WM_CLOSE:
        {
            PostQuitMessage(0);
        } break;
        
        case WM_SIZE:
        {
        } break;
        
        case WM_PAINT:
        {
            win32DisplayBuffer::Dimensions windowDim = renderArea.GetDeviceDimensions(windowHandle);
            
            PAINTSTRUCT paintInfo;
            HDC deviceContext = BeginPaint(windowHandle, &paintInfo);
            renderArea.updateDeviceFromBuffer(deviceContext, windowDim.width, windowDim.height);
            EndPaint(windowHandle, &paintInfo);
            
        } break;
        
        default:
        {
            handlerResult =  DefWindowProcA(windowHandle, message, wParam, lParam);
        } break;
    }
    
    return handlerResult;
}

void ProcessDigitalControllerState(SceneWalk::ControllerState *inputState, bool *runningState)
{
    for (i32 loop = 0; loop < countOf(inputState->keyList); loop++)
    {
        inputState->keyList[loop].keyTransitions = 0;
    }

    inputState->digitalInput = true;
    inputState->controllerValid = true;
    
    MSG Message;
    while(PeekMessage(&Message, 0, 0, 0, PM_REMOVE))
    {
        switch(Message.message)
        {
            case WM_QUIT:
            {
                *runningState = false;
            } break;
            
            case WM_KEYDOWN:
            case WM_KEYUP:
            {
                WPARAM keyCode = Message.wParam;
                bool isKeyDown = (Message.lParam & (1 << 31)) == 0 ? true : false;
                bool wasKeyDown = (Message.lParam & (1 << 30)) == 0 ? false : true;
                
                if (isKeyDown != wasKeyDown)
                {
                    switch(keyCode)
                    {
                        case 0x26:
                            inputState->moveFwd.isKeyDown = isKeyDown;
                            inputState->moveFwd.keyTransitions++;
                            break; // Up
                        
                        case 0x28:
                            inputState->moveBack.isKeyDown = isKeyDown;
                            inputState->moveBack.keyTransitions++;
                            break; // Down
                        
                        case 0x25:
                            inputState->turnLeft.isKeyDown = isKeyDown;
                            inputState->turnLeft.keyTransitions++;
                            break; // Left
                        
                        case 0x27:
                            inputState->turnRight.isKeyDown = isKeyDown;
                            inputState->turnRight.keyTransitions++;
                            break; // Right

                        case 0x5A:
                            inputState->moveLeft.isKeyDown = isKeyDown;
                            inputState->moveLeft.keyTransitions++;
                            break; // z

                        case 0x58:
                            inputState->moveRight.isKeyDown = isKeyDown;
                            inputState->moveRight.keyTransitions++;
                            break; // x
                    };
                }
            }
            break;
            
            default:
            {
                TranslateMessage(&Message);
                DispatchMessageA(&Message);
            } break;
        }
    }
}

r32 ProcessAnalogStick(i32 stickValue, i32 deadZone)
{
    r32 normalisedResult = 0.0f;
        
    if (stickValue > deadZone)
    {
        normalisedResult = static_cast<r32>(stickValue - deadZone) / static_cast<r32>(32767 - deadZone);
    }
    else if (stickValue < -deadZone)
    {
        normalisedResult = static_cast<r32>(stickValue + deadZone) / static_cast<r32>(32768 - deadZone);
    }

    return normalisedResult;
}

void ProcessAnalogControllerState(SceneWalk::ControllerState *inputState, i32 selectedController)
{
    inputState->digitalInput = false;

    XINPUT_STATE stateRead;
    if (XInputGetState_Dynamic(selectedController, &stateRead) == ERROR_SUCCESS)
    {
        inputState->controllerValid = true;

        XINPUT_GAMEPAD *padData = &(stateRead.Gamepad);
        inputState->leftStickX = ProcessAnalogStick(padData->sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
        inputState->leftStickY = ProcessAnalogStick(padData->sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
        inputState->rightStickX = ProcessAnalogStick(padData->sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
        inputState->rightStickY = ProcessAnalogStick(padData->sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
    }
    else
    {
        inputState->controllerValid = false;
    }
}

void InitControllerDevices(i32 *selectedController)
{
    HMODULE loadHandle = LoadLibraryA("XINPUT1_4.DLL");
    if (!loadHandle)
    {
        loadHandle = LoadLibraryA("XINPUT1_3.DLL");
    }
    if (!loadHandle)
    {
        loadHandle = LoadLibraryA("XINPUT9_1_0.DLL");        
    }

    if (loadHandle)
    {
        DYNAMIC_CALLLOAD(loadHandle, XInputGetState)
        DYNAMIC_CALLLOAD(loadHandle, XInputSetState)
    }

    // Determine whether we have an active controller
    for (i32 loop = 0; loop < XUSER_MAX_COUNT; loop++)
    {
        XINPUT_STATE stateRead;
        if (XInputGetState_Dynamic(loop, &stateRead) == ERROR_SUCCESS)
        {
            *selectedController = loop;
            break;
        }
    }
}

void ProgramLoop(HWND windowHandle)
{
    bool programLoopRunning = true;
    
    if (timeBeginPeriod(1) != TIMERR_NOERROR)
    {
        // If can't raise the scheduler interrupt rate then no choice but to implement
        // simulation period delay as a busy loop
        // TODO(KN): Implement busy loop
        return;
    }
    
    // Initialise high resolution timing for frame interval
    LARGE_INTEGER performanceFreqResult;
    QueryPerformanceFrequency(&performanceFreqResult);
    r64 timerFrequency = static_cast<r64>(performanceFreqResult.QuadPart);

    // Initialise XInput for gamepads
    i32 selectedController = -1;
    InitControllerDevices(&selectedController);
    
    // Initialise controller states for keyboard and gamepad
    SceneWalk::ControllerState keyState = {};
    SceneWalk::ControllerState gamepadState = {};

    SceneWalk world;
    world.initRenderBuffer(renderArea.displayBuffer.pixelData,
                           renderArea.displayBuffer.width,
                           renderArea.displayBuffer.height,
                           renderArea.displayBuffer.pitch);

    r64 frameTimePeriod = 1000000.0 / 60.0;    
    LARGE_INTEGER cycleInitTime;
    LARGE_INTEGER frameCompleteTime;
    QueryPerformanceCounter(&cycleInitTime);
    
    while (programLoopRunning)
    {
        ProcessDigitalControllerState(&keyState, &programLoopRunning);
        ProcessAnalogControllerState(&gamepadState, selectedController);
        
        HDC deviceContext = GetDC(windowHandle);
        win32DisplayBuffer::Dimensions windowDim = renderArea.GetDeviceDimensions(windowHandle);

        world.updateSimulationAndRender(&keyState, &gamepadState);
        
        renderArea.updateDeviceFromBuffer(deviceContext, windowDim.width, windowDim.height);        
        ReleaseDC(windowHandle, deviceContext);
        
        // Measure elapsed time in microseconds for total frame
        QueryPerformanceCounter(&frameCompleteTime);
        r64 frameTimeElapsed = static_cast<r64>(frameCompleteTime.QuadPart - cycleInitTime.QuadPart) * 1000000.0 / timerFrequency;
        
        // .. then wait for the rest of the time to pass until the end of this cycle
        if (frameTimeElapsed < frameTimePeriod)
        {
            DWORD waitTime = static_cast<DWORD>((frameTimePeriod - frameTimeElapsed) / 1000.0);
            if (waitTime)
            {
                Sleep(waitTime);
            }
            
            // TODO(KN): Do we need to spin here to consume any partial milliseconds
            // left over after sleep?
        }
        
        // Begin next interval
        QueryPerformanceCounter(&cycleInitTime);        
    }
}

int CALLBACK WinMain(HINSTANCE instance, HINSTANCE prevInstance, LPSTR commandLine, int showWindow)
{
    WNDCLASSEXA windowClass = {};
    
    windowClass.cbSize = sizeof(WNDCLASSEX);
    windowClass.style = CS_HREDRAW | CS_VREDRAW;
    windowClass.lpfnWndProc = WindowMessageHandler;
    windowClass.hInstance = instance;
    windowClass.lpszClassName = "PortalRender";
    windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
    
    renderArea.createDisplayBuffer(960, 540);
    
    if (RegisterClassExA(&windowClass))
    {
        HWND windowHandle = CreateWindowExA(0,
                                            windowClass.lpszClassName,
                                            "PortalRenderer",
                                            WS_OVERLAPPEDWINDOW | WS_VISIBLE,
                                            CW_USEDEFAULT,
                                            CW_USEDEFAULT,
                                            CW_USEDEFAULT,
                                            CW_USEDEFAULT,
                                            0,
                                            0,
                                            instance,
                                            0);
        
        if (windowHandle)
        {
            ProgramLoop(windowHandle);
        }
        else
        {
            // TODO(KN): Handle failure condition
        }
    }
    else
    {
        // TODO(KN): Handle failure condition
    }
    
    return 0;
}
