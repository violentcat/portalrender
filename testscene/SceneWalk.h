
#if !defined(SCENEWALK_H)
#define SCENEWALK_H

#include "PlatformTypes.h"
#include "VectorFunctions.h"

class SceneWalk
{    
public:
    
    struct KeyState
    {
        bool isKeyDown;
        u32 keyTransitions;
    };
    
    struct ControllerState
    {
        bool controllerValid;
        bool digitalInput;
        r32 leftStickX;
        r32 leftStickY;
        r32 rightStickX;
        r32 rightStickY;
        
        union
        {
            KeyState keyList[6];
            struct
            {            
                KeyState moveFwd;
                KeyState moveBack;
                KeyState turnLeft;
                KeyState turnRight;
                KeyState moveLeft;
                KeyState moveRight;
            };
        };
    };

    struct LineSegments
    {
        vfVec2 p0;
        vfVec2 p1;
        vfVec3 colour;
    };

    
public:

    void initRenderBuffer(void *pixelData, i32 width, i32 height, i32 pitch);
    void clearRenderBuffer(vfVec3 colour);
    void updateSimulationAndRender(ControllerState *digitalState, ControllerState *analogState);

    void drawStaticWorldFrame(i32 frameWidth, i32 frameHeight);
    void drawCameraWorldFrame(i32 frameWidth, i32 frameHeight);
    void drawPerspectiveWorldFrame(i32 frameWidth, i32 frameHeight);

    void setScreenSpaceExtent(vfRecti rect);
    vfRect getCameraBoundsForScreen();
    vfVec2 translateWorldPosToViewportOrigin(vfVec2 worldPos);
    vfVec2 translateWorldPosToCamera(vfVec2 worldPos);
    vfVec2 translateCameraPosToViewport(vfVec2 cameraPos);
    vfVec2 translatePerspectiveToViewport(vfVec2 perspectivePos);
    vfVec2 translatePerspectiveWallHeight(vfVec2 perspectivePos);

    
    void drawBoundingRect(vfRect rect, vfVec3 vfcolour);
    void drawFilledRect(vfVec2 min, vfVec2 max, vfVec3 colour);

    bool edgeTest(float delta, float edgeDiff, float *t0, float *t1);
    void drawLineRectIntersection(vfRect rect, vfVec2 start, vfVec2 end, vfVec3 colour);
    void drawSimpleLine(vfVec2 start, vfVec2 end, vfVec3 colour);

    
private:

    // Graphics
    u32 *rgbaTarget = nullptr;
    static const i32 bytesPerPixel = 4;
    i32 displayWidth;
    i32 displayHeight;
    i32 displayPitch;

    //
    vfRecti viewportBounds;
    
    // 
    vfRect worldBounds;
    vfVec2 worldDimensions;

    //
    vfVec2 position = {};
    r32 viewDirection = 0.0f;
};

#endif

