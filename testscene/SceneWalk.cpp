
#include "SceneWalk.h"

const SceneWalk::LineSegments lineTable[] =
{
    {{-90,-20},  {40,220},   {0.3f,0.9f,0.3f}},
    {{40,220},   {100,50},   {1.0f,0.4f,0.3f}},
    {{100,50},   {60,-70},   {1.0f,0.4f,1.0f}},
    {{60,-70},   {-50,-150}, {0.8f,0.7f,0.3f}},
    {{-50,-150}, {-90,-20},  {0.2f,0.8f,0.8f}},
};

void SceneWalk::initRenderBuffer(void *pixelData, i32 width, i32 height, i32 pitch)
{
    rgbaTarget = reinterpret_cast<u32 *>(pixelData);
    displayWidth = width;
    displayHeight = height;
    displayPitch = pitch;
}

void SceneWalk::clearRenderBuffer(vfVec3 colour)
{
    u32 colValue = intrn::roundNormalColourToARGB(colour.r, colour.g, colour.b);
    u32 *pixelClear = rgbaTarget;        

    // TODO(KN): Replace this with something based on intrinsics
    // to get some decent write performance (non-temporal store?)
    i32 totalElements = displayHeight * (displayPitch / bytesPerPixel);
    for (i32 loop = 0; loop < totalElements; loop++)
    {
        *pixelClear++ = colValue;
    }
}

void SceneWalk::updateSimulationAndRender(ControllerState *digitalState, ControllerState *analogState)
{
    if (digitalState->moveFwd.isKeyDown)
    {
        position = position + vectorAtRange(viewDirection, 2.0f);
    }
    if (digitalState->moveBack.isKeyDown)
    {
        position = position - vectorAtRange(viewDirection, 2.0f);
    }
    if (digitalState->turnLeft.isKeyDown)
    {
        viewDirection += intrn::to_radians;
        if (viewDirection > intrn::piflt)
        {
            viewDirection -= intrn::two_piflt;
        }
    }
    if (digitalState->turnRight.isKeyDown)
    {
        viewDirection -= intrn::to_radians;
        if (viewDirection < -intrn::piflt)
        {
            viewDirection += intrn::two_piflt;
        }
    }
    if (analogState->leftStickX > 0.0f)
    {
        viewDirection -= intrn::to_radians;
        if (viewDirection < -intrn::piflt)
        {
            viewDirection += intrn::two_piflt;
        }        
    }
    else if (analogState->leftStickX < 0.0f)
    {
        viewDirection += intrn::to_radians;
        if (viewDirection > intrn::piflt)
        {
            viewDirection -= intrn::two_piflt;
        }
    }
    if (analogState->leftStickY > 0.0f)
    {
        position = position + vectorAtRange(viewDirection, 2.0f);
    }
    else if (analogState->leftStickY < 0.0f)
    {
        position = position - vectorAtRange(viewDirection, 2.0f);
    }

    clearRenderBuffer({0.0f,0.0f,0.0f});

    i32 frameWidth = (displayWidth - 40) / 3;
    i32 frameHeight = displayHeight - 20;

    drawStaticWorldFrame(frameWidth, frameHeight);
    drawCameraWorldFrame(frameWidth, frameHeight);
    drawPerspectiveWorldFrame(frameWidth, frameHeight);
}

void SceneWalk::drawStaticWorldFrame(i32 frameWidth, i32 frameHeight)
{
    vfVec2 drawPosRectSize = {3.0f,3.0f};

    vfRecti frameView = {10, 10, 10+frameWidth, 10+frameHeight};
    setScreenSpaceExtent(frameView);

    drawBoundingRect(worldBounds, {1.0f,0.0f,1.0f});

    vfVec2 viewOrigin = translateWorldPosToViewportOrigin(position);

    vfVec2 drawPosMin = viewOrigin - drawPosRectSize;
    vfVec2 drawPosMax = viewOrigin + drawPosRectSize;    
    drawFilledRect(drawPosMin, drawPosMax, {1.0f,1.0f,1.0f});

    vfVec2 endPoint = position + vectorAtRange(viewDirection, 20.0f);
    vfVec2 viewEndPoint = translateWorldPosToViewportOrigin(endPoint);
    drawLineRectIntersection(worldBounds, viewOrigin, viewEndPoint, {1.0f,1.0f,1.0f});    

    for (i32 loop = 0; loop < 5; loop++)
    {
        vfVec2 wallPointView1 = translateWorldPosToViewportOrigin(lineTable[loop].p0);
        vfVec2 wallPointView2 = translateWorldPosToViewportOrigin(lineTable[loop].p1);
        drawSimpleLine(wallPointView1, wallPointView2, lineTable[loop].colour);
    }    
}

void SceneWalk::drawCameraWorldFrame(i32 frameWidth, i32 frameHeight)
{
    vfRecti frameView2 = {20+frameWidth, 10, 20+frameWidth+frameWidth, 10+frameHeight};
    setScreenSpaceExtent(frameView2);

    drawBoundingRect(worldBounds, {0.0f,1.0f,1.0f});
    vfVec2 drawPosRectSize = {3.0f,3.0f};
    
    vfVec2 viewOrigin = translateCameraPosToViewport(translateWorldPosToCamera(position));
    vfVec2 drawPosMin = viewOrigin - drawPosRectSize;
    vfVec2 drawPosMax = viewOrigin + drawPosRectSize;    
    drawFilledRect(drawPosMin, drawPosMax, {1.0f,1.0f,1.0f});

    vfVec2 endPoint = position + vectorAtRange(viewDirection, 20.0f);
    vfVec2 directionVector = translateWorldPosToCamera(endPoint);
    vfVec2 viewEndPoint = translateCameraPosToViewport(directionVector);
    drawSimpleLine(viewOrigin, viewEndPoint, {1.0f,1.0f,1.0f});    

    for (i32 loop = 0; loop < 5; loop++)
    {
        vfVec2 wallPoint1Vec = translateWorldPosToCamera(lineTable[loop].p0);
        vfVec2 wallPoint2Vec = translateWorldPosToCamera(lineTable[loop].p1);
        vfVec2 wallPointView1 = translateCameraPosToViewport(wallPoint1Vec);
        vfVec2 wallPointView2 = translateCameraPosToViewport(wallPoint2Vec);
        drawLineRectIntersection(worldBounds, wallPointView1, wallPointView2, lineTable[loop].colour);

        vfVec2 lineVec = wallPoint2Vec - wallPoint1Vec;

        // Clip to 90 deg FoV
        vfVec2 fovLeft = {-1.0f,1.0f};
        vfVec2 fovRight = {1.0f,1.0f};
        vfVec2 intersect = {0.0f};
        vfVec2 intersectLeft = {0.0f};
        vfVec2 intersectRight = {0.0f};
        i32 intersectCount = 0;
    
        r32 lineCross = crossProduct(wallPoint1Vec, lineVec);    
        r32 leftDenom = crossProduct(fovLeft, lineVec);
        r32 leftNumer = crossProduct(wallPoint1Vec, fovLeft);
        r32 rightDenom = crossProduct(fovRight, lineVec);
        r32 rightNumer = crossProduct(wallPoint1Vec, fovRight);

        r32 t1, t2 = 0.0f;

        if (leftDenom)
        {
            t1 = lineCross / leftDenom;
            t2 = leftNumer / leftDenom;
            if (t1 >= 0.0f && t2 >= 0.0f && t2 <= 1.0f)
            {
                intersect = intersectLeft = t1 * fovLeft;
                intersectCount++;
            }
        }

        if (rightDenom)
        {       
            t1 = lineCross / rightDenom;
            t2 = rightNumer / rightDenom;
            if (t1 >= 0.0f && t2 >= 0.0f && t2 <= 1.0f)
            {
                intersect = intersectRight = t1 * fovRight;
                intersectCount++;
            }
        }

        if (intersectCount == 0)
        {
            if (leftNumer > 0.0f && rightNumer < 0.0f)
            {
                drawLineRectIntersection(worldBounds, wallPointView1, wallPointView2, {1.0f,1.0f,1.0f});                
            }
        }
        else if (intersectCount == 1)
        {
            if (leftNumer > 0.0f && rightNumer < 0.0f)
            {
                wallPointView2 = translateCameraPosToViewport(intersect);
                drawLineRectIntersection(worldBounds, wallPointView1, wallPointView2, {1.0f,1.0f,1.0f});    
            } 
            else
            {
                wallPointView1 = translateCameraPosToViewport(intersect);
                drawLineRectIntersection(worldBounds, wallPointView1, wallPointView2, {1.0f,1.0f,1.0f});    
            }
        }
        else
        {
            wallPointView1 = translateCameraPosToViewport(intersectLeft);
            wallPointView2 = translateCameraPosToViewport(intersectRight);
            drawLineRectIntersection(worldBounds, wallPointView1, wallPointView2, {1.0f,1.0f,1.0f});    
        }    
    }    
}

void SceneWalk::drawPerspectiveWorldFrame(i32 frameWidth, i32 frameHeight)
{
    vfRecti frameView3 = {30+frameWidth+frameWidth-1, 10-1, 30+frameWidth+frameWidth+frameWidth+1, 10+frameHeight+1};
    setScreenSpaceExtent(frameView3);

    drawBoundingRect(worldBounds, {1.0f,1.0f,0.0f});

    frameView3 = {30+frameWidth+frameWidth, 10, 30+frameWidth+frameWidth+frameWidth, 10+frameHeight};
    setScreenSpaceExtent(frameView3);
    
    for (i32 loop = 0; loop < 5; loop++)
    {    
        vfVec2 wallPoint1Vec = translateWorldPosToCamera(lineTable[loop].p0);
        vfVec2 wallPoint2Vec = translateWorldPosToCamera(lineTable[loop].p1);

        vfVec2 lineVec = wallPoint2Vec - wallPoint1Vec;
        
        // Clip to 90 deg FoV
        vfVec2 fovLeft = {-1.0f,1.0f};
        vfVec2 fovRight = {1.0f,1.0f};
        vfVec2 intersect = {0.0f};
        vfVec2 intersectLeft = {0.0f};
        vfVec2 intersectRight = {0.0f};
        i32 intersectCount = 0;
    
        r32 lineCross = crossProduct(wallPoint1Vec, lineVec);    
        r32 leftDenom = crossProduct(fovLeft, lineVec);
        r32 leftNumer = crossProduct(wallPoint1Vec, fovLeft);
        r32 rightDenom = crossProduct(fovRight, lineVec);
        r32 rightNumer = crossProduct(wallPoint1Vec, fovRight);

        r32 t1, t2 = 0.0f;

        if (leftDenom)
        {
            t1 = lineCross / leftDenom;
            t2 = leftNumer / leftDenom;
            if (t1 >= 0.0f && t2 >= 0.0f && t2 <= 1.0f)
            {
                intersect = intersectLeft = t1 * fovLeft;
                intersectCount++;
            }
        }

        if (rightDenom)
        {       
            t1 = lineCross / rightDenom;
            t2 = rightNumer / rightDenom;
            if (t1 >= 0.0f && t2 >= 0.0f && t2 <= 1.0f)
            {
                intersect = intersectRight = t1 * fovRight;
                intersectCount++;
            }
        }
    
        if (intersectCount == 0)
        {
            if (leftNumer <= 0.0f || rightNumer >= 0.0f)
            {
                continue;
            }
        }
        else if (intersectCount == 1)
        {
            if (leftNumer > 0.0f && rightNumer < 0.0f)
            {
                wallPoint2Vec = intersect;            
            } 
            else
            {
                wallPoint1Vec = intersect;
            }
        }
        else
        {
            wallPoint1Vec = intersectLeft;
            wallPoint2Vec = intersectRight;
        }        

        vfVec2 perspWallPoint1 = {wallPoint1Vec.x / wallPoint1Vec.y, wallPoint1Vec.y};
        vfVec2 perspWallPoint2 = {wallPoint2Vec.x / wallPoint2Vec.y, wallPoint2Vec.y};

        vfVec2 cameraPointView1 = translatePerspectiveWallHeight(perspWallPoint1);
        vfVec2 cameraPointView2 = translatePerspectiveWallHeight(perspWallPoint2);

        i32 startX;
        i32 endX;
        r32 wallStepping;
        r32 wallHeight;
    
        if (cameraPointView2.x > cameraPointView1.x)
        {
            startX = intrn::roundToi32(cameraPointView1.x);
            endX = intrn::roundToi32(cameraPointView2.x);
            wallStepping = (cameraPointView2.y - cameraPointView1.y) / (cameraPointView2.x - cameraPointView1.x);
            wallHeight = cameraPointView1.y;
        }
        else
        {
            startX = intrn::roundToi32(cameraPointView2.x);
            endX = intrn::roundToi32(cameraPointView1.x);
            wallStepping = (cameraPointView1.y - cameraPointView2.y) / (cameraPointView1.x - cameraPointView2.x);
            wallHeight = cameraPointView2.y;
        }
    
        for (i32 colloop = startX; colloop <= endX; colloop++)
        {
            if (wallHeight < worldDimensions.y)
            {
                vfVec2 point1 = {(float)colloop, worldBounds.min.y};
                vfVec2 point2 = {(float)colloop, ((worldDimensions.y - wallHeight) / 2.0f) + worldBounds.min.y};
        
                drawSimpleLine(point1, point2, {0.0f,0.0f,0.7f});

                point1.y = point2.y;
                point2.y = point2.y + wallHeight;

                drawSimpleLine(point1, point2, lineTable[loop].colour);

                point1.y = point2.y;
                point2.y = worldBounds.max.y;

                drawSimpleLine(point1, point2, {0.3f,0.3f,0.3f});
            }
            else
            {
                vfVec2 point1 = {(float)colloop, worldBounds.min.y};
                vfVec2 point2 = {(float)colloop, worldBounds.max.y};
       
                drawSimpleLine(point1, point2, lineTable[loop].colour);
            }

            wallHeight += wallStepping;
        }    
    
        vfVec2 perspWallPoint1a = {wallPoint1Vec.x / wallPoint1Vec.y, wallPoint1Vec.y};
        vfVec2 perspWallPoint1b = {wallPoint1Vec.x / wallPoint1Vec.y, -wallPoint1Vec.y};
    
        vfVec2 perspWallPoint2a = {wallPoint2Vec.x / wallPoint2Vec.y, wallPoint2Vec.y};
        vfVec2 perspWallPoint2b = {wallPoint2Vec.x / wallPoint2Vec.y, -wallPoint2Vec.y};

        cameraPointView1 = translatePerspectiveToViewport(perspWallPoint1a);
        cameraPointView2 = translatePerspectiveToViewport(perspWallPoint1b);
        vfVec2 cameraPointView3 = translatePerspectiveToViewport(perspWallPoint2a);
        vfVec2 cameraPointView4 = translatePerspectiveToViewport(perspWallPoint2b);

        drawLineRectIntersection(worldBounds, cameraPointView1, cameraPointView3, {1.0f,1.0f,1.0f});
        drawLineRectIntersection(worldBounds, cameraPointView3, cameraPointView4, {1.0f,1.0f,1.0f});
        drawLineRectIntersection(worldBounds, cameraPointView4, cameraPointView2, {1.0f,1.0f,1.0f});
        drawLineRectIntersection(worldBounds, cameraPointView2, cameraPointView1, {1.0f,1.0f,1.0f});
    
//    drawSimpleLine(wallPointView1, wallPointView3, {1.0f,1.0f,1.0f});
//    drawSimpleLine(wallPointView3, wallPointView4, {1.0f,1.0f,1.0f});
//    drawSimpleLine(wallPointView4, wallPointView2, {1.0f,1.0f,1.0f});
//    drawSimpleLine(wallPointView2, wallPointView1, {1.0f,1.0f,1.0f});
    }
}

void SceneWalk::setScreenSpaceExtent(vfRecti rect)
{
    viewportBounds = rect;
    worldBounds = getCameraBoundsForScreen();
    worldDimensions = getDimensions(worldBounds);
}

vfRect SceneWalk::getCameraBoundsForScreen()
{
    vfRect result;
    result.min.x = static_cast<r32>(viewportBounds.min.x);
    result.min.y = static_cast<r32>(viewportBounds.min.y);
    result.max.x = static_cast<r32>(viewportBounds.max.x);
    result.max.y = static_cast<r32>(viewportBounds.max.y);
    return result;
}

vfVec2 SceneWalk::translateWorldPosToViewportOrigin(vfVec2 worldPos)
{
    vfVec2 resultPos;
    resultPos.x = worldPos.x + worldBounds.min.x + (worldDimensions.x / 2);
    resultPos.y = worldPos.y + worldBounds.min.y + (worldDimensions.y / 2);
    return resultPos;
}

vfVec2 SceneWalk::translateWorldPosToCamera(vfVec2 worldPos)
{
    vfVec2 translatePos;
    translatePos.x = worldPos.x - position.x;
    translatePos.y = worldPos.y - position.y;

    vfVec2 rotatePos;    
    rotatePos.x = intrn::sinScalar(viewDirection) * translatePos.x - intrn::cosScalar(viewDirection) * translatePos.y;
    rotatePos.y = intrn::cosScalar(viewDirection) * translatePos.x + intrn::sinScalar(viewDirection) * translatePos.y;

    return rotatePos;
}

vfVec2 SceneWalk::translateCameraPosToViewport(vfVec2 cameraPos)
{
    vfVec2 screenPos;
    screenPos.x = cameraPos.x + worldBounds.min.x + (worldDimensions.x / 2);
    screenPos.y = cameraPos.y + worldBounds.min.y + (worldDimensions.y / 2);

    return screenPos;
}

vfVec2 SceneWalk::translatePerspectiveToViewport(vfVec2 perspectivePos)
{
    vfVec2 screenPos;
    r32 halfWorldWidth = worldDimensions.x / 2;
    r32 halfWorldHeight = worldDimensions.y / 2;
    
    screenPos.x = worldBounds.min.x + halfWorldWidth + (perspectivePos.x * halfWorldWidth);
    screenPos.y = (worldDimensions.y*2.0f / perspectivePos.y) + worldBounds.min.y + halfWorldHeight;

    return screenPos;
}

vfVec2 SceneWalk::translatePerspectiveWallHeight(vfVec2 perspectivePos)
{
    vfVec2 screenPos;
    r32 halfWorldWidth = worldDimensions.x / 2;
    r32 halfWorldHeight = worldDimensions.y / 2;
    
    screenPos.x = worldBounds.min.x + halfWorldWidth + (perspectivePos.x * halfWorldWidth);
    screenPos.y = (worldDimensions.y*2.0f / perspectivePos.y) * 2.0f;

    return screenPos;
}

void SceneWalk::drawBoundingRect(vfRect rect, vfVec3 colour)
{
    vfVec2 topRight = {rect.max.x, rect.min.y};
    vfVec2 bottomLeft = {rect.min.x, rect.max.y};

    drawFilledRect(rect.min, topRight, colour);
    drawFilledRect(rect.min, bottomLeft, colour);
    drawFilledRect(bottomLeft, rect.max, colour);
    drawFilledRect(topRight, rect.max, colour);
}

void SceneWalk::drawFilledRect(vfVec2 topLeft, vfVec2 bottomRight, vfVec3 colour)
{
    vfRecti boundedRect;
    boundedRect.min.x = intrn::roundToi32(topLeft.x);
    boundedRect.min.y = intrn::roundToi32(topLeft.y);
    boundedRect.max.x = intrn::roundToi32(bottomRight.x);
    boundedRect.max.y = intrn::roundToi32(bottomRight.y);
    boundedRect = rectIntersectioni(boundedRect, viewportBounds);

    u32 colValue = intrn::roundNormalColourToARGB(colour.r, colour.g, colour.b);

    // Be mindful of pointer arithmetic here
    u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget + boundedRect.min.x) + (boundedRect.min.y * displayPitch);
    
    for (i32 yIndex = boundedRect.min.y; yIndex <= boundedRect.max.y; yIndex++)
    {
        u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt);        
        for (i32 xIndex = boundedRect.min.x; xIndex <= boundedRect.max.x; xIndex++)
        {
            *pixelOffset++ = colValue;
        }

        pixelPt += displayPitch;
    }
}

bool SceneWalk::edgeTest(r32 delta, r32 edgeDiff, r32 *t0, r32 *t1)
{
    if (delta == 0.0f)
    {
        return (edgeDiff > 0.0f);
    }

    r32 ratio = edgeDiff / delta;
    bool clipInside = false;
    
    if (delta < 0.0f)
    {
        if (ratio <= *t1)
        {
            clipInside = true;
            if (ratio > *t0)
            {
                *t0 = ratio;
            }
        }                            
    }
    else
    {
        if (ratio >= *t0)
        {
            clipInside = true;
            if (ratio < *t1)
            {
                *t1 = ratio;
            }
        }
    }

    return clipInside;
}

void SceneWalk::drawLineRectIntersection(vfRect rect, vfVec2 start, vfVec2 end, vfVec3 colour)
{
    r32 t0 = 0.0f;
    r32 t1 = 1.0f;
    vfVec2 delta = end - start;
    
    if (edgeTest(-delta.x, start.x - rect.min.x, &t0, &t1) && // left
        edgeTest(delta.x,  rect.max.x - start.x, &t0, &t1) && // right
        edgeTest(-delta.y, start.y - rect.min.y, &t0, &t1) && // bottom
        edgeTest(delta.y,  rect.max.y - start.y, &t0, &t1))   // top
    {
        vfVec2 clipStart = {start.x + t0 * delta.x, start.y + t0 * delta.y};
        vfVec2 clipEnd   = {start.x + t1 * delta.x, start.y + t1 * delta.y};
        
        drawSimpleLine(clipStart, clipEnd, colour);
    }
}

void SceneWalk::drawSimpleLine(vfVec2 start, vfVec2 end, vfVec3 colour)
{
    u32 colValue = intrn::roundNormalColourToARGB(colour.r, colour.g, colour.b);

    i32 startX, endX, startY, endY;
    
    if (intrn::absoluteValue(end.y - start.y) < intrn::absoluteValue(end.x - start.x))
    {
        if (start.x > end.x)
        {
            startX = intrn::roundToi32(end.x);
            endX = intrn::roundToi32(start.x);
            startY = intrn::roundToi32(end.y);
            endY = intrn::roundToi32(start.y);
        }
        else
        {
            startX = intrn::roundToi32(start.x);
            endX = intrn::roundToi32(end.x);
            startY = intrn::roundToi32(start.y);
            endY = intrn::roundToi32(end.y);
        }

        i32 deltaX = endX - startX;
        i32 deltaY = endY - startY;
        i32 eps = (deltaY << 1) - deltaX;
        i32 drawY = startY;

        i32 yStep = 1;
        if (deltaY < 0)
        {
            yStep = -1;
            deltaY = -deltaY;
        }
    
        u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget);

        for (i32 drawX = startX; drawX <= endX; drawX++)
        {
            u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt + drawY * displayPitch) + drawX;
            *pixelOffset = colValue;

            if (eps > 0)
            {
                drawY += yStep;
                eps -= (deltaX << 1);
            }

            eps += (deltaY << 1);
        }        
    }
    else
    {
        if (start.y > end.y)
        {
            startX = intrn::roundToi32(end.x);
            endX = intrn::roundToi32(start.x);
            startY = intrn::roundToi32(end.y);
            endY = intrn::roundToi32(start.y);
        }
        else
        {
            startX = intrn::roundToi32(start.x);
            endX = intrn::roundToi32(end.x);
            startY = intrn::roundToi32(start.y);
            endY = intrn::roundToi32(end.y);
        }

        i32 deltaX = endX - startX;
        i32 deltaY = endY - startY;
        i32 eps = (deltaX << 1) - deltaY;
        i32 drawX = startX;

        i32 xStep = 1;
        if (deltaX < 0)
        {
            xStep = -1;
            deltaX = -deltaX;
        }
    
        u8 *pixelPt = reinterpret_cast<u8 *>(rgbaTarget);

        for (i32 drawY = startY; drawY <= endY; drawY++)
        {
            u32 *pixelOffset = reinterpret_cast<u32 *>(pixelPt + drawY * displayPitch) + drawX;
            *pixelOffset = colValue;

            if (eps > 0)
            {
                drawX += xStep;
                eps -= (deltaY << 1);
            }

            eps += (deltaX << 1);
        }        
    }
}

