
#include "vc_StringFunctions.h"
#include "Intrinsics.h"

namespace vclib
{

bool compare(vcString first, vcString second)
{
    if (vclib::length(first) != vclib::length(second))
        return false;
    
    return compareRaw(first, second, vclib::length(second));
}
    
bool compareRaw(vcString first, char *raw, i32 length)
{
    i32 checkedCount = vclib::length(first);
    if (length)
    {
        checkedCount = vclib::minValue<i32>(checkedCount, length); 
    }

    char *iterRaw = raw;
    vcString iterFirst = first;

    for (i32 loop = 0; loop < checkedCount; loop++)
    {
        if (*iterRaw == 0 || (*iterRaw++ != *iterFirst++))
            return false;
    }

    // Ensure raw string has terminated when using
    // implicit length
    if (length == 0 && *iterRaw != 0)
    {
        return false;
    }
    
    return true;
}

void concatenate(vcString dst, vcString string)
{
}

void concatenateRaw(vcString dst, char *raw, i32 length)
{
    i32 currentLength = vclib::length(dst);
    i32 maxLength = vclib::capacity(dst);
    i32 srcLength = length > 0 ? length : maxLength;

    vcString srcBuffer = reinterpret_cast<vcString>(raw);
    vcString dstBuffer = &dst[currentLength];
    
    if (srcBuffer)
    {
        while(*srcBuffer && srcLength > 0 && (currentLength < maxLength))
        {
            *dstBuffer++ = *srcBuffer++;
            currentLength++;
            srcLength--;
        }

        vclib::setLength(dst, currentLength);
    }    
}

void clipStringToRange(vcString string, i32 start, i32 end)
{
    i32 leftIndex = start;
    i32 rightIndex = end;
    
    if (leftIndex < 0)
    {
        leftIndex = vclib::length(string) + leftIndex;
    }
    if (rightIndex < 0)
    {
        rightIndex = vclib::length(string) + rightIndex;
    }
    if (leftIndex == 0 || rightIndex < leftIndex)
    {
        vclib::setLength(string, rightIndex + 1);
        return;
    }

    i32 moveLength = rightIndex - leftIndex + 1;
    for (i32 loop = 0; loop < moveLength; loop++)
    {
        string[loop] = string[leftIndex++];
    }

    vclib::setLength(string, moveLength);
}

void lower(vcString string)
{
    i32 checkedCount = vclib::length(string);
    if (checkedCount)
    {
        i8 lowerOffset = 'a' - 'A';        
        for (i32 loop = 0; loop < checkedCount; loop++)
        {
            if (string[loop] >= 'A' && string[loop] <= 'Z')
            {
                string[loop] += lowerOffset;
            }
        }    
    }
}

r32 convertStringToReal32(vcString string)
{
    vcString iterString = string;
    vcString iterEnd = string + vclib::length(string);
    r32 result = 0.0f;
    
    // Check for initial sign
    r32 signValue = 1.0f;
    if (*iterString == '-')
    {
        signValue = -1.0f;
        iterString++;
    }
    else if (*iterString == '+')
    {
        iterString++;
    }

    // Accumulate value before decimal point
    while (iterString != iterEnd)
    {
        i32 digitVal = (*iterString) - '0';
        if (digitVal < 0 || digitVal > 9)
        {
            break;
        }
            
        result = (result * 10.0f) + (r32)digitVal;
        iterString++;
    }

    if (*iterString == '.')
    {
        iterString++;
        r32 fractionalResult = 0.0f;
        r32 fractionalDivisor = 1.0f;

        // Accumulate value after decimal point
        while (iterString != iterEnd)
        {
            i32 digitVal = (*iterString) - '0';
            if (digitVal < 0 || digitVal > 9)
            {
                break;
            }

            fractionalResult = (fractionalResult * 10.0f) + (r32)digitVal;
            fractionalDivisor *= 10.0f;
            iterString++;
        }

        result += (fractionalResult / fractionalDivisor);
    }

    return (result * signValue);
}

i32 convertStringToInt32(vcString string)
{
    vcString iterString = string;
    vcString iterEnd = string + vclib::length(string);
    i32 result = 0;
    
    // Check for initial sign
    i32 signValue = 1;
    if (*iterString == '-')
    {
        signValue = -1;
        iterString++;
    }
    else if (*iterString == '+')
    {
        iterString++;
    }

    // Accumulate value while still numeric
    while (iterString != iterEnd)
    {
        i32 digitVal = (*iterString) - '0';
        if (digitVal < 0 || digitVal > 9)
        {
            break;
        }
            
        result = (result * 10) + (i32)digitVal;
        iterString++;
    }
    
    return (result * signValue);
}

}
